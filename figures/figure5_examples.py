import matplotlib.pyplot as plt, matplotlib.colors as colors
import numpy as np
import tensorflow as tf

import config
from utilities.extraction_functions import onsets_offsets, top_k
from utilities.functions import restrict_kwargs


def norm(x, axis=None, q=None):
    if q is not None and 0. <= q <= 1.:
        if q <= .5:
            x_min = np.quantile(x, axis=axis, q=q, keepdims=True)
            x_max = np.quantile(x, axis=axis, q=1 - q, keepdims=True)
        elif q > .5:
            x_min = np.quantile(x, axis=axis, q=1 - q, keepdims=True)
            x_max = np.quantile(x, axis=axis, q=q, keepdims=True)
    else:
        x_min = np.min(x, axis=axis, keepdims=True)
        x_max = np.max(x, axis=axis, keepdims=True)

    with np.errstate(divide='ignore', invalid='ignore'):
        x = (x - x_min) / (x_max - x_min)
        x[np.isnan(x)] = 0.

    return np.clip(x, 0, 1)


def plot_predictions(start,
                     end,
                     true,
                     pred,
                     names,
                     threshold=None,
                     min_frames=5,
                     frame_dur=1./80.,
                     show_negative_classes=True,
                     show_continous_predictions=False,
                     filename=None,
                     wav_file=None,
                     frame_args=None,
                     env=None,
                     include=None,
                     spec=None,
                     stft_args=None,
                     samplerate=48000,
                     ):

    slc = slice(start, end)
    true_slice = {k: v[slc] for k, v in true.items()}
    pred_slice = {k: v[slc] for k, v in pred.items()}
    slice_keep = {k: v[slc] for k, v in pred.items()}

    thresholds = {'Detection': 0., 'Sex': 0, 'Source': 0}
    thresholds.update(threshold)

    if spec is not None:
        if stft_args is not None:
            spec = spec[slice(int(start * samplerate * frame_dur), int(end * samplerate * frame_dur))]
            spec = tf.signal.stft(spec.T, **stft_args)
            spec = 20 * np.log10(np.abs(spec))
            if len(spec.shape) == 3:
                spec = np.max(spec, axis=0)
            spec = spec.T
        else:
            spec = spec[slc]


    if wav_file is not None:
        import soundfile as sf
        sr = sf.info(wav_file).samplerate
        wav_start = int(slc.start * sr * frame_dur)
        wav_stop = int(slc.stop * sr * frame_dur)
        y, sr = sf.read(wav_file, start=wav_start, stop=wav_stop)
        env = np.abs(tf.signal.frame(y.T, **frame_args, axis=-1, pad_end=True))
        env = norm(np.sqrt(np.mean(env, axis=-1)) * np.max(env, axis=-1))

    elif env is not None:
        env = env[slc]

    pred_slice['Detection'] = np.where(pred_slice['Detection'] > thresholds['Detection'],
                                       pred_slice['Detection'],
                                       np.zeros_like(pred_slice['Detection']))
    ons, offs = onsets_offsets(pred_slice['Detection'] > thresholds['Detection'])

    mask = (offs - ons) > min_frames
    ons = ons[mask]
    offs = offs[mask]

    if env is not None:
        env_ = np.ma.masked_array(env, mask=False)
        for on, off in zip(ons, offs):
            env_.mask[on:off] = True
        threshold_env = np.quantile(env_[~env_.mask], q=.9)
        pred_slice['Detection'][env < threshold_env] = 0

    fig, axs = plt.subplots(nrows=3)

    pred_slice['Sex'] = np.where(pred_slice['Sex'] > thresholds['Sex'],
                                 pred_slice['Sex'],
                                 np.zeros_like(pred_slice['Sex']))
    pred_slice['Sex'] = top_k(array=pred_slice['Sex'], axis=-1, k=1)
    pred_slice['Sex'] *= pred_slice['Detection']

    pred_slice['Source'] = np.where(pred_slice['Source'] > thresholds['Sex'],
                                    pred_slice['Source'],
                                    np.zeros_like(pred_slice['Source']))
    pred_slice['Source'] = top_k(pred_slice['Source'], axis=-1, k=1)
    pred_slice['Source'] *= pred_slice['Detection']

    final = {k: 2 * true_slice[k] - pred_slice[k] for k in true_slice if k != 'presence_Source'}

    tick_labels = names
    if not show_negative_classes:
        present_classes = (np.sum(final['Source'], axis=0) != 0)
        tick_labels = [_ for _, present in zip(tick_labels, present_classes) if present]
        final['Source'] = final['Source'][:, present_classes]
    elif isinstance(show_negative_classes, list):
        final['Source'] = final['Source'][:, np.array([i for i, _ in enumerate(tick_labels) if _ in show_negative_classes])]
        tick_labels = show_negative_classes

    height_ratios = [1, 2, len(tick_labels)]
    if include is not None:
        final = {k: v for k, v in final.items() if k in include}
        if 'Detection' not in include:
            height_ratios.pop(-3)
        if 'Sex' not in include:
            height_ratios.pop(-2)
        if 'Source' not in include:
            height_ratios.pop(-1)
    height_ratios.insert(0, sum(height_ratios))

    fig, axs = plt.subplots(nrows=len(final) + 1, sharex=True,
                            gridspec_kw={'height_ratios': height_ratios})

    # NB: the order of the keys is crucial for the order of the subplots
    labels = {'Detection': ['Detection'],
              'Sex': ['Female', 'Male'],
              'Source': tick_labels}
    if include is not None:
        labels = {k: v for k, v in labels.items() if k in include}

    n_frames = [final[key].shape[0] for key in final][0]

    for i, key in enumerate(list(labels.keys())):
        axs[i+1].hlines(xmin=0,
                        xmax=n_frames,
                        y=np.arange(.5, len(labels[key])-1, 1),
                        color='black',
                        linewidth=.5,
                        linestyles=':')
        axs[i + 1].imshow(final[key].T,
                          interpolation='nearest',
                          # For plotting network results: FP in red, TN in white, TP in black, FN in blue
                          cmap=colors.ListedColormap(['red', 'white', "black", 'blue']),
                          vmin=-1,
                          vmax=2)
        axs[i + 1].axis([0, n_frames, -.5, len(labels[key])-.5])
        axs[i + 1].set_yticks(list(range(len(labels[key]))))
        axs[i + 1].set_yticklabels(labels[key])

    if len(final) > 0:
        axs[0].set_yticklabels([])
        if spec is not None:
            axs[0].imshow(spec, origin="lower")

        axs[0].axis([0, n_frames, 0, (spec.shape[0]-1 if spec is not None else 1)])
        if show_continous_predictions:
            axs[0].plot(slice_keep['Source'] * (spec.shape[0] if spec is not None else 1), linewidth=.5)

        axs[0].set_yticks([])
        axs[-1].set_xticks(np.linspace(0, n_frames, 6))
        axs[-1].set_xticklabels(np.round(np.linspace(0, n_frames, 6) * frame_dur, 2))
        if frame_dur is not None:
            axs[-1].set_xlabel("Time (s)")

        [ax.set_aspect('auto') for ax in axs.flat]
    else:
        if spec is not None:
            axs.imshow(spec, origin="lower")

        axs.axis([0, n_frames, 0, (spec.shape[0]-1 if spec is not None else 1)])

        if show_continous_predictions:
            axs.plot(slice_keep['Source'] / np.sum(slice_keep['Source'], axis=-1, keepdims=True) * (spec.shape[0] if spec is not None else 1), linewidth=.5)

        axs.set_aspect('auto')
        axs.set_yticklabels([])
        axs.set_xticks(np.linspace(0, n_frames, 6))
        axs.set_xticklabels(np.round(np.linspace(0, n_frames, 6) * frame_dur, 2))
        if frame_dur is not None:
            axs.set_xlabel("Time (s)")
        axs.set_aspect('auto')

    plt.subplots_adjust(hspace=0)

    if filename is not None:
        plt.savefig(filename, dpi=1000, bbox_inches='tight')
    else:
        plt.show()


if __name__ == "__main__":
    import os
    import pickle

    # paths
    # data_path is the path to a pickle file, as output by evaluate.py
    # i.e. the raw predictions on the test data of a model
    data_path = 'C:/Users/killian/Desktop/database_rooks/model_tests/20230222_214116_ovlp0.pickle'

    # out_path is simply where to put the PNG file afterwards
    out_path = 'C:/Users/killian/Desktop'

    # Sets font size for all plots
    plt.rcParams.update({'font.size': 14})

    with open(data_path, 'rb') as file:
        spec, true, pred = pickle.load(file)

    # Reproduce the subpanels of the examples figure
    names = sorted(config.classes['Source'])

    same_args = dict(
        spec=spec,
        true=true,
        pred=pred,
        threshold={'Detection': .5, 'Source': .2, 'Sex': 0, 'presence_Source': 0},
        env=None,
        names=names,
        frame_dur=1. / 80.,
        show_negative_classes=False,
        stft_args=restrict_kwargs(config.spectrogram_args, tf.signal.stft)
    )

    # A
    plot_predictions(
        # slc is a time selection in spectrogram frames, e.g. here: take from frame 2000 to frame 3200
        # with frame_dur = 1 / 80 in seconds, that's a 15s interval
        # it selects some calls from Bashir and Jolene
        start=2000,
        end=15*80+2000,
        filename=f'{out_path}/Fig5A.png',
        **same_args
    )

    # B
    plot_predictions(
        # same as above, but slightly more difficult
        # it selects part of a song bout from Balbo
        start=300000 + 8 * 80 + 40,
        end=304000 - 1120 - 40,
        filename=f'{out_path}/Fig5B.png',
        **same_args
    )
