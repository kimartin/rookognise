import warnings
from typing import Callable
from math import ceil
from modules.custom_layers import LearnedNormPool2D, LinearSum
from utilities.functions import restrict_kwargs, restricted
import tensorflow as tf
import tensorflow_addons as tfa
tfkl = tf.keras.layers
K = tf.keras.backend


def apply_norm(x=None, norm="batch", scale=True, axis=-1 if K.image_data_format() == 'channels_last' else 1, **kwargs):
    if norm is None:
        norm = ''

    arg_dict = dict(scale=scale or 'mean' in norm, axis=axis)
    if 'batch' in norm:
        layer = tfkl.BatchNormalization(renorm=False, **arg_dict, **kwargs)
    elif 'renorm' in norm:
        layer = tfkl.BatchNormalization(renorm=True, **arg_dict, **kwargs)
    elif 'layer' in norm:
        layer = tfkl.LayerNormalization(**arg_dict, **kwargs)
    elif 'instance' in norm:
        layer = tfa.layers.InstanceNormalization(**arg_dict, **kwargs)
    else:
        return x

    if x is not None:
        return layer(x)

    return layer


def squeeze_excite_module(x,
                          r: float = 16.,
                          pooling: str | tuple = 'conv',
                          activation: None | str | tfkl.Layer = None,
                          activation_args: None | dict = None,
                          conv_kwargs: None | dict = None,
                          norm: None | str = None,
                          ):
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1
    input_shape = x.shape
    ndim = len(input_shape) - 2
    filters = input_shape[channel_axis]
    if channel_axis == -1:
        x_shape = input_shape[1:-1]
    else:
        x_shape = input_shape[2:]

    conv_layer = getattr(tfkl, f'Conv{ndim}D')

    match pooling:
        case 'conv':
            pooling_layer = conv_layer(
                filters=filters,
                groups=filters,
                activation=None,
                kernel_size=x_shape,
                **conv_kwargs
            )
        case 'max' | 'avg' | 'average':
            pool_layer = getattr(tfkl, f'Global{pooling.capitalize()}Pool{ndim}D')
            pooling_layer = pool_layer(keepdims=True)
        case (layer, layer_kwargs):
            # General case : if self.pooling is a layer with provided arguments
            if layer_kwargs is None:
                layer_kwargs = dict()
            pooling_layer = layer(**layer_kwargs)
        case _:
            raise ValueError(f'Unmatched case for pooling: {pooling=}')

    pooled = pooling_layer(x)

    first_conv = conv_layer(filters=int(filters / r),
                            kernel_size=1,
                            activation=activation if not isinstance(activation, type(tfkl.Layer)) else None,
                            **conv_kwargs)(pooled)
    if isinstance(activation, type(tfkl.Layer)):
        first_conv = activation(**activation_args)(first_conv)

    if norm:
        first_conv = apply_norm(first_conv, nrom=norm)

    second_conv = conv_layer(filters=filters,
                             activation="sigmoid",
                             kernel_size=1,
                             **conv_kwargs)(first_conv)

    out = tfkl.Multiply()([x, second_conv])
    return out

def pool_function(x=None,
                  pool_type='max',
                  pool_size=3,
                  activation=None,
                  channel_axis=-1,
                  padding='valid',
                  norm=None,
                  ndim=None):
    if hasattr(pool_size, '__len__'):
        ndim = len(pool_size)
    elif x is not None:
        ndim = len(x.shape) - 2
    elif ndim is None:
        raise ValueError('Please provide a ndim integer argument, giving the desired dimensionality of pooling')

    if pool_type == 'max':
        layer = getattr(tfkl, f'MaxPool{ndim}D')(pool_size=pool_size, padding=padding)
    elif pool_type in ['average', 'avg']:
        layer = getattr(tfkl, f'AvgPool{ndim}D')(pool_size=pool_size, padding=padding)
    elif pool_type == 'rms':
        layer = LearnedNormPool2D(pool_size=pool_size, power=2., trainable=False, padding=padding)
    elif isinstance(pool_type, float | int):
        layer = LearnedNormPool2D(pool_size=pool_size, power=pool_type, trainable=True, padding=padding)
    elif pool_type == 'depthwise' or (pool_type == 'conv' and x is None):
        layer = getattr(tfkl, f'DepthwiseConv{ndim}D')(depth_multiplier=1,
                                                       kernel_size=pool_size,
                                                       strides=pool_size,
                                                       padding=padding,
                                                       activation=activation)
    elif pool_type == 'conv' and x is not None:
        layer = getattr(tfkl, f'Conv{ndim}D')(filters=x.shape[channel_axis],
                                              groups=x.shape[channel_axis],
                                              kernel_size=pool_size,
                                              strides=pool_size,
                                              padding=padding,
                                              activation=activation)
    else:
        warnings.warn(f"pool_type was {pool_type}, which is not covered by this function. Accordingly,"
                      f"input was returned without any action. "
                      f"To actually do a pooling operation, pass one of 'max', 'average', 'rms', 'conv' "
                      f"or a scalar to pool_type argument.")
        return x

    if x is not None:
        x = layer(x)
        x = apply_norm(x, norm=norm)
        return x

    return layer


def conv_batchnorm_layer(
        x,
        filters,
        kernel_size,
        padding=None,
        strides=1,
        dilation_rate=1,
        activation=None,
        activation_args=None,
        causal=False,
        conv_type=tfkl.Conv2D,
        norm='renorm',
        dropout_rate=0.,
        conv_kwargs=None,
        squeeze_excite_r=0.,
        squeeze_excite_op='conv',
        name=None,
):
    """
    Wrapper function over the composition SpatialDropout -> ZeroPadding -> Conv -> BatchNorm.
    Each element except Conv is optional, controlled by their respective arguments

    :param x: A Model object. The SparrowLayer operation will be added at the end of it.
    :param filters: int. Number of filters for the Conv operation.
    :param kernel_size: int or tuple of int. The convolutional kernel.
    :param strides: int or tuple of int. The convolutional strides.
    :param dilation_rate: int or tuple of int. The convolutional dilation rate.
    :param padding: None or list of int. Gives the axes to zero-pad over.
    :param activation: callable or str. The activation function for the convolution.
    :param activation_args: None or dict. Additional arguments passed to activation if activation is callable.
    :param causal: bool. Affects both the convolution and the zero padding operations. If True, both will be modified to
    be causal by zero-padding at the beginning of the second-to-last axis (which is assumed to be the time axis, the
    last being the filter axis). If False, zero-padding is done half at the beginning and half at the end of the
    second-to-last axis.
    :param conv_type: None or Layer class. Must be non-instantiated. If not None, one Conv1D, Conv2D or Conv3D, giving
    the type of operations to add to the model. If None, is inferred from len(kernel_size) with a warning.
    :param batchnorm: bool. Whether (True) or not (False) to apply BatchNormalization after the convolution.
    BatchNormalization is used with default parameters in Tensorflow, except for renorm below.
    :param renorm: bool. If False, use BatchNormalization. If True, use BatchRenormalization.
    :param dropout_rate: float, 0. <= dropout_rate < 1. The dropout rate to use for SpatialDropout. Will randomly drop
    this proportion of the (entire) feature maps output by the layer before it at each learning step.
    As a special case, dropout_rate = 0. skips the inclusion of the SpatialDropout layer.
    :param conv_kwargs: None or dict. Additional convolution arguments
    :param squeeze_excite: None or int. If not None, adds a SqueezeExcitation block following this specification.

    :return: a Model object, x with a SparrowLayer operation on top.
    """
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    if isinstance(padding, int):
        padding = [padding]

    # ConvND args
    conv_kwargs = conv_kwargs or dict()
    # Activation args
    activation_args = activation_args or dict()

    conv_dim = conv_type.__name__[-2:]

    # Dropout
    if dropout_rate > 0.:
        dropout_layer = getattr(tfkl, f'SpatialDropout{conv_dim}')
        x = dropout_layer(rate=dropout_rate)(x)

    # Zero-padding
    if isinstance(padding, list | tuple) and len(padding) > 0 and hasattr(kernel_size, '__len__'):
        pad_layer = getattr(tfkl, f'ZeroPadding{conv_dim}')
        padds = [0] * len(kernel_size) if hasattr(kernel_size, '__len__') else [0]
        for p in padding:
            padds[p] = (kernel_size[p] + 2 * dilation_rate - 1, 0) if causal else kernel_size[p] // 2 + dilation_rate - 1
        x = pad_layer(padding=padds)(x)

    # Conv operation
    conv_arg_dict = dict(
        kernel_size=kernel_size,
        activation=activation if not isinstance(activation, type(tfkl.Layer)) else None,
        padding=padding if isinstance(padding, str) else "valid",
        strides=strides,
        dilation_rate=dilation_rate,
        **conv_kwargs,
        name=name if not isinstance(activation, type(tfkl.Layer)) else None,
        filters=filters,
    )
    x = conv_type(**restrict_kwargs(conv_arg_dict, conv_type))(x)
    if activation is not None and isinstance(activation, type(tfkl.Layer)):
        x = activation(**activation_args, name=name)(x)

    # Batch Normalization
    x = apply_norm(x, norm=norm, axis=channel_axis)

    # Squeeze-Excitation block
    if squeeze_excite_r > 0.:
        x = squeeze_excite_module(x,
                                  r=squeeze_excite_r,
                                  pooling=squeeze_excite_op,
                                  activation=activation,
                                  activation_args=activation_args,
                                  conv_kwargs=conv_kwargs)

    return x


def residual_block(
        x,
        filters,
        kernel_size=(1, 3),
        dilation_rate=1,
        norm='renorm',
        activation='relu',
        activation_args=None,
        conv_kwargs=None,
        squeeze_excite_r=0.,
        squeeze_excite_op='conv',
        dropout_rate=0.,
        conv_type=tfkl.Conv2D,
        name=None,
):
    channel_axis = -1 if K.image_data_format() == 'channels_last' else 1
    in_channels = x.shape[channel_axis]

    activation_args = activation_args or dict()
    conv_kwargs = conv_kwargs or dict()

    ndim = conv_type.__name__[-2:]

    # Dropout
    if dropout_rate > 0.:
        dropout_layer = getattr(tfkl, f'SpatialDropout{ndim}')
        y = dropout_layer(rate=dropout_rate)(x)
    else:
        y = x

    conv_layer = getattr(tfkl, f'Conv{ndim}')
    y = conv_layer(kernel_size=1,
                   filters=filters,
                   activation=activation if not isinstance(activation, type(tfkl.Layer)) else None,
                   **restrict_kwargs(conv_kwargs, conv_layer))(y)
    if isinstance(activation, type(tfkl.Layer)):
        y = activation(**activation_args)(y)
    y = apply_norm(y, norm=norm)

    depthwise_conv_layer = getattr(tfkl, f'DepthwiseConv{ndim}')
    y = depthwise_conv_layer(kernel_size=kernel_size,
                             activation=activation if not isinstance(activation, type(tfkl.Layer)) else None,
                             dilation_rate=dilation_rate,
                             padding='same',
                             **restrict_kwargs(conv_kwargs, depthwise_conv_layer))(y)
    if isinstance(activation, type(tfkl.Layer)):
        y = activation(**activation_args)(y)
    y = apply_norm(y, norm=norm)

    y = conv_layer(filters=in_channels,
                   kernel_size=1,
                   activation=None,
                   **restrict_kwargs(conv_kwargs, conv_layer))(y)

    if squeeze_excite_r > 0.:
        y = squeeze_excite_module(y,
                                  r=squeeze_excite_r,
                                  pooling=squeeze_excite_op,
                                  activation=activation,
                                  activation_args=activation_args,
                                  conv_kwargs=conv_kwargs)

    x = tfkl.Add(name=name)([x, y])

    return x


def resnext_layer(
        x,
        kernel_size=(1, 3),
        dilation_rate=1,
        depth_multiplier=2.,
        norm='renorm',
        activation='relu',
        activation_args=None,
        conv_kwargs=None,
        squeeze_excite_r=0.,
        squeeze_excite_op='conv',
        dropout_rate=0.,
        conv_type=tfkl.Conv2D,
        filters=None,
        name=None,
):
    channel_axis = -1 if K.image_data_format() == 'channels_last' else 1
    in_channels = x.shape[channel_axis]

    activation_args = activation_args or dict()
    conv_kwargs = conv_kwargs or dict()

    ndim = conv_type.__name__[-2:]

    # Dropout
    if dropout_rate > 0.:
        dropout_layer = getattr(tfkl, f'SpatialDropout{ndim}')
        y = dropout_layer(rate=dropout_rate)(x)
    else:
        y = x

    if 'Separable' in conv_type.__name__:
        y = conv_type(
            filters=filters,
            kernel_size=kernel_size,
            activation=activation if not isinstance(activation, type(tfkl.Layer)) else None,
            padding='same',
            **restrict_kwargs(conv_kwargs, conv_type)
        )(y)
    else:
        depthwise_conv_layer = getattr(tfkl, f'DepthwiseConv{ndim}')
        y = depthwise_conv_layer(kernel_size=kernel_size,
                                 activation=None,
                                 dilation_rate=dilation_rate,
                                 padding='same',
                                 **restrict_kwargs(conv_kwargs, depthwise_conv_layer))(y)

        y = apply_norm(y, norm=norm)

        conv_layer = getattr(tfkl, f'Conv{ndim}')
        y = conv_layer(
            filters=filters,
            kernel_size=1,
            activation=activation if not isinstance(activation, type(tfkl.Layer)) else None,
            **restrict_kwargs(conv_kwargs, conv_layer))(y)

    if activation is not None and isinstance(activation, type(tfkl.Layer)):
        y = activation(**activation_args)(y)

    conv_layer = getattr(tfkl, f'Conv{ndim}')
    y = conv_layer(filters=in_channels,
                   kernel_size=1,
                   activation=None,
                   **restrict_kwargs(conv_kwargs, conv_layer))(y)

    if squeeze_excite_r > 0.:
        y = squeeze_excite_module(y,
                                  r=squeeze_excite_r,
                                  pooling=squeeze_excite_op,
                                  activation=activation,
                                  activation_args=activation_args,
                                  conv_kwargs=conv_kwargs)

    x = LinearSum(name=name)([x, y])

    return x


def mixing_scale_layer(x,
                       kernel_size,
                       growth,
                       width,
                       activation='relu',
                       unit_layer=resnext_layer,
                       activation_args=None,
                       norm='renorm',
                       conv_type=tfkl.Conv2D,
                       padding='same',
                       causal=False,
                       dropout_rate=0.,
                       squeeze_excite_r=0.,
                       squeeze_excite_op=None,
                       conv_kwargs=None,
                       c=0.,
                       **kwargs
                       ):
    in_channels = x.shape[-1 if K.image_data_format() == 'channels_last' else 1]

    if activation_args is None:
        activation_args = dict()
    if conv_kwargs is None:
        conv_kwargs = dict()

    args = restrict_kwargs(locals(), unit_layer, x=x, filters=growth, **kwargs)
    x_list = [x] + [unit_layer(dilation_rate=2**d, **args) for d in range(width)]
    out = tfkl.Concatenate()(x_list)

    if c:
        out = conv_type(**restrict_kwargs(conv_kwargs, conv_type, filters=int(in_channels * c), kernel_size=1, activation=None,))(out)
        #out = apply_norm(out, norm=norm)

    return out


def multi_scale_dense_module(
        x,
        growth,
        width,
        depth,
        activation,
        kernel_size,
        c=0.,
        unit_layer=resnext_layer,
        activation_args=None,
        norm='renorm',
        conv_type=tfkl.Conv2D,
        padding='same',
        causal=False,
        dropout_rate=0.,
        squeeze_excite_r=0.,
        squeeze_excite_op=None,
        conv_kwargs=None,
        **kwargs
):
    for j in range(depth):
        x = restricted(mixing_scale_layer)(d=locals(), x=x, **kwargs)

    return x


def self_attention_layer(x, in_kernel_size=1, out_kernel_size=1, r=2., norm='renorm', conv_type=tfkl.Conv1D, conv_kwargs=None, name=None):
    if not conv_kwargs:
        conv_kwargs = dict()

    channel_axis = -1 if K.image_data_format() == 'channels_last' else 1
    in_channels = x.shape[channel_axis]
    hidden_channels = int(round(in_channels / r))

    query = conv_type(filters=hidden_channels, kernel_size=in_kernel_size, activation=None, use_bias=False, **conv_kwargs)(x)
    query = apply_norm(query, norm=norm, axis=channel_axis)

    key = conv_type(filters=hidden_channels, kernel_size=in_kernel_size, activation=None, use_bias=False, **conv_kwargs)(x)
    key = apply_norm(key, norm=norm, axis=channel_axis)

    value = tfkl.Conv1D(filters=hidden_channels, kernel_size=in_kernel_size, activation=None, use_bias=False, **conv_kwargs)(x)
    value = apply_norm(value, norm=norm, axis=channel_axis)

    score = tfkl.Attention(use_scale=True)([query, key, value])
    score = conv_type(filters=in_channels, kernel_size=out_kernel_size, activation=None, **conv_kwargs)(score)
    score = apply_norm(score, norm=norm, axis=channel_axis)

    x = LinearSum(name=name)([x, score])

    return x


def tcn_module(x,
               kernel_size,
               depth,
               strides=1,
               filters: None | int = None,
               stacks: int = 1,
               base_dilation_rate: int = 2,
               causal: bool = False,
               padding: str = 'same',
               activation: None | str | type[tfkl.Layer] | Callable = None,
               activation_args=None,
               conv_type=tfkl.Conv1D,
               norm='renorm',
               pool_type=None,
               pool_size=None,
               conv_kwargs=None,
               squeeze_excite_r=0.,
               squeeze_excite_op='conv',
               unit_layer=conv_batchnorm_layer,
               name=None,
               ):
    channel_axis = -1 if K.image_data_format() == 'channels_last' else 1
    for s in range(stacks):
        for d in range(depth):
            x = restricted(unit_layer)(locals(),
                                       x=x,
                                       in_channels=x.shape[channel_axis],
                                       filters=filters,
                                       depth_multiplier=filters,
                                       dilation_rate=base_dilation_rate**d,
                                       name=name if s == stacks - 1 and d == depth - 1 and not pool_type else None)
        if pool_type:
            x = pool_function(x=x, pool_type=pool_type, pool_size=pool_size, norm=None)
    return x


def wavegram(freq_axis_size,
             filters: int | tuple = 32,
             block_ratio: int | tuple = (2, 2, 2),
             kernel_size: int | tuple = 3,
             pool_size: int | tuple = 3,
             pool_type: str | float = "max",
             activation: str | type(tfkl.Layer) = tfkl.LeakyReLU,
             activation_args: None | dict = None,
             norm: str = 'renorm',
             conv_type: type(tfkl.Layer) = tfkl.DepthwiseConv1D,
             squeeze_excite_r: float | int = 0.,
             squeeze_excite_op: str = 'avg',
             conv_kwargs: None | dict = None,
             base_dilation_rate: int = 2,
             unit_layer: Callable | type(tfkl.Layer) = conv_batchnorm_layer,
             ):

    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    if activation == tfkl.LeakyReLU and not activation_args:
        activation_args = {"alpha": 0.01}

    general_args = dict(
        kernel_size=kernel_size,
        activation=activation,
        activation_args=activation_args,
        conv_kwargs=conv_kwargs,
        norm=norm,
        squeeze_excite_r=squeeze_excite_r,
        squeeze_excite_op=squeeze_excite_op,
        conv_type=conv_type,
        padding='same',
    )

    if isinstance(filters, int) and isinstance(block_ratio, int) and isinstance(pool_size, int):
        raise ValueError('provide at least one of filters, block_ratio, or pool_size as a tuple to inform network depth.')
    elif isinstance(block_ratio, tuple):
        depth = len(block_ratio)
    elif isinstance(pool_size, tuple):
        depth = len(pool_size)
    else:
        depth = len(filters)

    if isinstance(filters, int):
        filters = tuple([filters] * depth)

    if isinstance(pool_size, int):
        pool_size = tuple([pool_size] * depth)

    if isinstance(block_ratio, int):
        block_ratio = tuple([block_ratio] * depth)

    def inner_func(x):
        for b, f, p in zip(block_ratio, filters, pool_size, strict=True):
            if unit_layer != conv_batchnorm_layer:
                x = restricted(conv_batchnorm_layer)(
                    locals(),
                    x=x,
                    filters=f,
                    **general_args
                )
            x = pool_function(x, pool_type=pool_type, pool_size=p, activation=None, channel_axis=channel_axis)
            for i in range(b):
                x = restricted(unit_layer)(locals(),
                                           x=x,
                                           in_channels=x.shape[channel_axis],
                                           filters=f,
                                           depth_multiplier=int(round(x.shape[channel_axis] / f)),
                                           dilation_rate=base_dilation_rate ** i,
                                           **general_args
                                           )
        if channel_axis == -1:
            x = tfkl.Reshape(target_shape=(*x.shape[1:-1], freq_axis_size, -1))(x)
        else:
            x = tfkl.Reshape(target_shape=(-1, freq_axis_size, *x.shape[2:]))(x)

        return x

    return inner_func


if __name__ == '__main__':
    from utilities.functions import get_model_memory_usage, count_model_parameters
    c = None
    inp = tfkl.Input(shape=(1, 800, 32))
    x = tcn_module(inp, kernel_size=9, stacks=3)

