"""
Extract labels from prediction data output by predict.py
"""
import os
import numpy as np
import pickle
from tqdm import tqdm

from utilities.extraction_functions import labels_from_network

import config

# Common root
root = "C:/Users/killian/Desktop/database_rooks"

# Path to prediction_files
# data_path = f'{root}/data/test'
data_path = f"{root}/unseen"

# Path to output
# out_path = f"{root}/unseen"
out_path = data_path

# Get list of .pickle files in data_path
files = os.listdir(path=data_path)
files = [f for f in files if f.endswith('pickle')]

# Define names
names = {
    "Source": ["Aristotle", "Balbo", "Bashir", "Braad", "Brain", "Bussell", "Cassandra", "Connelly", "Elie",
               "Feisty", "Fry", "Gigi", "Huxley", "Jolene", "Jonas", "Kafka", "Leo", "Merlin", "Osiris", "Pomme",
               "Siobhan", "Tom"],
    "Sex": ['Female', 'Male']
}

for file in tqdm(files, total=len(files)):
    out_filename = file.replace("pickle", "txt")

    with open(f'{data_path}/{file}', 'rb') as connection:
        data = pickle.load(file=connection)

    # reshaping here assumes the first axis of v is the sample/batch axis,
    # so reshape actually concatenates samples one after another
    data = {k: np.reshape(v, (-1, v.shape[-1] if v.shape[-1] < 30 else 1)) for k, v in data.items()}

    labels = labels_from_network(pred=data,
                                 threshold={'Detection': .5, 'Source': .2},
                                 merge=2,
                                 min_frames=5,
                                 min_to_keep={'Detection': .5, 'Source': .2},
                                 frame_dur=1. / config.fps,
                                 names={k: sorted(n) for k, n in names.items()},
                                 show_confidence=False,
                                 detection_name="Detection",
                                 unattributed="Inc",
                                 multiple="Pls",
                                 strict=False,
                                 force=True,
                                 force_choice_r=2.,
                                 )

    labels.to_csv(f'{out_path}/{out_filename}', sep="\t", index=False, header=False)

