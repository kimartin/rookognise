"""
Replicates Fig. 4, the post-processing / confidence curve.
"""


import matplotlib.pyplot as plt, matplotlib.colors as colors
import matplotlib.gridspec as gridspec
import matplotlib.ticker as mtick
import numpy as np
from utilities.extraction_functions import onsets_offsets


def figure_confidence(true, pred, num_thresholds=200, filename=None):
    fig, axs = plt.subplots(ncols=2, figsize=(9, 4))
    [ax.tick_params(axis='both', labelsize=plt.rcParams['font.size'] - 4) for ax in axs.flat]

    on_true, off_true = onsets_offsets(true['Detection'])
    on_pred, off_pred = onsets_offsets(pred['Detection'] > 0.05)

    axs[0].hist(off_true - on_true,
                alpha=.5,
                align='left',
                weights=100*np.full(shape=(len(on_true), ),
                                    fill_value=1 / len(on_true)),
                cumulative=False,
                bins=20,
                range=(0, 20))
    axs[0].hist(off_pred - on_pred,
                alpha=.5,
                align="left",
                weights=100*np.full(shape=(len(on_pred), ),
                                    fill_value=1 / len(on_pred)),
                cumulative=False,
                bins=20,
                range=(0, 20))
    axs[0].axvline(x=5.5, color='black', label='_nolegend_')
    axs[0].yaxis.set_major_formatter(mtick.PercentFormatter())
    axs[0].legend(['Labelled', 'Predicted'], fontsize=plt.rcParams['font.size'] - 4,
                  bbox_to_anchor=(.95, .95), loc='upper right')
    axs[0].set_xlabel('Length', labelpad=0)

    pred_id = pred['Source'][np.sum(true['Source'], axis=-1) == 1]
    true_id = true['Source'][np.sum(true['Source'], axis=-1) == 1]

    diff = np.sort(pred_id, axis=-1)
    diff = diff[:, -1] / diff[:, -2]

    correct = (np.argmax(pred_id, axis=-1) == np.argmax(true_id, axis=-1)).astype('int')
    thresholds = 10**np.linspace(np.log10(np.min(diff)), np.log10(np.max(diff)), num_thresholds + 1)

    kept = np.array([np.sum(diff > _) for _ in thresholds]) / len(diff)
    accuracy = np.array([np.sum(correct * (diff > _).astype('int')) for _ in thresholds]) / len(diff)

    # curves
    axs[1].plot(thresholds, (accuracy / kept) * 100, color = "C0")
    axs[1].plot(thresholds, kept * 100, color="tab:orange")
    axs[1].set_xscale('log')

    # threshold line, solid black
    axs[1].vlines(x=2, ymin=0, ymax=100, colors='black')

    axs[1].hlines(y=(accuracy / kept)[np.argmin(np.abs(thresholds - 2))]*100, xmin=0, xmax=2, ls = "--", color = "C0", linewidth=1)
    axs[1].hlines(y=kept[np.argmin(np.abs(thresholds - 2))]*100, xmin=0, xmax=2, ls = "--", color = "tab:orange", linewidth=1)

    axs[1].axis([1, thresholds[-1], 0, 100])

    axs[1].yaxis.set_major_formatter(mtick.PercentFormatter())
    axs[1].set_ylabel("", labelpad=0)
    axs[1].set_xlabel(r'$\frac{Highest\,prediction}{Second\,highest\,prediction}$', labelpad=0)

    accuracy = round((accuracy /kept * 100)[np.argmin(np.abs(thresholds - 2))], 2)
    retained = round((kept * 100)[np.argmin(np.abs(thresholds - 2))], 2)

    print(f'Accuracy: {accuracy}')
    print(f'Frames kept: {retained}')

    plt.text(x=6,
             y=accuracy,
             s=f'{accuracy:.2f}%',
             color="C0",
             horizontalalignment='right',
             verticalalignment='top',
             fontdict={"fontsize": plt.rcParams['font.size'] - 4}
             )
    plt.text(x=6,
             y=retained,
             s=f'{retained:.2f}%',
             color="tab:orange",
             horizontalalignment='right',
             verticalalignment='bottom',
             fontdict={"fontsize": plt.rcParams['font.size'] - 4}
             )

    axs[1].legend(['Accuracy', 'Retained'],
                  fontsize=plt.rcParams['font.size'] - 4,
                  bbox_to_anchor=(.95, .95),
                  loc='upper right')
    plt.subplots_adjust(wspace=.5, hspace=.5)

    if filename is None:
        plt.show()
    else:
        plt.savefig(filename, dpi=1000, bbox_inches='tight')
        plt.close()


if __name__ == "__main__":
    import pickle

    # paths
    # data_path is the path to a pickle file, as output by predict (if Lines 83-84 are uncommented)
    # i.e. the raw predictions on the test data of a model
    data_path = 'C:/Users/killian/Desktop/database_crows/model_tests/20230712_184001_ovlp0.pickle'
    # data_path = 'C:/Users/killian/Desktop/database_rooks/model_tests/20230222_214116_ovlp0.pickle'

    # out_path is simply where to put the PNG file afterwards
    out_path = 'C:/Users/killian/Desktop'

    # Sets font size for all plots
    plt.rcParams.update({'font.size': 14})

    with open(data_path, 'rb') as file:
        spec, true, pred = pickle.load(file)

    # Figure 4
    figure_confidence(true,
                      pred,
                      num_thresholds=200,
                      filename=f'{out_path}/Fig4.png'
                      )