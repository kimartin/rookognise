"""
Implements a bunch of metrics, mostly as an exercise to myself when I was learning Tensorflow.

Most interesting are FocalCrossentropy, which implements the focal loss, and MultiChannelAUC, which generalizes the AUC
metric from Tensorflow to outputs with multiple classes (multi-label classification at least).
"""
import numpy as np
import tensorflow as tf
K = tf.keras.backend
from tensorflow.python.ops import math_ops


class FocalCrossentropy(tf.keras.losses.Loss):
    def __init__(self,
                 gamma=0.,
                 label_smoothing=0.,
                 alpha=1.,
                 cos=0.,
                 beta=0.,
                 class_weights=None,
                 name="focal_bce",
                 **kwargs):
        """
        :param gamma: Stength of the focal term.
        :param label_smoothing: float in [0, 1[. Applied to the ground truth labels such that the labels are rescaled
        from {0, 1} to {label_smoothing, 1-label_smoothing}
        :param cos: Strength of the cosine similarity penalty. Should be a float >= 0
        :param alpha: regularisation term for the focal loss. Defaults to 0 if gamma is 0, otherwise should be
        a positive float such that 0 < alpha < 1.
        :param beta: alternative way to essentially set alpha based on the effective number of samples. Supersedes
        alpha if given as a float such that 0 < beta < 1.
        :param class_weights: Weights to be applied to each class during computation of the loss.
        """
        super().__init__(name=name, **kwargs)

        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

        if not 0. <= alpha <= 1.:
            raise ValueError('alpha should be a float in [0., 1.]')
        if not 0. <= beta <= 1.:
            raise ValueError('beta should be a float in [0., 1.]')

        self.beta = beta
        self.alpha = alpha if self.beta <= 0. else 1.
        self.cos = cos
        self.class_weights = class_weights
        if self.class_weights is not None:
            if self.beta > 0.:
                self.class_weights = (1 - self.beta) / (1 - self.beta ** self.class_weights)
            self.class_weights /= np.sum(self.class_weights)
            self.class_weights = tf.constant(self.class_weights)

        self.gamma = gamma
        self.label_smoothing = label_smoothing

    @tf.function
    def cosine_penalty(self, y_pred):
        l2 = tf.sqrt(tf.reduce_sum(y_pred ** 2, axis=1 if self.channel_axis == -1 else 2))
        l2 = tf.expand_dims(l2, axis=-1)
        l2 = tf.matmul(l2, l2, transpose_b=True)
        cos_penalty = tf.matmul(y_pred, y_pred, transpose_a=True)
        cos_penalty = tf.math.divide_no_nan(cos_penalty, l2)
        cos_penalty = tf.linalg.set_diag(cos_penalty, diagonal=tf.zeros(shape=cos_penalty.shape[:-1]))
        cos_penalty = tf.maximum(0, cos_penalty)
        cos_penalty = tf.reduce_sum(cos_penalty, axis=[-1, -2])
        # not explicitly dividing by 2 since it is compensated by the sum of a symmetric matrix above, only by n(n-1)
        cos_penalty /= (y_pred.shape[self.channel_axis] * (y_pred.shape[self.channel_axis] - 1))

        return tf.reduce_mean(cos_penalty)

    # @tf.function
    def call(self, y_true, y_pred, from_logits=False):
        # Apply label smoothing
        # NB: this formulation is appropriate for multilabel, one-vs-all classification, NOT for multiclass, one-vs-one
        y_true = y_true * (1 - self.label_smoothing * 2) + self.label_smoothing

        ce = K.binary_crossentropy(y_true, y_pred, from_logits=from_logits)

        # Applying focal loss parameter
        if self.gamma > 0.:
            p_t = y_true * y_pred + (1-y_true) * (1-y_pred)
            p_t = tf.pow(1 - p_t, self.gamma)
            ce *= p_t

        if self.alpha != 1.:
            a_t = y_true * self.alpha + (1 - y_true) * (1 - self.alpha)
            ce *= a_t
        elif self.beta > 0. or self.class_weights is not None:
            ce *= self.class_weights

        # if tf.rank(ce) == 3:
        #     ce = tf.reduce_sum(ce, axis=self.channel_axis)

        # Cosine penalty to discourage overlaps between classes
        if self.cos > 0.:
            ce *= (1 + self.cos * self.cosine_penalty(y_pred))

        return ce

    def get_config(self):
        conf = super().get_config()
        conf.update({
            "gamma": self.gamma,
            "alpha": self.alpha,
            "beta": self.beta,
            "cos": self.cos,
            "label_smoothing": self.label_smoothing,
            "class_weights": self.class_weights,
        })
        return conf


class MyAUC(tf.keras.metrics.AUC):
    def __init__(self, num_labels=1, multi_label=True, curve='ROC', **kwargs):
        self.prg = False
        if curve == "PRG":
            curve = "PR"
            self.prg = True

        super().__init__(num_labels=num_labels, multi_label=multi_label, curve=curve, **kwargs)
        # since the code below works only with -1, might as well hard-code it for now
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    def update_state(self, y_true, y_pred, sample_weight=None):
        """
        The regular Tensorflow metric requires rank 2 tensors (batch_size, num_labels), so we need to reshape them
        to accommodate rank 3 and above tensors.
        """
        if sample_weight is not None:
            sample_weight = tf.reshape(sample_weight, [-1, 1])

        y_pred = tf.clip_by_value(y_pred, K.epsilon(), 1-K.epsilon())

        if self.channel_axis == 1:
            shape_ = list(range(y_true.shape))
            shape_.pop(1)
            shape_.append(1)
            y_pred = tf.transpose(y_pred, perm=shape_)
            y_true = tf.transpose(y_true, perm=shape_)

        y_pred = tf.reshape(y_pred, [-1, y_pred.shape[-1]])
        y_true = tf.reshape(y_true, [-1, y_pred.shape[-1]])
        super().update_state(y_true, y_pred, sample_weight=sample_weight)

    def count_non_missing_labels(self):
        non_missing_classes = math_ops.reduce_max(
            self.true_positives + self.false_negatives,
            axis=list(range(len(self.true_positives.shape) - 1)),
            keepdims=True,
        )
        non_missing_classes = tf.cast(non_missing_classes > 0., self.true_positives.dtype)
        return math_ops.reduce_sum(non_missing_classes)

    def result(self):
        """
        Small adjustment so that missing classes do not artificially lower the result
        """
        result = super().result()
        if self.num_labels > 1:
            result *= self.num_labels / self.count_non_missing_labels()
        return result


if __name__ == '__main__':
    loss = MyAUC(num_labels=15, curve='ROC')
    loss.update_state(y_true=tf.cast(tf.random.uniform(shape=(800, 15)) > .5, dtype=tf.float32),
                      y_pred=tf.random.uniform(shape=(800, 15)))
    print(loss.count_non_missing_labels())
