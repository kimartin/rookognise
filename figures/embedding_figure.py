import pickle
import os

import matplotlib.pyplot as plt
import numpy as np
from umap import UMAP

from utilities.visualisations import scatter_projections

import config

path = 'C:/Users/killian/Desktop/database_crows/embeddings'

files = os.listdir(path)

pred_id = []
true_id = []
for file in files:
    spec, true, pred = pickle.load(open(f'{path}/{file}', 'rb'))

    classes = np.array(config.classes['Source'])

    pred_id.append(pred['bidirectional'][np.sum(true['Source'], axis=-1) == 1])
    true_id.append(classes[np.argmax(true['Source'][np.sum(true['Source'], axis=-1) == 1], axis=-1)])

pred_id = np.concatenate(pred_id, axis=0)
true_id = np.concatenate(true_id, axis=0)
scatter_projections(
    projection=UMAP(n_neighbors=30, metric='euclidean').fit_transform(pred_id),
    labels=true_id,
    show_legend=False,
    color_palette='nipy_spectral',
    s=1,
    alpha=.2,
)
plt.savefig('C:/Users/killian/Desktop/embedding_crows.png', dpi=300, bbox_inches='tight')
