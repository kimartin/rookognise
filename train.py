"""
Handles network training.
See config to define most parameters, but several variables are defined here for convenience
"""

import os
import tensorflow as tf
K = tf.keras.backend
tfkl = tf.keras.layers
import random
import numpy as np
import pandas as pd

from models.assemble_model import build_model
from models.callback_assemblers import lr_finder_callback_func, training_callback_func

from generators.audio_generators import AudioGenerator, WaveformGenerator, SpectrogramGenerator

from utilities.pcen import PCEN
from utilities.metrics import MyAUC
from modules.custom_layers import LearnedDRC, Spectrogram, TrainableMelFilterbank, WaveformFilters, AddPositionMapping, VectorSTFT
from utilities.functions import product_dict, restrict_kwargs, restricted, count_model_parameters, get_model_memory_usage, split_train_validation
from models.optimizers import optimiser_initialisation
from modules.custom_modules import wavegram

# This one script is not in the repo as it needs some identification to send an email
from utilities.send_email import send_email
import config

os.environ['TF_GPU_THREAD_MODE'] = 'gpu_private'

# "net" trains a network as normal. "lr" runs a learning rate range test to determine best learning rate values.
train_type = "net"
# Monitor one of the metrics to determine Early Stopping
monitor = "val_Source_ap"
# Model name
root_model_name = 'Rookognise'
# Generator type
generator = SpectrogramGenerator

model_parameters_list = product_dict(**config.loop_parameters)
number_of_trainings = len(model_parameters_list)

label_file = pd.read_csv(config.data_file, sep='\t')
if config.data is not None:
    label_file['Audio'] = [f'{config.data}/{_}' for _ in label_file['Audio']]
    label_file['Label'] = [f'{config.data}/{_}' for _ in label_file['Label']]
(train_labels, train_audio), (valid_labels, valid_audio) = split_train_validation(label_file, train_name='train', validation_name='valid')

for i, local_params in enumerate(model_parameters_list):
    K.clear_session()

    if any([all([dc[k] == local_params[k] for k in dc]) for dc in config.excluded_combinations]):
        # skips excluded combinations of parameters, as defined in config.excluded_combinations
        continue

    print(local_params)

    if train_type == 'net':
        # get k from local_params if it's one of the keys, or default to config.k
        k = local_params.pop('k', config.k)
    else:
        k = 3.

    def format_name(value, capitalize=True):
        if callable(value):
            val = value.__name__
        elif isinstance(value, tuple | list):
            val = ''.join([str(v) for v in value])
        elif isinstance(value, bool | int | float):
            val = str(value)
        elif isinstance(value, str):
            val = value
        else:
            val = None

        if capitalize and val:
            return val.capitalize()

        return val

    param_list_name = [f'{k}{format_name(v)}' for k, v in local_params.items()]
    model_name = '_'.join([root_model_name] + param_list_name)

    generator_args = config.spectrogram_generator_args if generator == SpectrogramGenerator else config.waveform_generator_args

    # split loop_params to distribute to the correct functions
    def distribute(dc):
        return {k: local_params[k] if k in local_params else v for k, v in dc.items()}

    loss = local_params.pop('loss', config.loss)

    local_gen_params = distribute(generator_args)
    local_filter_params = distribute(config.filter_args)
    local_spec_params = distribute(config.spectrogram_args)
    local_mel_params = distribute(config.melspectrogram_args)
    local_mel_params.pop('norm', None)
    local_model_params = distribute(config.general_model_args)
    local_params.pop('conv_type', None)
    local_stem_params = distribute(config.stem_args)
    local_frontend_params = distribute(config.frontend_args)
    local_head_params = distribute(config.head_args)
    local_optimiser_params = distribute(config.optimiser_args)
    frontend = local_params.pop('frontend', config.frontend)

    # Set seeds
    seed = local_params.pop('seed', config.seed)
    # that might be overkill, but at least I'm sure to cover my bases
    tf.random.set_seed(seed)
    random.seed(seed)
    np.random.seed(seed)

    # Define the generators
    train_gen = generator(
        **local_gen_params,
        **config.label_generator_args,
        **config.training_args,
        k=k,
        data=train_audio,
        labels=train_labels,
        training=True,
    )
    val_gen = None
    if train_type == 'net':
        val_gen = generator(
            **local_gen_params,
            **config.label_generator_args,
            **config.validation_args,
            data=valid_audio,
            labels=valid_labels,
            # add_reconstruct=config.head_args['add_reconstruct'],
            training=False,
        )

    # Model input shapes, and outputs number of classes
    chunks, labels, _ = train_gen[0]
    batch_size, *input_shape = chunks.shape
    heads = {k: v.shape[-1] for k, v in labels.items()}

    # Losses objects
    loss_dict = {k: loss(**config.loss_args) for k, n in heads.items()}

    def stem(first_layer=None, add_frequency_mapping=False, add_wavegram=False):
        def _inner_func(x):
            waveform_ = spectrogram_ = melspec_ = wavegram_ = first_ = x
            if generator == AudioGenerator:
                waveform_ = WaveformFilters(**local_filter_params, dtype=tf.float32)(x)

            if add_wavegram:
                wavegram_ = restricted(wavegram)(
                    local_model_params,
                    **config.wavegram_args
                )(x)

            if generator in [WaveformGenerator, AudioGenerator]:
                spectrogram_ = Spectrogram(**local_spec_params, dtype=tf.float32)(waveform_)
                melspec_ = TrainableMelFilterbank(**local_mel_params, dtype=tf.float32)(spectrogram_)

            if first_layer == 'PCEN':
                first_ = PCEN(**config.pcen_args)(melspec_)
            elif first_layer == 'LearnedDRC':
                first_ = LearnedDRC(**config.learn_drc_args)(melspec_)
            else:
                first_ = melspec_

            if add_wavegram == 'add':
                first_ = tfkl.Concatenate()([first_, wavegram_])
            elif add_wavegram == 'only':
                first_ = wavegram_

            if add_frequency_mapping:
                first_ = AddPositionMapping(axis=-2)(first_)

            # uncomment the line below if you wish to explore add_reconstruct=True, otherwise it's unneeded and adds
            # some assumptions to build_model (namely, that stem returns a tuple instead of only one object)
            return first_  #, waveform_, spectrogram_, melspec_

        return _inner_func

    built_stem = stem(**local_stem_params)
    built_frontend = frontend(**local_frontend_params, **restrict_kwargs(local_model_params, frontend))

    model = build_model(
        input_shape=input_shape,
        stem=built_stem,
        frontend=built_frontend,
        heads=heads,
        masks=config.masks_combinations,
        **local_model_params,
        **local_head_params,
        name=root_model_name,
        batch_size=batch_size,
    )

    # print(model.summary())
    print(count_model_parameters(model),
          get_model_memory_usage(batch_size=batch_size, model=model,
                                 policy=config.policy,
                                 per_layer=False))
    # tf.keras.utils.plot_model(model=model, to_file=f'C:/Users/killian/Desktop/{model_name}.png', show_shapes=True)

    compile_params = dict(
        optimizer=optimiser_initialisation(
            **local_optimiser_params,
            steps_per_epoch=len(train_gen),
            training=train_type),
        loss=loss_dict,
        loss_weights=local_params.pop('loss_weights', {'Detection': 1, 'Sex': 1, 'Source': 1, 'presence_Source': 1})
    )

    # Train
    callbacks = None
    epochs = config.epochs
    if train_type == 'net':
        # Define metrics
        metric_args = dict(num_thresholds=200)
        metrics_dict = {k: [MyAUC(num_labels=c, curve="ROC", name="auroc", **metric_args),
                            MyAUC(num_labels=c, curve="PR", name="ap", **metric_args)]
                        for k, c in heads.items()}
        compile_params.update(dict(weighted_metrics=metrics_dict))

        callbacks = training_callback_func(
            **config.callback_args,
            monitor=monitor,
            path=f"{config.path}/{root_model_name}",
            model_name=model_name
        )
    elif train_type == 'lr':
        epochs = 1
        callbacks = lr_finder_callback_func(
            path=f"lr_test/{root_model_name}",
            lr_min=1e-10,
            lr_max=1e-0,
            model_name=model_name
        )

    model.compile(**compile_params)

    # Launch
    history = model.fit(
        x=train_gen,
        validation_data=val_gen,
        epochs=epochs,
        verbose=1,
        callbacks=callbacks,
    )

    if val_gen:
        best_epoch = history.history[monitor].index(max(history.history[monitor]))
        best_metrics = {'epoch': best_epoch + 1, **{k: round(v[best_epoch], 4) for k, v in history.history.items()}}
        best_metrics = f' \n{best_metrics}'
    else:
        best_metrics = ''

    # Notify me (NOTE: for privacy reasons this function is not in the repository)
    send_email(body=f"Entrainement {i + 1} sur {number_of_trainings} fini ! {model_name} {best_metrics}")

    # Clear session
    K.clear_session()
