"""
More or less random collection of layers as classes

"""

import math
import tensorflow as tf
from librosa.filters import mel
K = tf.keras.backend
tfkl = tf.keras.layers
from utilities.functions import flatten


class BoundedTruncatedNormal(tf.initializers.TruncatedNormal):
    """
    Slight  re-parametrisation of the original initializer to allow direct specification of boundaries.
    """
    def __init__(self, lower, upper, logscale=True, **kwargs):
        self.lower = lower
        self.upper = upper
        self.logscale = logscale
        super().__init__(mean=(upper + lower) / 2, stddev=abs(upper - lower) / 4, **kwargs)

    def __call__(self, shape, dtype=None, **kwargs):
        x = super().__call__(shape=shape, dtype=dtype, **kwargs)
        if self.logscale:
            x = tf.math.log(x)
        return x

    def get_config(self):
        conf = super().get_config()
        conf.update({'min': self.min, 'max': self.max, 'logscale': self.logscale})
        return conf


class LearnedDRC(tfkl.Layer):
    def __init__(self, alpha=0., **kwargs):
        self.alpha = alpha
        self.__name__ = "LearnedDRC"
        super(LearnedDRC, self).__init__(**kwargs)

    def build(self, input_shape):
        self.param = self.add_weight(shape=(1, ),
                                     name="alpha",
                                     initializer=tf.keras.initializers.Constant(value=self.alpha),
                                     dtype=self.dtype,
                                     trainable=self.trainable,
                                     )

        super(LearnedDRC, self).build(input_shape)

    def call(self, x, **kwargs):
        return tf.pow(x, tf.math.sigmoid(self.param))

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({"alpha": self.alpha})
        return conf


class RootCompression(tfkl.Layer):
    """
    Performs the operation y = x^(1/pow), where pow is a (possibly learnable) hyperparameter
    NB: negative inputs are constrained to 0.
    """

    def __init__(self, power=3., learnable=False, constraints=None, **kwargs):
        super().__init__(**kwargs)
        self.power = power
        self.learnable = learnable
        self.constraints = constraints
        self.root = 1 / self.power

    def call(self, x, **kwargs):
        return tf.pow(tf.maximum(0, x), self.root)

    def build(self, input_shape):
        self.root = self.add_weight(shape=(),
                                    initializer=tf.keras.initializers.Constant(self.root),
                                    constraint=self.constraints,
                                    trainable=self.learnable,
                                    dtype=self.dtype,
                                    name="root")
        super().build(input_shape)

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({
            "power": self.power,
            "learnable": self.learnable,
            "constraints": self.constraints
        })
        return conf


class FReLU(tfkl.Layer):
    def __init__(self, kernel_size, groups=-1, activation=None, activation_args=None,
                 conv_kwargs=None, renorm=False, **kwargs):
        super().__init__(**kwargs)
        self.renorm = renorm
        self.kernel_size = kernel_size
        self.groups = groups
        self.activation = activation
        self.activation_args = activation_args or dict
        self.conv_kwargs = conv_kwargs or dict

    def build(self, input_shape):
        channel_axis = -1 if K.image_data_format() == 'channels_last' else 1
        filters = input_shape[channel_axis]

        self.conv_ = tfkl.Conv2D(filters=filters,
                                 kernel_size=self.conv_kwargs,
                                 activation=self.activation_args if not isinstance(self.activation, type(tfkl.Layer)) else None,
                                 groups=self.groups if self.groups > 0 else filters,
                                 use_bias=self.activation is not None,
                                 **self.conv_kwargs)
        if isinstance(self.activation, type(tfkl.Layer)):
            self.conv_ = self.activation(**self.activation_args)(self.conv_)

        self.bn_ = tfkl.BatchNormalization(renorm=self.renorm)

        super().build(input_shape)

    def call(self, inputs, **kwargs):
        x1 = self.conv_(inputs)
        x1 = self.bn_(x1)
        return tf.maximum(x1, x)

    def get_config(self):
        conf = super().get_config()
        conf.update({
            'renorm': self.renorm,
            'kernel_size': self.kernel_size,
            'groups': self.groups,
            'activation': self.activation,
            'activation_args': self.activation_args,
            'conv_kwargs': self.conv_kwargs,
        })
        return conf


class PELU(tfkl.Layer):
    """
    Implements Parameterized Exponential Linear Units (PELU) activation:
        PELU(x) = c * x if x > 0 else a * exp(x/b) - 1
    where a, b and c are non-negative constants.

    This implementation allows a, b and c to be learned as network weights during training.
    """
    def __init__(self,
                 alpha_initializer="ones",
                 alpha_regularizer=None,
                 alpha_constraint="nonneg",
                 beta_initializer="ones",
                 beta_regularizer=None,
                 beta_constraint="nonneg",
                 c_initializer="ones",
                 c_regularizer=None,
                 c_constraint="nonneg",
                 shared_axes=None,
                 **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

        self.alpha_initializer = tf.keras.initializers.get(alpha_initializer)
        self.alpha_regularizer = tf.keras.regularizers.get(alpha_regularizer)
        self.alpha_constraint = tf.keras.constraints.get(alpha_constraint)

        self.beta_initializer = tf.keras.initializers.get(beta_initializer)
        self.beta_regularizer = tf.keras.regularizers.get(beta_regularizer)
        self.beta_constraint = tf.keras.constraints.get(beta_constraint)

        self.c_initializer = tf.keras.initializers.get(c_initializer)
        self.c_regularizer = tf.keras.regularizers.get(c_regularizer)
        self.c_constraint = tf.keras.constraints.get(c_constraint)

        if shared_axes is None:
            self.shared_axes = None
        elif not isinstance(shared_axes, (list, tuple)):
            self.shared_axes = [shared_axes]
        else:
            self.shared_axes = list(shared_axes)

    def build(self, input_shape):
        param_shape = list(input_shape[1:])
        if self.shared_axes is not None:
            for i in self.shared_axes:
                param_shape[i - 1] = 1
        self.a = self.add_weight(
            shape=param_shape,
            name='a',
            initializer=self.alpha_initializer,
            regularizer=self.alpha_regularizer,
            constraint=self.alpha_constraint
        )
        self.b = self.add_weight(
            shape=param_shape,
            name='b',
            initializer=self.beta_initializer,
            regularizer=self.alpha_regularizer,
            constraint=self.alpha_constraint
        )
        self.c = self.add_weight(
            shape=param_shape,
            name='c',
            initializer=self.alpha_initializer,
            regularizer=self.alpha_regularizer,
            constraint=self.alpha_constraint
        )
        axes = {}
        if self.shared_axes:
            for i in range(1, len(input_shape)):
                if i not in self.shared_axes:
                    axes[i] = input_shape[i]
        # self.input_spec = tf.keras.layers.InputSpec(ndim=len(input_shape), axes=axes)
        super().build(input_shape)

    def call(self, x, **kwargs):
        pos = self.c * x
        neg = self.a * (tf.math.exp(x / self.b) - 1)
        return tf.where(x > 0, pos, neg)

    def get_config(self):
        conf = super().get_config()
        conf.update({
            'alpha_initializer': tf.keras.initializers.serialize(self.alpha_initializer),
            'alpha_regularizer': tf.keras.regularizers.serialize(self.alpha_regularizer),
            'alpha_constraint': tf.keras.constraints.serialize(self.alpha_constraint),
            'beta_initializer': tf.keras.initializers.serialize(self.beta_initializer),
            'beta_regularizer': tf.keras.regularizers.serialize(self.beta_regularizer),
            'beta_constraint': tf.keras.constraints.serialize(self.beta_constraint),
            'c_initializer': tf.keras.initializers.serialize(self.c_initializer),
            'c_regularizer': tf.keras.regularizers.serialize(self.c_regularizer),
            'c_constraint': tf.keras.constraints.serialize(self.c_constraint),
            'shared_axes': self.shared_axes,
        })
        return conf


# class AttentionPooling(tfkl.Conv2D):
#     def __init__(self, **kwargs):
#         super().__init__(**kwargs)
#
#         self.attention_matrix = tfkl.Conv2D(filters=self.filters,
#                                             kernel_size=self.kernel_size,
#                                             strides=self.strides,
#                                             padding=self.padding,
#                                             activation='sigmoid')
#         self.classication_matrix = tfkl.Conv2D(filters=self.filters,
#                                                kernel_size=self.kernel_size,
#                                                strides=self.strides,
#                                                padding=self.padding,
#                                                activation=None)
#         self.product = tfkl.Multiply()
#
#     def call(self, inputs):
#         attention = self.attention_matrix(inputs)
#         attention = tf.divide(attention, tf.reduce_sum(attention, keepdims=True))
#         classification = self.classication_matrix(inputs)
#         output = tf.multiply(attention, classification)


class MaxOut(tfkl.Layer):
    """
    Implements max-out activation, taking a list of inputs of arbitrary length (but of the same shape) and returning the
    element-wise maximum.
    """
    def __init__(self, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.axis = axis

    def get_config(self):
        conf = super().get_config()
        conf.update({"axis": self.axis})
        return conf

    def call(self, inputs, **kwargs):
        inputs = tf.stack(inputs, axis=-1)
        return tf.reduce_max(inputs, axis=-1)


class GenericNormPool():
    def __init__(self, power=2., pool_size=2, strides=None, padding='valid', data_format=None, **kwargs):
        super().__init__(pool_size=pool_size, strides=strides, padding=padding, data_format=data_format, **kwargs)
        self.power = power

    def build(self, input_shape):
        self.power_var = self.add_weight(name="p", shape=(), trainable=self.trainable,
                                         initializer=tf.keras.initializers.Constant(tf.math.log(self.power)))
        super().build(input_shape)

    def call(self, x):
        pow = tf.math.exp(self.power_var)
        x = tf.pow(super().call(tf.pow(tf.abs(x), pow)), 1. / pow)
        return x

    def get_config(self):
        return {**super().get_config(), 'power': self.power}


class LearnedNormPool1D(GenericNormPool, tfkl.AvgPool1D):
    pass
class LearnedNormPool2D(GenericNormPool, tfkl.AvgPool2D):
    pass
class LearnedNormPool3D(GenericNormPool, tfkl.AvgPool3D):
    pass


class GenericLogMeanExpPool():
    def __init__(self,
                 a=1.,
                 pool_size=2,
                 strides=None,
                 padding='valid',
                 data_format=None,
                 **kwargs):
        super().__init__(pool_size=pool_size, strides=strides, padding=padding, data_format=data_format, **kwargs)
        self.a = a

    def build(self, input_shape):
        self.trainable_a = tf.Variable(initial_value=self.a, dtype=self.dtype, trainable=True)
        super().build(input_shape)

    def call(self, x):
        a = tf.cast(self.trainable_a, x.dtype)
        return tf.math.log(super().call(tf.math.exp(a * x))) / a

    def get_config(self):
        conf = super().get_config()
        conf.update({"a": self.a})
        return conf


class LogMeanExpPool1D(GenericLogMeanExpPool, tfkl.AvgPool1D):
    pass
class LogMeanExpPool2D(GenericLogMeanExpPool, tfkl.AvgPool2D):
    pass
class LogMeanExpPool3D(GenericLogMeanExpPool, tfkl.AvgPool3D):
    pass


class GenericLogConvExpPool():
    def __init__(self,
                 kernel_size=(2, 2),
                 strides=None,
                 groups=1,
                 padding='valid',
                 data_format=None,
                 **kwargs):
        super().__init__(kernel_size=kernel_size,
                         groups=groups,
                         strides=strides,
                         padding=padding,
                         data_format=data_format,
                         kernel_constraint=tf.keras.constraints.UnitNorm(axis=[0, 1, 2]),
                         **kwargs)

    def call(self, inputs):
        outputs = tf.math.log(self.call(tf.math.exp(inputs)))
        return outputs


class LogConvExpPool2D(GenericLogMeanExpPool, tfkl.Conv2D):
    pass


class GlobalWeightedRankPooling(tfkl.Layer):
    def __init__(self, r=.5, *args, **kwargs):
        super(GlobalWeightedRankPooling, self).__init__(*args, **kwargs)
        self.r = r

        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    def build(self, input_shape):
        self.weight = self.add_weight(shape=(1, ), initializer=tf.keras.initializers.Constant(value=self.r),
                                      trainable=self.trainable)
        super(GlobalWeightedRankPooling, self).build(input_shape)  # Be sure to call this at the end

    def func(self, x):
        x = tf.transpose(x)
        x = tf.map_fn(lambda a: tf.sort(K.flatten(a), direction="DESCENDING"), x)
        size = x.shape[1]

        Zr = (tf.pow(self.weight, size) - 1) / (self.weight - 1)
        powers = tf.range(size, dtype=x.dtype)
        r_array = tf.pow(self.weight, powers)

        values = tf.multiply(x, r_array)
        values = tf.reduce_sum(values, axis=-1) / Zr

        return values

    def call(self, x, **kwargs):
        return tf.map_fn(self.func, x)

    def compute_output_shape(self, input_shape):
        return (1, )


class GLU(tfkl.Layer):
    """
    Gated Linear Unit layer:
        GLU(x) = Conv(x) * sigmoid(Conv(x))
    Note that the two conv operations have separate weights, but are implemented as a single Conv layer with twice the
    depth.

    Base arguments are similar to the Conv layers in Tensorflow:
        filters,
        kernel_size,
        strides,
        dilation_rate
        padding
    Other arguments:
        activation: can either be passed similarly to the Conv layer, as a string or a callable function; can also
            be passed as a layer object (not instantiated), with additional arguments passed as a dict to activation_args
        conv_type: specifies the actual conv operation (defaults to Conv2D)
        con_kwargs: optional dict containing additional arguments to the conv layer
        axis: integer specifying the channel axis. Only provided as an option to override the default value determined
            by tf.keras.backend.image_data_format().
    Additional arguments will be absorbed and passed to the Layer class
    """
    def __init__(self,
                 filters,
                 kernel_size,
                 strides=(1, 1),
                 dilation_rate=1,
                 padding="same",
                 activation=None,
                 activation_args=None,
                 conv_kwargs=None,
                 conv_type=tfkl.Conv2D,
                 **kwargs):
        super().__init__(**kwargs)
        self.axis = -1 if K.image_data_format() == "channels_last" else 1
        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.dilation_rate = dilation_rate
        self.padding = padding
        self.activation = activation
        self.activation_args = activation_args or dict()
        self.conv_kwargs = conv_kwargs or dict()
        self.conv_type = conv_type

        self.conv = self.conv(filters=2*self.filters,
                              kernel_size=self.kernel_size,
                              dilation_rate=self.dilation_rate,
                              activation=None,
                              strides=self.strides,
                              padding=self.padding,
                              **self.conv_kwargs)

        if self.activation is not None and isinstance(self.activation, type(tfkl.Layer)):
            self.activation = self.activation(**self.activation_args)

    def build(self, input_shape):
        self.conv.build(input_shape)
        # if activation is passed as a layer object
        if self.activation is not None and isinstance(self.activation, type(tfkl.Layer)):
            self.activation.build(input_shape)
        super().build(input_shape)

    def call(self, inputs, **kwargs):
        x = self.conv(inputs)
        # x1 = x[..., :self.filters]
        # x2 = x[..., self.filters:]
        x1, x2 = tf.split(x, num_split=2, axis=self.axis)

        sig = tf.nn.sigmoid(x1)
        act = self.activation(x2)
        return sig * act

    def compute_output_shape(self, input_shape):
        output_shape = list(input_shape)
        output_shape[1] = (output_shape[1] - self.kernel_size[0]) // self.strides[0] + 1
        output_shape[2] = (output_shape[2] - self.kernel_size[1]) // self.strides[1] + 1
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({
            'filters': self.filters,
            'kernel_size': self.kernel_size,
            'strides': self.strides,
            'dilation_rate': self.dilation_rate,
            'padding': self.padding,
            'activation': self.activation,
            'activation_args': self.activation_args,
            'conv_kwargs': self.conv_kwargs,
            'conv_type': self.conv_type,
            'axis': self.axis,
        })
        return conf


class ModifiedHighway(GLU):
    """
    HighWay activation layer. Similar to GLU, but adds a second term that allows information to flow through with
     only a linear transformation (modified from the original version for dimension matching):

    H(x) = sigmoid(ConvA(x)) * activation(ConvB(x)) + (1 - sigmoid(ConvA(x)) * ConvB(x)
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def call(self, inputs, **kwargs):
        x = self.conv(inputs)
        # x1 = x[..., :self.filters]
        # x2 = x[..., self.filters:]
        x1, x2 = tf.split(x, num_or_size_splits=2, axis=self.axis)

        # x1, x2 = tf.split(x, num_or_size_splits=2, axis=self.axis)

        sig = tf.nn.sigmoid(x1)
        act = self.activation(x2)
        return sig * act + (1 - sig) * x2


class Clip(tfkl.Layer):
    """
    Element-wise minimum of the inputs
    """
    def call(self, inputs, **kwargs):
        res = inputs[0]
        for clip in inputs[1:]:
            res = tf.minimum(res, clip)
        return res


class AlternateClip(tfkl.Layer):
    """
    Element-wise minimum of the inputs
    """
    def call(self, inputs, **kwargs):
        res = tf.stack(inputs, axis=0)
        return tf.reduce_min(res, axis=0)


class LinearSum(tfkl.Layer):
    def build(self, input_shape):
        n = len(input_shape)
        self.coeffs = self.add_weight(name='coeffs', shape=(n, ), trainable=self.trainable, dtype=self.dtype)

        super().build(input_shape)

    def call(self, inputs, *args, **kwargs):
        inputs = tf.stack(inputs, axis=-1)
        coeffs = tf.nn.softmax(self.coeffs)
        out = tf.multiply(inputs, coeffs)
        return tf.reduce_sum(out, axis=-1)


class Quantile(tfkl.Layer):
    def __init__(self, quantiles, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.quantiles = quantiles
        self.axis = axis

    def build(self, input_shape):
        idx = tf.constant(self.quantiles, dtype=self.dtype)
        idx *= tf.cast(input_shape[self.axis], self.dtype)
        self.idx = tf.cast(idx, tf.int32)
        super().build(input_shape)

    def call(self, inputs, **kwargs):
        x = tf.sort(inputs, axis=self.axis)
        q = tf.gather(x, indices=self.idx, axis=self.axis)
        return q

    def compute_output_shape(self, input_shape):
        output_shape = input_shape
        output_shape[self.axis] = len(self.quantiles)
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({"quantiles": self.quantiles, "axis": self.axis})
        return conf


class AttentionActivation(tfkl.Activation):
    def __init__(self, depth_divider=1, inner_activation=None, norm='batch', renorm=False, **kwargs):
        super().__init__(**kwargs)
        self.depth_divider = depth_divider
        self.inner_activation = inner_activation
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1
        self.norm = norm
        self.renorm = renorm

        self.conv1 = None
        self.conv2 = None
        self.layers = []

        self.product = tfkl.Multiply()

    def build(self, input_shape):
        self.conv1 = tfkl.Conv2D(filters=input_shape[self.channel_axis] // self.depth_divider,
                                 kernel_size=1,
                                 activation=self.inner_activation)
        self.conv2 = tfkl.Conv2D(filters=input_shape[self.channel_axis],
                                 kernel_size=1,
                                 activation='sigmoid')
        self.layers = [self.conv1, self.conv2]

        match self.norm:
            case 'batch':
                self.norm1 = tfkl.BatchNormalization(axis=self.channel_axis, renorm=self.renorm)
                self.norm2 = tfkl.BatchNormalization(axis=self.channel_axis, renorm=self.renorm)

                self.layers.insert(1, self.norm1)
                self.layers.insert(-1, self.norm2)
            case 'layer':
                self.norm1 = tfkl.LayerNormalization(axis=self.channel_axis)
                self.norm2 = tfkl.LayerNormalization(axis=self.channel_axis)

                self.layers.insert(1, self.norm1)
                self.layers.insert(-1, self.norm2)

        super().build(input_shape)

    def call(self, x, **kwargs):
        y = self.layers[0](x)
        for layer in self.layers[1:]:
            y = layer(y)
        return self.product([x, y])

    def get_config(self):
        conf = super().get_config()
        conf.update({'depth_divider': self.depth_divider,
                     'inner_activation': self.inner_activation,
                     'batchnorm': self.batchnorm})
        return conf


class TripletAttention(tfkl.Layer):
    def __init__(self, kernel_size, conv_kwargs=None, **kwargs):
        super().__init__(**kwargs)
        self.kernel_size = kernel_size
        self.conv_kwargs = dict() if conv_kwargs is None else conv_kwargs

    def build(self, input_shape):
        conv_kwargs = {'kernel_size': self.kernel_size, 'filters': 1, 'activation': 'sigmoid', **self.conv_kwargs}

        self.conv0 = tfkl.Conv2D(**conv_kwargs)
        self.conv1 = tfkl.Conv2D(**conv_kwargs)
        self.conv2 = tfkl.Conv2D(**conv_kwargs)
        super().build(input_shape)

    @staticmethod
    def zpooling(x, axis):
        return tf.concat([tf.reduce_mean(x, axis, keepdims=True), tf.reduce_max(x, axis, keepdims=True)], axis=axis)

    def call(self, x, **args):
        perm0 = self.zpooling(x, axis=-1)
        perm1 = self.zpooling(tf.transpose(x, perm=[0, 1, 3, 2]), axis=-1)
        perm2 = self.zpooling(tf.transpose(x, perm=[0, 3, 2, 1]), axis=-1)

        conv0 = self.conv0(perm0)
        conv1 = self.conv1(perm1)
        conv2 = self.conv2(perm2)

        conv1 = tf.transpose(conv1, perm=[0, 1, 3, 2])
        conv2 = tf.transpose(conv2, perm=[0, 3, 2, 1])

        out = tf.stack([x * conv0, x * conv1, x * conv2], axis=-1)
        return tf.reduce_mean(out, axis=-1)


class Expand(tfkl.Layer):
    def __init__(self, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.axis = axis

    def call(self, x, **kwargs):
        return K.expand_dims(x, axis=self.axis)

    def compute_output_shape(self, input_shape):
        output_shape = list(input_shape)
        output_shape.insert(self.axis, 1)
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({"axis": self.axis})
        return conf


class Squeeze(tfkl.Layer):
    def __init__(self, axis=None, **kwargs):
        super().__init__(**kwargs)
        self.axis = axis

    def call(self, x, **kwargs):
        return K.squeeze(x, axis=self.axis)

    def compute_output_shape(self, input_shape):
        output_shape = tuple(filter(lambda x: x != 1, input_shape))
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({"axis": self.axis})
        return conf


class MatMul(tfkl.Layer):
    def __init__(self, mask, name='matmul', **kwargs):
        super().__init__(name=name, **kwargs)
        self.mask = mask

        self.mask_ = self.add_weight(shape=mask.shape,
                                     dtype=self.dtype,
                                     name='mask',
                                     trainable=False,
                                     initializer=tf.keras.initializers.Constant(value=tf.cast(self.mask, self.dtype)))
        # self.mask = tf.cast(mask, self.dtype)
        # For now, effectively forced to -1
        # self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    def compute_output_shape(self, input_shape):
        output_shape = input_shape
        output_shape[-1] = self.mask_.shape[-1]

    def call(self, inputs, **kwargs):
        out = tf.matmul(inputs, tf.cast(self.mask_, dtype=inputs.dtype))
        return out

    def get_config(self):
        conf = super().get_config()
        conf.update({'mask': self.mask})
        return conf


class SplitAxis(tfkl.Layer):
    def __init__(self, groups, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.groups = groups
        self.axis = axis

    def call(self, x, **kwargs):
        x = tf.split(x, num_or_size_splits=self.groups, axis=self.axis)
        x = tf.stack(x, axis=self.axis)
        return x

    def compute_output_shape(self, input_shape):
        output_shape = input_shape
        output_shape[self.axis] = output_shape[self.axis] // self.groups
        output_shape.insert(self.axis, self.groups)
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({
            "groups": self.groups,
            "axis": self.axis
        })
        return conf


class Print(tfkl.Layer):
    """
    Helper layer simply meant to display whatever passes through it.

    Any logic in the call method will be performed and printed, but the inputs are returned without modifications.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def call(self, x, **kwargs):
        tf.print(tf.reduce_min(x), tf.reduce_max(x))
        return x


class Indicator(tfkl.Layer):
    """
    Implements indicator function as a layer, specifically:
        x, y: y if x > 0 else 0

    Works for each sample separately, and with an optional axis argument to ignore one or more other axis/axes.

    Expects a list of length 2, e.g.: Indicator(axis=-1)([x, y])

    Lists of length > 2 will work, but all elements after the first 2 are ignored.
    """
    def __init__(self, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.axis = axis
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    def build(self, input_shape):
        # all but batch and chosen axis
        self.axes = list(range(1, len(input_shape)))
        self.axes.pop(self.axis)
        super().build(input_shape)

    def call(self, x, **kwargs):
        return tf.where(tf.reduce_sum(x[0], axis=self.axes, keepdims=True) > 0., x[1], tf.zeros_like(x[1]))

    def get_config(self):
        conf = super().get_config()
        conf.update({"axis": self.axis})
        return conf

    def compute_output_shape(self, input_shape):
        return input_shape


class OvODecodingLayer(tfkl.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

        self._built = False
        self.M = None
        self.k = None

    @staticmethod
    def one_vs_one_code_matrix(K):
        cols = list(range(K * (K - 1) // 2))
        rows_neg = flatten([list(range(j + 1, K)) for j in range(K)])
        rows_pos = flatten([[j] * (K - j - 1) for j in range(K - 1)])

        indices = tf.constant([rows_pos + rows_neg,
                               cols + cols],
                              dtype=tf.int64)
        indices = tf.transpose(indices)
        values = tf.ones(shape=(K * (K - 1) // 2,))
        values = tf.concat([values, -values], axis=0)

        M = tf.sparse.SparseTensor(indices=indices,
                                   values=values,
                                   dense_shape=(K, K * (K - 1) // 2))
        M = tf.sparse.reorder(M)

        return tf.sparse.to_dense(M)

    def _build(self, input_shape):
        self.k = int((1 + math.sqrt(1 + 8 * input_shape[self.channel_axis])) / 2)
        self.M = self.one_vs_one_code_matrix(self.k)
        self._built = True

    def decoding(self, x):
        if self.channel_axis == -1:
            decoded = tf.matmul(x, self.M, transpose_b=True)
        else:
            decoded = tf.matmul(self.M, x, transpose_b=True)
        return decoded

    def call(self, x, **kwargs):
        if not self._built:
            self._build(x.shape)

        return self.decoding(x)

    def compute_output_shape(self, input_shape):
        output_shape = list(input_shape)
        output_shape[self.channel_axis] = self.k
        return output_shape


class MultiLossLearnableWeighting(tfkl.Layer):
    """
    This layer is intended to add a weighted sum of multiple losses to a model with the weighting coefficients learned
    during model training.

    To work, the model should be passed the ground truth corresponding to each output along the regular inputs.
    """

    def __init__(self, losses, **kwargs):
        self.nb_outputs = len(losses)
        self.loss_list = losses
        self.is_placeholder = True
        super().__init__(**kwargs)

    def build(self, input_shape):
        self.vars = [self.add_weight(name=f'var_{i}',
                                     shape=(1, ),
                                     initializer="ones",
                                     constraint="nonneg",
                                     trainable=True) for i in range(self.nb_outputs)]
        super().build(input_shape)

    def multi_loss(self, ys_true, ys_pred):
        if not (len(ys_true) == self.nb_outputs and len(ys_pred) == self.nb_outputs):
            raise ValueError(f"Lists of outputs did not match the expected lengths: "
                             f"len(y_true)={len(ys_true)} and len(y_pred)={len(ys_pred)}, "
                             f"but nb_outputs={self.nb_outputs}")
        loss_values = [loss(ys_true[k], ys_pred[k]) for k, loss in self.loss_list.items()]

        weighted = [loss / (2 * var**2) for loss, var in zip(loss_values, self.vars)]
        reg_term = tf.math.log(K.prod([1+v**2 for v in self.vars]))

        return tf.reduce_mean(weighted) + reg_term

    @tf.function
    def call(self, inputs, **kwargs):
        ys_true = inputs[0]
        ys_pred = inputs[1]
        loss = self.multi_loss(ys_true, ys_pred)
        self.add_loss(loss, inputs=inputs)
        # We won't actually use the output.
        return ys_pred

    def get_config(self):
        conf = super().get_config()
        conf.update({
            'loss_list': self.loss_list,
            'nb_outputs': self.nb_outputs
        })
        return conf


class AddPositionMapping(tfkl.Layer):

    """
    Adds a positional mapping to the input, padding the channel axis by 1 per specified axis.
    The positional mapping gives context for later layers by adding a linspace operation between specified min and
    max, and adding this to the channel axis of the input.

    Alternatively, the mapping can instead be added to the input instead of concatenated.
    """
    def __init__(self, axis, type='concat', min=0., max=1., **kwargs):
        """
        :param axis: Axis (or axes) to add mappings for (e.g. 1 for the first non-batch, non-channel axis).
        :param type: Operation to use. Either 'concat' (mapping will be concatenated to the end of the channel axis)
            or 'add' (mapping will be added to all the inputs at the respective locations).
        :param min: Minimum value of the mapping
        :param max: Maximum value of the mapping
        :param kwargs: Additional Layer class arguments, passed to the super() method.
        """
        super().__init__(**kwargs)
        self.min = min
        self.max = max
        self.axis = axis
        self.type = type
        if not hasattr(self.axis, '__len__'):
            self.axis = [self.axis]
        if not hasattr(self.min, '__len__'):
            self.min = [self.min] * len(self.axis)
        if not hasattr(self.max, '__len__'):
            self.max = [self.max] * len(self.axis)

        self.channel_axis = -1 if K.image_data_format() == 'channels_last' else 1
        if self.type == 'concat':
            self.op = tfkl.Concatenate(axis=self.channel_axis)
        elif self.type == "add":
            self.op = tfkl.Add()

    def build(self, input_shape):
        self.positional_mappings = []
        self.axis = [_ if _ >= 0 else len(input_shape) + _ for _ in self.axis]
        for i, ax in enumerate(self.axis):
            shape = [1] * len(input_shape)
            shape[ax] = input_shape[ax]
            temp = tf.cast(tf.linspace(start=self.min[i], stop=self.max[i], num=input_shape[ax]), dtype=self.dtype)
            temp = tf.reshape(temp, shape)
            for ax2, dim in enumerate(input_shape):
                if ax2 not in [0, self.channel_axis if self.channel_axis == 1 else len(input_shape) - 1, ax]:
                    temp = tf.repeat(temp, axis=ax2, repeats=dim)
            self.positional_mappings.append(temp)
        super().build(input_shape)

    @tf.function
    def call(self, inputs, **kwargs):
        if tf.shape(self.positional_mappings[0])[0] == 1 and inputs.shape[0] is not None:
            self.positional_mappings = [tf.repeat(_, axis=0, repeats=inputs.shape[0]) for _ in self.positional_mappings]
        return self.op([inputs, *self.positional_mappings])

    def get_config(self):
        conf = super().get_config()
        conf.update({'axis': self.axis, 'type': self.type, 'min': self.min, 'max': self.max})
        return conf


class Spectrogram(tfkl.Layer):
    def __init__(self,
                 frame_length,
                 frame_step,
                 fft_length=None,
                 window_fn=tf.signal.hamming_window,
                 pad_end=True,
                 power=1.,
                 phase=False,
                 **kwargs):
        self.frame_length = frame_length
        self.frame_step = frame_step
        self.fft_length = fft_length or self.frame_length
        self.window_fn = window_fn
        self.pad_end = pad_end
        self.power = power
        self.phase = phase

        self.channel_axis = -1 if K.image_data_format() == 'channels_last' else 1

        super().__init__(**kwargs)

    def get_config(self):
        conf = super().get_config()
        conf.update({'frame_length': self.frame_length,
                     'frame_step': self.frame_step,
                     'fft_length': self.fft_length,
                     'window_fn': self.window_fn,
                     'pad_end': self.pad_end,
                     'power': self.power,
                     'phase': self.phase})
        return conf

    @tf.function
    def call(self, inputs, *args, **kwargs):
        if self.channel_axis == -1:
            inputs = tf.transpose(inputs, perm=(0, 2, 1))

        spec = tf.signal.stft(
            signals=inputs,
            frame_length=self.frame_length,
            frame_step=self.frame_step,
            fft_length=self.fft_length,
            pad_end=self.pad_end,
            window_fn=self.window_fn
        )

        if self.channel_axis == -1:
            spec = tf.transpose(spec, perm=(0, 2, 3, 1))

        out = tf.abs(spec) ** self.power

        # if self.phase:
        #     out = tf.concat([out, tf.math.angle(spec)], axis=self.channel_axis)

        return out


class VectorSTFT(tf.keras.layers.Layer):
    """
    Creates a STFT matrix to apply to any signal of a given length.
    Can be applied over batches or multichannel as long as the signals are of the form [..., samples].
    Returns a STFT matrix of shape [...[,channels], freq_bins, time_bins]
    """
    def __init__(self,
                 frame_length,
                 fft_length=None,
                 frame_step=None,
                 window_fn=tf.signal.hamming_window,
                 pad_end=True,
                 dtype=tf.float32,
                 power=1.,
                 **kwargs):
        super().__init__(dtype=dtype, name='VectorSTFT', **kwargs)

        self.channel_axis = -1 if K.image_data_format() == 'channels_last' else 1
        self.axis = 1 if self.channel_axis == -1 else 2

        self.power = power

        self.frame_length = frame_length
        self.window_fn = window_fn
        self.pad_end = pad_end
        # defaults to ~75% overlap
        self.frame_step = frame_step or self.frame_length // 4
        # defaults to the nearest power of 2 above self.frame_length
        self.fft_length = fft_length or int(2 ** tf.math.ceil(tf.math.log(tf.cast(self.frame_length, tf.float32)) / tf.math.log(2.)))

    def p(self):
        """
        Creates a zero-padding sparse matrix for vector STFT
        """
        return tf.SparseTensor(indices=tf.constant(value=[(i, i) for i in range(self.frame_length)], dtype=tf.int64),
                               values=tf.ones(shape=self.frame_length,  dtype=self.dtype),
                               dense_shape=(self.fft_length, self.frame_length))

    def w(self, inverse=False):
        val = self.window_fn(window_length=self.frame_length)
        if inverse:
            val = 1/val
        return tf.SparseTensor(indices=tf.constant(value=[(i, i) for i in range(self.frame_length)], dtype=tf.int64),
                               values=tf.cast(val, self.dtype),
                               dense_shape=(self.frame_length, self.frame_length))

    def f(self):
        f = tf.cast(tf.tensordot(tf.range(self.fft_length), tf.range(self.fft_length), axes=0), dtype=self.dtype)
        f *= - 2 * math.pi / self.fft_length
        f = tf.complex(real=tf.cos(f), imag=tf.sin(f))

        return f

    def build(self, input_shape):
        f = self.f()
        p = self.p()
        w = self.w(inverse=False)
        a = tf.linalg.matmul(tf.sparse.to_dense(p), tf.sparse.to_dense(w))
        self.a = tf.linalg.matmul(f, tf.cast(a, f.dtype))
        self.a = self.a[:(self.fft_length // 2 + 1)]

        super().build(input_shape)

    def call(self, inputs, **kwargs):
        # self.a combines the three operations of Windowing, FFT-padding, and Zero-Padding
        res = tf.matmul(
            self.a,
            tf.signal.frame(
                tf.cast(inputs, dtype=self.a.dtype),
                frame_step=self.frame_step,
                frame_length=self.frame_length,
                pad_end=self.pad_end,
                axis=self.axis
            )
        )

        return tf.abs(res) ** self.power


class TrainableMelFilterbank(tfkl.Layer):
    def __init__(self, n_mels,
                 fmin,
                 fmax,
                 a=0.00024,
                 b=0.741,
                 norm='slaney',
                 htk=True,
                 trainable=True,
                 librosa=True,
                 lateral_inhibition=False,
                 **kwargs):
        super().__init__(trainable=trainable, **kwargs)
        self.htk = htk
        if self.htk:
            self.a = 2595.
            self.b = 700.
        else:
            self.a = a
            self.b = b
        self.n_mels = n_mels
        self.fmin = float(fmin)
        self.fmax = float(fmax)
        self.norm = norm
        self.librosa = librosa
        self.lateral_inhibition = lateral_inhibition

        self.a_weight = self.add_weight(name='a', shape=(), dtype=self.dtype, trainable=self.trainable, initializer=tf.initializers.Constant(value=self.a))
        self.b_weight = self.add_weight(name='b', shape=(), dtype=self.dtype, trainable=self.trainable, initializer=tf.initializers.Constant(value=self.b))

        self.hz_to_mel = self.htk_hz_to_mel if self.htk else self.generic_hz_to_mel
        self.mel_to_hz = self.htk_mel_to_hz if self.htk else self.generic_mel_to_hz

        self.filter_ = None

    def build(self, input_shape):
        self.steps = input_shape[2 if K.image_data_format() == 'channels_last' else 3]

        if self.librosa and not self.trainable:
            self.filter_ = mel(sr=2*self.fmax,
                               n_mels=self.n_mels,
                               n_fft=int(2*(self.steps - 1)),
                               fmin=self.fmin,
                               fmax=self.fmax,
                               norm=self.norm,
                               htk=self.htk)
            if self.lateral_inhibition:
                lateral_inhibition_coefficients = tf.concat([tf.constant([0, 0], dtype=self.filter_.dtype),
                                                             [tf.tensordot(self.filter_[i], self.filter_[i - 1], axes=(0, 0)) for
                                                              i in range(1, self.filter_.shape[0])],
                                                             tf.constant([0, 0], dtype=self.filter_.dtype)],
                                                            axis=0)
                lateral_inhibition_coefficients = lateral_inhibition_coefficients[1:-1] / (
                            lateral_inhibition_coefficients[2:] + lateral_inhibition_coefficients[:-2])
                lateral_inhibition_coefficients = tf.expand_dims(lateral_inhibition_coefficients, axis=-1)
                self.filter_ = tf.subtract(
                    self.filter_,
                    tf.add(
                        tf.multiply(lateral_inhibition_coefficients[1:], tf.roll(self.filter_, axis=0, shift=-1)),
                        tf.multiply(lateral_inhibition_coefficients[:-1], tf.roll(self.filter_, axis=0, shift=1)),
                    )
                )

        else:
            self.fft_frequencies = tf.linspace(start=self.fmin, stop=self.fmax, num=self.steps)
            self.fft_frequencies = tf.cast(self.fft_frequencies, self.dtype)
            self.fft_frequencies = tf.reshape(self.fft_frequencies, (1, -1))
            self.filter_ = self.mel_filter()

        super().build(input_shape)

    @tf.function
    def generic_hz_to_mel(self, f):
        return tf.math.divide_no_nan(f, self.a_weight * f + self.b_weight)

    @tf.function
    def generic_mel_to_hz(self, f):
        return tf.multiply(self.b_weight, tf.math.divide_no_nan(f, 1. - tf.multiply(self.a_weight, f)))

    @tf.function
    def htk_hz_to_mel(self, f):
        return tf.multiply(self.a_weight,
                           tf.math.divide(tf.math.log1p(tf.math.divide_no_nan(f, self.b_weight)),
                                          tf.math.log(10.)))

    @tf.function
    def htk_mel_to_hz(self, f):
        return tf.multiply(self.b_weight, tf.pow(10., tf.math.divide_no_nan(f, self.a_weight)) - 1.)

    @tf.function
    def mel_filter(self):
        mel_frequencies = self.mel_to_hz(f=tf.linspace(self.hz_to_mel(self.fmin), self.hz_to_mel(self.fmax), self.n_mels + 2))
        mel_frequencies = tf.cast(mel_frequencies, self.dtype)
        mel_frequencies = tf.reshape(mel_frequencies, shape=(-1, 1))

        fdiff = tf.subtract(mel_frequencies[1:], mel_frequencies[:-1])
        ramps = tf.subtract(mel_frequencies, self.fft_frequencies)

        weights = tf.maximum(0., tf.minimum(-ramps[:self.n_mels] / fdiff[:self.n_mels], ramps[2:] / fdiff[1:]))

        if self.norm == 'slaney':
            # normalizes weights so all filters have the same total energy
            weights = tf.math.divide_no_nan(2 * weights,
                                            tf.subtract(mel_frequencies[2:self.n_mels + 2],
                                                        mel_frequencies[:self.n_mels]))

        weights = tf.maximum(0., weights)

        if self.lateral_inhibition:
            lateral_inhibition_coefficients = tf.concat([tf.constant([0, 0], dtype=weights.dtype),
                                                         [tf.tensordot(weights[i], weights[i - 1], axes=(0, 0)) for i in range(1, weights.shape[0])],
                                                         tf.constant([0, 0], dtype=weights.dtype)],
                                                        axis=0)
            lateral_inhibition_coefficients = lateral_inhibition_coefficients[1:-1] / (lateral_inhibition_coefficients[2:] + lateral_inhibition_coefficients[:-2])
            lateral_inhibition_coefficients = tf.expand_dims(lateral_inhibition_coefficients, axis=-1)
            weights = tf.subtract(
                weights,
                tf.add(
                    tf.multiply(lateral_inhibition_coefficients[1:], tf.roll(weights, axis=0, shift=-1)),
                    tf.multiply(lateral_inhibition_coefficients[:-1], tf.roll(weights, axis=0, shift=1)),
                )
            )

        return weights

    @tf.function
    def call(self, inputs, *args, **kwargs):
        filter_ = self.mel_filter() if self.trainable else self.filter_
        return tf.maximum(0., tf.matmul(filter_, inputs))

    def get_config(self):
        conf = super().get_config()
        conf.update({'a': self.a,
                     'b': self.b,
                     'n_mels': self.n_mels,
                     'htk': self.htk,
                     'norm': self.norm,
                     'fmin': self.fmin,
                     'fmax': self.fmax,
                     'lateral_inhibition': self.lateral_inhibition,
                     })
        return conf


class WaveformFilters(tfkl.Layer):
    def __init__(self, pre, norm=True, remove_offset=False, trainable=False, mean=0.0, stddev=.5, **kwargs):
        super().__init__(trainable=trainable, **kwargs)
        self.pre = pre
        self.norm = norm
        self.remove_offset = remove_offset
        self.mean = mean
        self.stddev = stddev

        self.channel_axis = -1 if K.image_data_format() == 'channels_last' else 1
        self.width_axis = -2 if K.image_data_format() == 'channels_last' else -1

        self.call_func = self.build_call_func()

    def build(self, input_shape):
        shape = [1] * (len(input_shape) - 1)
        if self.trainable:
            shape[0] = int(1 + self.pre)
            initializer = tf.keras.initializers.TruncatedNormal(mean=self.mean, stddev=self.stddev)
        else:
            shape[0] = 2
            initializer = tf.keras.initializers.Constant(value=[-self.pre, 1.])

        self.preemphasis_filter = self.add_weight(
            shape=tuple(shape),
            name='pre',
            trainable=self.trainable,
            initializer=initializer,
            dtype=self.dtype,
        )

        self.in_channels = input_shape[self.channel_axis]
        # if self.channel_axis == -1:
        #     self.channel_axis = len(shape) - 1

        super().build(input_shape)

    def build_call_func(self):
        @tf.function
        def call_func(inputs, filt):
            filt = tf.repeat(filt, repeats=self.in_channels, axis=self.channel_axis)

            padding_shape = [[0, 0]] * len(inputs.shape)
            padding_shape[self.width_axis] = [len(filt) - 1 if self.trainable else 1, 0]

            filt = tf.expand_dims(filt, axis=1 if self.channel_axis == -1 else 2)
            return tf.nn.conv1d(input=tf.pad(inputs, padding_shape),
                                filters=tf.cast(filt, inputs.dtype),
                                stride=1,
                                padding='VALID')

        def make_trainable(f):
            @tf.function
            def inner(inputs, filt):
                filt = tf.math.divide_no_nan(filt, filt[-1])
                filt = tf.math.divide_no_nan(filt, tf.reduce_sum(filt, axis=0, keepdims=True))
                return f(inputs, filt)

            return inner

        def make_norm(f):
            @tf.function
            def inner(inputs, filt):
                return tf.math.divide_no_nan(f(inputs, filt),
                                             tf.reduce_max(tf.abs(inputs), axis=range(1, len(inputs.shape) - 1), keepdims=True))
            return inner

        if self.norm:
            call_func = make_norm(call_func)
        if self.trainable:
            call_func = make_trainable(call_func)

        return call_func

    @tf.function
    def call(self, inputs, *args, **kwargs):
        return self.call_func(inputs, self.preemphasis_filter)

    def get_config(self):
        conf = super().get_config()
        conf.update({'pre': self.pre, 'remove_offset': self.remove_offset, 'norm': self.norm, 'mean': self.mean, 'stddev': self.stddev})
        return conf


if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt
    import soundfile as sf
    from utilities.functions import preemphasis

    x = WaveformFilters(pre=-1., trainable=False, norm=True)
    y = Spectrogram(frame_length=2400, fft_length=2400, frame_step=600, phase=False)
    z = TrainableMelFilterbank(n_mels=120, fmin=0, fmax=24000, trainable=False)

    wav, sr = sf.read('../test_audio/SM4A_20200113_083000-Feisty-test.wav')
    test = wav.astype('float32')
    wav = tf.cast(wav, tf.float32)
    test1 = preemphasis(test.copy(), pre=-1., normalise=True, axis=0)
    test2 = preemphasis(test.copy(), pre=1., normalise=True, axis=0)
    wav = tf.expand_dims(wav, axis=0)
    wav_x1 = WaveformFilters(pre=-1., trainable=False, norm=True)(wav)
    wav_x2 = WaveformFilters(pre=1., trainable=False, norm=True)(wav)
    print(wav_x1.shape, wav_x2.shape, test1.shape, test2.shape, wav.shape)

    def func(x):
        return x / np.max(np.abs(x))

    fig, axs = plt.subplots(nrows=2, ncols=3)
    axs[0, 0].plot(test1[:, 0])
    axs[0, 1].set_title('test2')
    axs[0, 1].plot(test2[:, 0])
    axs[1, 0].plot(wav_x1[0, :, 0])
    axs[1, 1].plot(wav_x2[0, :, 0])
    axs[0, 2].plot(test1[:, 0] - wav_x1[0, :, 0])
    axs[1, 2].plot(test2[:, 0] - wav_x2[0, :, 0])

    # axs[0, 0].plot(func(test1[:, 0]) - func(wav_x1[0, :, 0]), alpha=.5)
    # axs[0, 1].plot(func(test1[:, 0]) - func(wav_x2[0, :, 0]), alpha=.5)
    # axs[1, 0].plot(func(test2[:, 0]) - func(wav_x1[0, :, 0]), alpha=.5)
    # axs[1, 1].plot(func(test2[:, 0]) - func(wav_x2[0, :, 0]), alpha=.5)
    plt.show()
