import os
import tensorflow as tf
from datetime import datetime

from modules.custom_callbacks import LearningRateRangeFinder, PlotModel, LateStopping, PerformanceLossWeighting, FileLog, AugmentedLogger


def lr_finder_callback_func(path,
                            lr_min=1e-10,
                            lr_max=10,
                            model_name='model'
                            ):
    path = os.path.join(path, datetime.now().strftime('%Y%m%d_%H%M%S'))
    if not os.path.exists(path):
        os.makedirs(path)

    stop_if_nan = tf.keras.callbacks.TerminateOnNaN()
    lr_finder = LearningRateRangeFinder(filename=f"{path}/lr_range.csv",
                                        lr_min=lr_min,
                                        lr_max=lr_max)
    plot = PlotModel(to_file=f'{path}/{model_name}.png')

    return [stop_if_nan, lr_finder, plot]


def training_callback_func(path,
                           patience,
                           monitor="val_loss",
                           mode="auto",
                           min_delta=1e-4,
                           verbose=0,
                           min_epochs=0,
                           cycle=1,
                           save_weights_only=True,
                           save_best_only=False,
                           loss_weighthing_monitor=None,
                           loss_weighting_alpha=0.,
                           loss_weighting_gamma=0,
                           tensorboard=False,
                           model_name='model'
                           ):

    path = f'{path}/{datetime.now().strftime("%Y%m%d_%H%M%S")}'
    os.makedirs(name=path)

    # Stops in case of NA values
    nan_terminate = tf.keras.callbacks.TerminateOnNaN()

    # Stops when the network stops improving
    # NB: LateStopping is a modification of the EarlyStopping callback that adds two arguments:
    # min_epochs prevents stopping the training before that many epochs have passed
    # cycle only allows stopping on multiples of its value (e.g. if conditions are met to stopped on epoch 20 but cycle = 6,
    # the training will be stopped at epoch 24 if no better performance is reached)
    if min_epochs > 0:
        early_stop = LateStopping(monitor=monitor,
                                  patience=patience,
                                  min_delta=min_delta,
                                  mode=mode,
                                  verbose=verbose,
                                  cycle=cycle,
                                  min_epochs=min_epochs)
    else:
        early_stop = tf.keras.callbacks.EarlyStopping(monitor=monitor,
                                                      patience=patience,
                                                      min_delta=min_delta,
                                                      mode=mode,
                                                      verbose=verbose)

    # Log run information
    csv_logger = AugmentedLogger(filename=os.path.join(path, 'training.csv'))
    # Log a plot of the model
    plot = PlotModel(to_file=f'{path}/{model_name}.png')
    # Log the config file
    conf_log = FileLog(path=f'{os.path.dirname(os.path.dirname(os.path.realpath(__file__)))}/config.py', to=path)

    # Checkpoints
    checkpoint = tf.keras.callbacks.ModelCheckpoint(
        filepath=f'{path}/weights.tf' if save_best_only else (f'{path}/checkpoints' + '/ep/{epoch:02d}.tf'),
        monitor=monitor,
        mode=mode,
        save_weights_only=save_weights_only,
        save_best_only=save_best_only)
    # checkpoint = AverageModelCheckpoint(
    #     filepath=f'{path}/weights.tf' if save_best_only else (f'{path}/checkpoints' + '/ep/{epoch:02d}.tf'),
    #     monitor=monitor,
    #     mode=mode,
    #     save_weights_only=save_weights_only,
    #     save_best_only=save_best_only,
    #     update_weights=False,
    # )

    callback_list = [
        early_stop,
        csv_logger,
        nan_terminate,
        plot,
        conf_log,
        checkpoint,
    ]

    # optionally allows creation of a Tensorboard dashboard to monitor the network
    # NB: currently does not quite work
    if tensorboard:
        callback_list.append(
            tf.keras.callbacks.TensorBoard(log_dir=os.path.join(path, "logs"), profile_batch='2, 10')
        )

    # tested two different loss weighting schemes for multitask learning
    if loss_weighthing_monitor is not None:
        dlw = PerformanceLossWeighting(
            monitor=loss_weighthing_monitor,
            alpha=loss_weighting_alpha,
            gamma=loss_weighting_gamma
        )
        callback_list.append(dlw)

    return callback_list


