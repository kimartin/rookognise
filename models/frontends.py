"""
Note: for the sake of modularity, all front-end functions here are meant to work like so:
    - provide the arguments to build the front-end architecture
    - the function defines an inner function that takes an input x
    - this function can be passed to another function to build the actual network
"""


from typing import Callable
import tensorflow as tf
tfkl = tf.keras.layers
K = tf.keras.backend
from utilities.functions import restrict_kwargs
from modules.custom_modules import multi_scale_dense_module, conv_batchnorm_layer, pool_function, resnext_layer


def ConvPoolFrontEnd(filters: int = 64,
                     ratio: tuple = (1, 1, 1, 2, 2, 2),
                     kernel_size: tuple = (3, 3),
                     pool_every: int = 3,
                     pool_size: tuple = (1, 3),
                     pool_type: str | float = "max",
                     activation: str | type[tfkl.Layer] | Callable = tfkl.LeakyReLU,
                     padding: tuple = (0),
                     causal: bool = False,
                     norm: str = 'renorm',
                     squeeze_excite_r: float = 0.,
                     squeeze_excite_op: str = 'avg',
                     conv_type: type[tfkl.Layer] = tfkl.Conv2D,
                     conv_kwargs: None | dict = None,
                     activation_args: None | dict = None,
                     **kwargs,
                     ):
    """
    # architecture based on 10.23919/EUSIPCO.2017.8081512

    Creates a SparrowNet front-end as a linear stack of convolution and pooling layers.
    The actual layer stack is based on the sparrow submission from Grill & Schlüter (2017) to the Bird Audio
    Detection Challenge.

    The default arguments values build the Rookognise system submitted in the paper.
    Note that they work with any input size as the last convolutional layer summarises the entire remaining frequency
    range, but this can result in a massive convolutional kernel.
    """
    activation_args = activation_args or dict()
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    if activation == tfkl.LeakyReLU:
        activation_args = {"alpha": 0.01}

    def inner_func(x):
        for i, r in enumerate(ratio):
            if i == len(ratio) - 1:
                freq_summary_kernel = (3, x.shape[2 if channel_axis == -1 else -1] - 2)

            x = conv_batchnorm_layer(x,
                                     filters=int(filters * r),
                                     kernel_size=kernel_size if i != len(ratio) - 1 else freq_summary_kernel,
                                     activation=activation,
                                     activation_args=activation_args,
                                     causal=causal,
                                     norm=norm,
                                     padding=padding,
                                     conv_kwargs=conv_kwargs,
                                     squeeze_excite_r=squeeze_excite_r,
                                     squeeze_excite_op=squeeze_excite_op,
                                     conv_type=conv_type
                                     )
            if (i + 1) % pool_every == 0 or i == len(ratio) - 1:
                x = pool_function(x,
                                  pool_type=pool_type,
                                  pool_size=pool_size,
                                  activation=None,
                                  channel_axis=channel_axis)

        # This is the original structure of the frontend, kept for reference
        # x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, **general_args)
        # x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, **general_args)
        # x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, **general_args)
        # x = pool_function(x, pool_type=pool_type, pool_size=pool_size, activation=None, channel_axis=channel_axis)
        # x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, **general_args)
        # x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, **general_args)
        # freq_summary_kernel = (x.shape[1 if channel_axis == -1 else -1] - 2, 3)
        # x = conv_batchnorm_layer(x, filters=filters, kernel_size=freq_summary_kernel, **general_args)
        # x = pool_function(x, pool_type=pool_type, pool_size=pool_size, activation=None, channel_axis=channel_axis)

        return x
    return inner_func


def ConvPoolFrontEndV2(filters: int = 64,
                       ratio: tuple = (1, 2, 2, 2, 3, 3),
                       kernel_size: tuple = (3, 3),
                       pool_every: int = 3,
                       pool_size: tuple = (1, 3),
                       pool_type: str | float = "max",
                       activation: str | type[tfkl.Layer] | Callable =tfkl.LeakyReLU,
                       activation_args: None | dict = None,
                       padding: tuple = (0),
                       causal: bool = False,
                       norm: str = 'renorm',
                       squeeze_excite_r: float = 0.,
                       squeeze_excite_op: str = 'avg',
                       conv_type: type[tfkl.Layer] = tfkl.Conv2D,
                       conv_kwargs: None | dict = None,
                       **kwargs
                       ):
    """
    Creates a SparrowNet front-end as a linear stack of convolution and pooling layers.
    The actual layer stack is based on the sparrow submission from Grill & Schlüter (2017) to the Bird Audio
    Detection Challenge.

    A slight difference is that the last pooling layer can come before the last convolutional layer, so
    the last kernel that summarises over the remaining frequency axis is smaller than with the first version.
    """
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    if activation == tfkl.LeakyReLU and not activation_args:
        activation_args = {"alpha": 0.01}

    arg_dict = dict(
        activation=activation,
        activation_args=activation_args,
        causal=causal,
        norm=norm,
        padding=padding,
        conv_kwargs=conv_kwargs,
        squeeze_excite_r=squeeze_excite_r,
        squeeze_excite_op=squeeze_excite_op,
        conv_type=conv_type
    )

    def inner_func(x):
        for i, r in enumerate(ratio):
            if (i + 1) % pool_every == 0:
                x = pool_function(x,
                                  pool_type=pool_type,
                                  pool_size=pool_size,
                                  activation=None,
                                  channel_axis=channel_axis)

            if i == len(ratio) - 1:
                freq_summary_kernel = (3, x.shape[2 if channel_axis == -1 else -1])

            x = conv_batchnorm_layer(x,
                                     kernel_size=kernel_size if i < len(ratio) - 1 else freq_summary_kernel,
                                     filters=int(filters * r),
                                     **arg_dict)(x)

        return x
    return inner_func


def MSDFrontEnd(
        filters: int = 32,
        kernel_size: tuple = (3, 3),
        pool_size: tuple = (1, 2),
        activation: str | type[tfkl.Layer] | Callable = tfkl.LeakyReLU,
        activation_args: None | dict = None,
        pool_type: str | float ='max',
        norm: str = 'renorm',
        causal: bool = False,
        squeeze_excite_r: float = 0.,
        squeeze_excite_op: str = 'avg',
        conv_type: type[tfkl.Layer] = tfkl.Conv2D,
        unit_layer: Callable | type[tfkl.Layer] = conv_batchnorm_layer,
        conv_kwargs: None | dict = None,
        depth: int = 3,
        width: int = 3,
        c: float = 1.,
        **kwargs
):
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    if activation == tfkl.LeakyReLU and not activation_args:
        activation_args = {"alpha": 0.01}

    # Arguments used by all sparrow layers
    general_args = dict(
        activation=activation,
        activation_args=activation_args,
        causal=causal,
        conv_kwargs=conv_kwargs,
        norm=norm,
        conv_type=conv_type,
        squeeze_excite_r=squeeze_excite_r,
        squeeze_excite_op=squeeze_excite_op,
    )

    msd_args = dict(
        growth=filters,
        width=width,
        depth=depth,
        c=c,
        unit_layer=unit_layer,
        **restrict_kwargs(kwargs, unit_layer, padding='same')
    )

    pool_args = dict(
        pool_type=pool_type,
        pool_size=pool_size,
        activation=None,
        channel_axis=channel_axis,
    )

    def inner_func(x):
        x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, padding='same', **general_args)
        x = pool_function(x, **pool_args)
        x = multi_scale_dense_module(x, kernel_size=kernel_size, **msd_args, **general_args)
        x = pool_function(x, **pool_args)
        x = multi_scale_dense_module(x, kernel_size=kernel_size, **msd_args, **general_args)
        x = pool_function(x, **pool_args)
        x = multi_scale_dense_module(x, kernel_size=kernel_size, **msd_args, **general_args)
        x = pool_function(x, **pool_args)
        freq_summary_kernel = (3, x.shape[2 if channel_axis == -1 else -1])
        x = conv_batchnorm_layer(x, filters=filters, kernel_size=freq_summary_kernel, padding=[0], **general_args)

        return x
    return inner_func


def MSDNeXtFrontend(
        filters: int = 32,
        kernel_size: tuple = (3, 3),
        pool_size: tuple = (1, 2),
        activation: str | type[tfkl.Layer] | Callable = tfkl.LeakyReLU,
        activation_args: None | dict = None,
        pool_type: str | float = 'max',
        norm: str = 'renorm',
        causal: bool = False,
        squeeze_excite_r: float = 0.,
        squeeze_excite_op: str = 'avg',
        conv_type: type[tfkl.Layer] = tfkl.Conv2D,
        unit_layer: Callable | type[tfkl.Layer] = conv_batchnorm_layer,
        conv_kwargs: None | dict = None,
        depth: int = 3,
        width: int = 3,
        c: float = 1.,
        **kwargs
):
    return MSDFrontEnd(
        filters=filters,
        kernel_size=kernel_size,
        pool_size=pool_size,
        activation=activation,
        activation_args=activation_args,
        pool_type=pool_type,
        norm=norm,
        causal=causal,
        squeeze_excite_r=squeeze_excite_r,
        squeeze_excite_op=squeeze_excite_op,
        unit_layer=resnext_layer,
        conv_type=conv_type,
        conv_kwargs=conv_kwargs,
        depth=depth,
        width=width,
        c=c,
        **kwargs
    )


def MSDNeXtFrontendV2(
        filters: int = 32,
        kernel_size: tuple = (3, 3),
        pool_size: tuple = (1, 3),
        activation: str | type[tfkl.Layer] | Callable = tfkl.LeakyReLU,
        activation_args: None | dict = None,
        pool_type: str | float = 'max',
        norm: str = 'renorm',
        causal: bool = False,
        squeeze_excite_r: float = 0.,
        squeeze_excite_op: str = 'avg',
        conv_type: type[tfkl.Layer] = tfkl.Conv2D,
        unit_layer: Callable | type[tfkl.Layer] = conv_batchnorm_layer,
        conv_kwargs: None | dict = None,
        depth: int = 3,
        width: int = 3,
        c: float = 1.,
        **kwargs
):
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    if activation == tfkl.LeakyReLU and not activation_args:
        activation_args = {"alpha": 0.01}

    # Arguments used by all sparrow layers
    general_args = dict(
        activation=activation,
        activation_args=activation_args,
        causal=causal,
        conv_kwargs=conv_kwargs,
        norm=norm,
        squeeze_excite_r=squeeze_excite_r,
        squeeze_excite_op=squeeze_excite_op,
        conv_type=conv_type
    )

    msd_args = dict(
        growth=filters,
        width=width,
        depth=depth,
        c=c,
        unit_layer=unit_layer,
        **restrict_kwargs(kwargs, unit_layer, padding='same')
    )

    pool_args = dict(
        pool_type=pool_type,
        pool_size=pool_size,
        activation=None,
        channel_axis=channel_axis,
    )

    def inner_func(x):
        x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, padding=[0], **general_args)
        x = multi_scale_dense_module(x, kernel_size=kernel_size, **msd_args, **general_args)
        x = pool_function(x, **pool_args)
        x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, padding=[0], **general_args)
        x = multi_scale_dense_module(x, kernel_size=kernel_size, **msd_args, **general_args)
        x = pool_function(x, **pool_args)
        x = conv_batchnorm_layer(x, filters=filters, kernel_size=kernel_size, padding=[0], **general_args)
        # remove bottleneck for the last MSD module
        # msd_args['c'] = None
        x = multi_scale_dense_module(x, kernel_size=kernel_size, **msd_args, **general_args)
        freq_summary_kernel = (3, x.shape[2 if channel_axis == -1 else -1])
        x = conv_batchnorm_layer(x, filters=filters, kernel_size=freq_summary_kernel, padding=[0], **general_args)

        return x

    return inner_func


def ConvNeXtFrontEnd(filters: int | tuple = (64, 64, 64, 64),
                     block_ratio: tuple = (2, 2, 3, 3),
                     kernel_size: int | tuple = (3, 3),
                     pool_size: tuple = (1, 2),
                     pool_type: str | float = "max",
                     activation: str | type[tfkl.Layer] = tfkl.LeakyReLU,
                     activation_args: None | dict = None,
                     norm: str = 'renorm',
                     conv_type: type[tfkl.Layer] = tfkl.Conv2D,
                     squeeze_excite_r: float | int = 0.,
                     squeeze_excite_op: str ='avg',
                     conv_kwargs: None | dict = None,
                     dilation_rate: int | tuple = 1,
                     depth_multiplier: float = 2.,
                     **kwargs,
                     ):

    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    if activation == tfkl.LeakyReLU and not activation_args:
        activation_args = {"alpha": 0.01}

    # Arguments used by all sparrow layers
    general_args = dict(
        activation=activation,
        activation_args=activation_args,
        conv_kwargs=conv_kwargs,
        norm=norm,
        squeeze_excite_r=squeeze_excite_r,
        squeeze_excite_op=squeeze_excite_op,
        conv_type=conv_type,
    )

    if isinstance(filters, int):
        filters = tuple([filters] * len(block_ratio))

    def inner_func(x):
        for b, f in zip(block_ratio, filters, strict=True):
            # the conv_batchnorm_layer call is required to change layer depths along the network
            x = conv_batchnorm_layer(x, filters=f, kernel_size=kernel_size, padding='same', **general_args)
            for i in range(b):
                x = resnext_layer(x, kernel_size=kernel_size, depth_multiplier=depth_multiplier, dilation_rate=dilation_rate, **general_args)
            x = pool_function(x, pool_type=pool_type, pool_size=pool_size, activation=None, channel_axis=channel_axis)

        # one last layer to summarise over the frequency axis
        x = conv_batchnorm_layer(x, filters=f, kernel_size=(1, x.shape[2]), padding='valid', **general_args)

        return x
    return inner_func


if __name__ == '__main__':
    from utilities.functions import count_model_parameters, get_model_memory_usage
    inp = tfkl.Input(shape=(480000, 6))

