"""
Script for extracting embeddings from models.
"""
import os
import tensorflow as tf
import importlib.util
from tqdm import tqdm
import pickle

from generators.audio_generators import BaseAudioGenerator, SpectrogramGenerator
from utilities.functions import flatten
from utilities.metrics import MyAUC
from utilities.output_manipulation import format_outputs, make_embedding_model

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)


os.chdir('C:/Users/killian/Desktop/database_crows')

# model(s) path (must be a list, but can be the path to a directory containing several models)
models = ["runs/Rookognise"]
models = flatten([[f'{m}/{f}' for f in os.listdir(m)] for m in models])
print(models)
# data path (must be the path to a directory containing both audio and label files of the formats given in config)
data_path = "data/validation"

model_comparison_path = "embeddings"

# Predictions
for m in tqdm(models, total=len(models)):
    try:
        import re
        spec = importlib.util.spec_from_file_location("config", f'{m}/config.py')
        config = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(config)
    except (FileNotFoundError, ModuleNotFoundError):
        import config

    # Get pattern
    pattern = re.search(pattern='[0-9]+_[0-9]+', string=m)

    # Retrieve model
    model = tf.keras.models.load_model(f'{m}/weights.tf', compile=False)

    # Extract the embedding outputs
    model = make_embedding_model(model, embedding_layer_names=['bidirectional'], keep_original_output=True)
    print(model.outputs)

    # Create generator
    # select the correct generator depending on the model's input_shape
    gen = SpectrogramGenerator if len(model.input_shape) == 4 else BaseAudioGenerator
    gen_args = config.spectrogram_generator_args if gen == SpectrogramGenerator else config.waveform_generator_args
    gen_args = dict(data=f"{data_path}",
                    **gen_args,
                    **config.label_generator_args,
                    **config.validation_args,
                    # add_reconstruct=config.head_args['add_reconstruct'],
                    training=False)
    gen_args['chunk_overlap'] = 0.
    local_gen = gen(**gen_args)

    chunks, labels, _ = local_gen[0]
    heads = {k: v.shape[-1] for k, v in labels.items()}

    # Get model parameters from the plotted model
    model_parameters = [f for f in os.listdir(m) if f.endswith('png')][0]

    # Compile model
    model.compile(loss=None, metrics=None)

    # Save predictions
    predictions = format_outputs(model, data=local_gen, return_labels=True, chunk_overlap=0., return_inputs=False)
    with open(f'{model_comparison_path}/{pattern.group()}_embeddings.pickle', 'wb') as connection:
        pickle.dump(obj=predictions, file=connection, protocol=pickle.HIGHEST_PROTOCOL)