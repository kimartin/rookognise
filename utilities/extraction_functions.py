import pickle

import numpy as np
from typing import Callable
import pandas as pd

import config
from utilities.functions import find_overlaps, onsets_offsets, top_k, num_decimal_places


def apply_threshold(arr,
                    threshold: float | int | Callable | list | np.ndarray
                    ):
    if arr.ndim > 1:
        if isinstance(threshold, list):
            arr = np.array([apply_threshold(arr[:, i], threshold[i]) for i in range(arr.shape[-1])]).T
        else:
            arr = np.array([apply_threshold(arr[:, i], threshold) for i in range(arr.shape[-1])]).T
        return arr

    try:
        arr = threshold(arr)
    except TypeError:
        if isinstance(threshold, np.ndarray):
            arr = arr > np.reshape(threshold, (1, -1))
        else:
            arr = arr > threshold

    return arr


def force_choice(array, r=2., axis=-1):
    slc = [slice(None)] * array.ndim
    slc[axis] = slice(-2, None)
    top2 = np.sort(array, axis=axis)[tuple(slc)]

    # get the top 2 values along axis
    # NB: using slice here so *ratio* is broadcastable with *array* (slighly easier than using np.expand_dims on ratio)
    slc[axis] = -1
    num = top2[tuple(slc)]
    slc[axis] = -2
    denom = top2[tuple(slc)]

    with np.errstate(divide='ignore', invalid='ignore'):
        num /= denom
        num[~np.isfinite(num)] = 0.
        num = num > r
        num = np.expand_dims(num, axis=axis)

    arr_top = top_k(array, k=1, axis=axis)

    return np.where(num, arr_top, array)


def rle(pred: np.ndarray,
        # threshold: None | float | int | Callable = None,
        min_frames: float = 0.,
        min_to_keep: float = 0.,
        trim: bool = False,
        merge: int | float = 2,
        method: Callable = np.max,
        # downward_spike_ratio: int | float = .5
        ):
    """
    Function to take the output predictions from a model (either directly or after loading from a .pickle file.
    This works by essentially doing RLE per class in the predictions, with a provided threshold.

    Additional arguments can be used to refine treatment of predictions, such as imposing a minimum length (min_frames),
    a minimum confidence in the predictions (min_to_keep and method, used to define method(detection) > min_to_keep),
    and minimum length between separate predictions (merge).
    Note that these arguments are applied in this order: first merge, then min_frames, and then min_to_keep and method.

    :param pred: 1D array-like, boolean array.
    If one or more callable(s) are passed, they must be functions that take pred (or pred[..., class]) and return
    a boolean array of the same shape.
    Threshold can always be a single scalar or callable (or a list of length 1), and in this case will be applied to the
    entire pred array.
    If pred.ndim > 1, threshold can be passed as a list of length pred.shape[-1], where each element will be used as
    a separate threshold per class in pred.
    :param min_frames: int. Minimum length of detections (in frames) to be retained.
    :param min_to_keep: float between 0 and 1. Threshold used by method(detection) to determine minimum confidence in
    output predictions.
    :param trim: bool. If True, remove detections that touch the extremities of the first axis.
    :param merge: int. Detections separated by less than this many frames will be merged.
    :param method: callable. Determines low detections (i.e. nominally above threshold, but "not above enough"). Defaults
        to np.max, in which case detections must reach min_to_keep at least once to be kept. Must return a scalar.
        Implemented as a test of method(detection) > min_to_keep
    :return: tuple of onsets and offsets. If pred.ndim == 1, each is a single array. If pred.ndim > 1, each is a list
    of arrays, where each array corresponds to predictions for one class in the predictions.
    """
    frames = len(pred)

    # basically run-length encoding
    on, off = onsets_offsets(pred)

    if len(on) > 1:
        if trim:
            if on[0] == 0:
                on = on[1:]
                off = off[1:]

            if off[-1] == frames:
                on = on[:-1]
                off = off[:-1]

        if merge > 0:
            # merge detections separated by segments shorter than the merge argument
            long_separation = np.array(on[1:] - off[:-1] > merge)
            on = on[[True, *long_separation]]
            off = off[[*long_separation, True]]

        if min_frames > 0:
            # remove detections shorter than min_frames
            long_detection = np.array(off - on > min_frames)
            on = on[long_detection]
            off = off[long_detection]

        if min_to_keep > 0.:
            # remove detections with low confidence
            high_detection = np.array([method(pred[o:f]) > min_to_keep for o, f in zip(on, off)])
            if len(high_detection) > 0:
                on = on[high_detection]
                off = off[high_detection]

    return on, off


def labels_from_prediction(pred: np.ndarray,
                           threshold: None | float | int | Callable = None,
                           min_frames: float = 5.,
                           min_to_keep: float = 0.,
                           trim: bool = False,
                           merge: int | float = 2,
                           method: Callable = np.max,
                           # downward_spike_ratio: int | float = .5
                           ):
    if pred.ndim == 1:
        pred = np.expand_dims(pred, axis=-1)

    out = apply_threshold(pred, threshold)

    res = zip(*[
        rle(out[:, i],
            min_frames=min_frames,
            min_to_keep=min_to_keep,
            trim=trim,
            merge=merge,
            method=method,
            ) for i in range(out.shape[-1])
        ])
    return res


from time import time

# Main function
def labels_from_network(pred,
                        threshold: float | float | Callable | dict = 0.5,
                        min_frames: int | dict = 5,
                        merge: int | dict = 2,
                        min_to_keep: float | dict = 0.,
                        method: Callable | dict = np.max,
                        frame_dur: float = 1. / 80.,
                        names: None | dict = None,
                        show_confidence: bool = False,
                        detection_name: str = "Detection",
                        unattributed: str = "Inc",
                        multiple: str = "Pls",
                        strict: bool = False,
                        force: bool = True,
                        force_choice_r: float = 2.,
                        ):
    """
    From a dict of predictions, retrieve intervals above given thresholds, with additional parameters to specify
    more specific behaviour.

    :param pred: dict. The predictions to use. The keys of the dict can be used in the other arguments to customise
    behaviour to each element of pred.
    NB: for threshold, min_frames, merge, min_to_keep, method: each argument can be given as a single value or as a dict.
    The pred keys will be used as a reference to extract specific values to apply to each element of pred. Other keys
    will be ignored.
    :param threshold: float or dict. Threshold value(s) to use, between 0 and 1 (or within the range of pred values).
    :param min_frames: int or dict. Keep only intervals of at least that many consecutive frames.
    :param merge: int or dict. Merge intervals spearated by that many frames or less.
    :param min_to_keep: float or dict. Keep only intervals where method(interval) >= min_to_keep
    :param method: callable or dict. Function to determine intervals to keep in conjunction with min_to_keep.
    :param frame_dur: float or None. If given, this is the duration of a frame in seconds (the interval between
    :param names:
    :param show_confidence:
    :param detection_name: str.
    :param unattributed: str. Name to use for detections not attributed not an individual.
    :param multiple: str. Name to use for detections attributed to >1 individuals.
    :param strict: bool.
    :param force: bool.
    :param force_choice_r:
    :return:
    """
    if not isinstance(threshold, dict):
        threshold = {k: threshold for k in pred}

    pred = {k: pred[k] for k in threshold if k in pred}

    pred = {k: v if v.ndim > 1 else np.expand_dims(v, axis=-1) for k, v in pred.items()}
    # num_classes = {k: v.shape[-1] for k, v in pred.items()}

    # Extend parameters to dict if given as scalars
    if not isinstance(min_to_keep, dict):
        min_to_keep = {k: min_to_keep for k in pred}
    if not isinstance(merge, dict):
        merge = {k: merge for k in pred}
    if not isinstance(min_frames, dict):
        min_frames = {k: min_frames for k in pred}
    if not isinstance(method, dict):
        method = {k: method for k in pred}

    # Suppresses all labels below the threshold
    te = time()
    pred = {k: v * apply_threshold(v, threshold[k]) for k, v in pred.items()}
    t1 = time() - te

    # Non-maximum suppression
    te = time()
    if force:
        def hard_nms(x):
            return force_choice(x, axis=0, r=force_choice_r)

        pred = {k: hard_nms(v) if v.shape[-1] > 1 else v for k, v in pred.items()}
    else:
        def soft_nms(x):
            return x * np.exp(x - x.max(axis=0, keepdims=True))

        pred = {k: soft_nms(v) if v.shape[-1] > 1 else v for k, v in pred.items()}
    t2 = time() - te
    # pred = {k: v * top_k(v, axis=-1, k=1) * (np.sum(v, axis=-1, keepdims=True) > 0) for k, v in pred.items()}

    # Get putative labels
    te = time()
    on_offs = {
        k: labels_from_prediction(pred=pred[k],
                                  threshold=threshold[k],
                                  min_to_keep=min_to_keep[k],
                                  min_frames=min_frames[k],
                                  merge=merge[k],
                                  method=method[k],
                                  trim=True)
        for k in pred
    }
    t3 = time() - te

    if names is None:
        names = {detection_name: ''}
    elif detection_name not in names:
        names[detection_name] = ''

    if names is not None:
        names = {k: names[k] for k in pred}

    # Organise the labels in data frames
    te = time()
    results = {k: pd.DataFrame(dict(zip(['Start', 'End'], on_offs[k]))) for k in names}
    results = {k: results[k].assign(Name=names[k], Output=k) for k in results}
    results = pd.concat(results, axis=0).droplevel(1, axis=0).reset_index(drop=True)
    t4 = time() - te

    # Explodes the onsets and offsets, so we have full columns of scalars instead of columns of lists
    # results = results.set_index('Name')
    te = time()
    results = results.apply(pd.Series.explode)
    results = results.reset_index(drop=True)
    t5 = time() - te

    te = time()
    # Remove missing values
    results = results.dropna(subset=['Start', 'End'])
    # Sort by increasing Start
    results = results.sort_values(by=['Start', 'Output']).reset_index(drop=True)
    # Define the groups that overlap
    results = find_overlaps(results, start='Start', end='End', name='group')
    t6 = time() - te

    te = time()
    if strict:
        # Suppresses anything outside a general detection
        results = results.groupby('group').filter(lambda x: x['Name'].eq('').any())
    t7 = time() - te

    # Clean detections
    te = time()
    # keeps unattributed vocalisations
    res_unattributed = results.groupby('group').filter(lambda x: x['Output'].eq(detection_name).all())
    res_unattributed['Name'] = unattributed

    results = results.groupby('group').filter(lambda x: x['Output'].ne(detection_name).any())

    # vocalisations explained by exactly one value from each dict
    res_unique = results.groupby('group').filter(lambda x: x['Output'].nunique() == x.shape[0])
    if strict:
        # removes unattributed detections
        # (e.g. avoids attributing a full 1-sec voc if an individual is detected in 1 frame...)
        res_unique = res_unique.loc[res_unique['Output'].ne(detection_name)].reset_index(drop=True)
    res_unique = res_unique.groupby('group').agg({'Start': min, 'End': max, 'Name': '-'.join})

    results = results.groupby('group').filter(lambda x: x['Output'].nunique() != x.shape[0])

    # other vocalisations
    res_other = results.loc[results['Output'].ne(detection_name)]
    res_other = res_other.groupby('group').agg({'Start': min, 'End': max, 'Name': lambda x: f'{multiple}-{",_".join(x)}'})
    t8 = time() - te
    res = pd.concat([res_unique, res_unattributed, res_other], ignore_index=True)

    # Displays confidence scores
    if show_confidence:
        res['Name'] = [f'{name}-{round(np.mean(pred[output][start:end, names[output].index(name)]) * 100)}'
                       for start, end, name, output, _ in res.itertuples(index=False)]

    te = time()
    res = res[['Start', 'End', 'Name']]
    res = res.drop_duplicates(subset=['Start', 'End'])
    res[['Start', 'End']] *= frame_dur
    # bit of rounding to get nicer-looking labels at the cost of slightly offsetting the labels
    res[['Start', 'End']] = res[['Start', 'End']].round(decimals=num_decimal_places(value=frame_dur))
    res = res.loc[res['End'] > res['Start']]
    res = res.reindex(['Start', 'End', 'Name'], axis=1).reset_index(drop=True)
    t9 = time() - te

    tot = sum([t1, t2, t3, t4, t5, t6, t7, t8, t9])
    print([_ / tot for _ in [t1, t2, t3, t4, t5, t6, t7, t8, t9]], tot)

    return res


if __name__ == '__main__':
    path = 'C:/Users/killian/Desktop/database_rooks/model_tests/20230207_094929_ovlp0.pickle'
    pred = pickle.load(open(path, 'rb'))[-1]

    # pred = {'Detection': np.zeros(shape=1000),
    #         'Identification': np.zeros(shape=(1000, 3)),
    #         'Sexe': np.zeros(shape=(1000, 2))}
    # pred['Detection'][100:200] = .5
    # pred['Identification'][150:200, 0] = .5
    # pred['Identification'][100:148, 1] = .5
    # pred['Sexe'][100:200, 0] = .5
    # pred['Detection'][300:400] = .5
    # pred['Identification'][350:351, 0] = .5
    # pred['Sexe'][350:356, 0] = .5
    res = labels_from_network(pred,
                              threshold={'Source': .2, 'Sex': .2, 'Detection': .2},
                              strict=True,
                              show_confidence=False,
                              names=config.classes, frame_dur=1.)
    print(res)




