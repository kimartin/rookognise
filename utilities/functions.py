"""
Mostly random functions that are useful somewhere else in the project. No particular theme.
"""
import re
import numpy as np
from functools import wraps, reduce
from time import time
import itertools
import pandas as pd
import os
from itertools import chain
from scipy import ndimage
from inspect import signature
import tensorflow as tf
K = tf.keras.backend


def timing(f, times=1000):
    """
    Decorate functions with this for easy timing estimations

    :param f: a callable object (/function) or list therefor
    :param times: how many times to run f and aggregate the results
    """

    @wraps(f)
    def wrap(*args, **kw):
        t = []
        for _ in range(times):
            ts = time()
            result = f(*args, **kw)
            te = time()
            t.append(te - ts)
        quartiles = np.percentile(t, [25, 50, 75])
        print(
            f"{f.__name__}: min {min(t)} - lq {quartiles[0]} - avg {sum(t) / times} - "
            f"med {quartiles[1]} - uq {quartiles[2]} - max {max(t)}")

    return wrap


def listfiles(path,
              pattern=None,
              negate=False,
              full_names=False,
              ):
    files = sorted(os.listdir(path))

    if pattern is None:
        if full_names and path is not None:
            files = [os.path.join(path, f) for f in files]
        return files

    files = match_filenames(files, pattern=pattern, negate=negate)

    if full_names and path is not None:
        files = [f'{path}/{f}' for f in files]

    return files


def split_train_validation(df,
                           train_name='train',
                           validation_name='valid',
                           dataset_col='Dataset',
                           label_col='Label',
                           audio_col='Audio',
                           ):
    """
    Takes a pandas DataFrame with at least three columns, and splits it into a train and a validation datasets.
    One column contains the names (or path) of label files, another the names (or path) of audio files, and one
    contains which dataset each file should fall into.

    :param df: DataFrame.
    :param train_name: The name of the train split.
    :param validation_name: The name of the validation split
    :param dataset_col: The column specifying which file goes into which dataset.
    :param label_col: The column specifying names/path to label files.
    :param audio_col: The column specifying names/path to audio files.
    :return: 4 lists of file names in two tuples. The first two are the train labels and audio, the last two are the
    validation labels and audio.
    """
    train_data = df.loc[df[dataset_col].eq(train_name)]
    validation_data = df.loc[df[dataset_col].eq(validation_name)]

    train_labels = train_data[label_col].to_list()
    train_audio = train_data[audio_col].to_list()

    validation_labels = validation_data[label_col].to_list()
    validation_audio = validation_data[audio_col].to_list()

    return (train_labels, train_audio), (validation_labels, validation_audio)


def match_filenames(filenames,
                    pattern: None | str | list = None,
                    negate: list | bool = False,
                    ):
    if not isinstance(filenames, list):
        filenames = [filenames]
    if not isinstance(pattern, list):
        pattern = [pattern]
    if not isinstance(negate, list):
        negate = [negate] * len(pattern)

    for pat, neg in zip(pattern, negate, strict=True):
        filenames = [f for f in filenames if bool(re.search(pattern=pat, string=f)) ^ neg]

    return filenames


def unstack(a, axis=0):
    """
    Replicates tensorflow.unstack in numpy
    """
    return list(np.moveaxis(a, axis, 0))


def num_decimal_places(value):
    if not isinstance(value, str):
        value = str(value)
    m = re.match(r"^[0-9]*\.(([0-9]*[1-9])?)0*$", value)
    return len(m.group(1)) if m is not None else 0


def broadcastable(a, b):
    """
    Tests whether arrays a and b can be broadcast together.
    """
    return all((m == n) or (m == 1) or (n == 1) for m, n in zip(a.shape[::-1], b.shape[::-1], strict=True))


def flatten(ll):
    """
    Flattens a list of lists into a list.
    """
    return list(chain.from_iterable(ll))


def round_robin(*iterables, strict=True):
    """
    Implements a Round Robin distribution on a list of iterables.
    For 2 iterables, this corresponds to alternating concatenation
    Note that the output will only go as far as the shortest iterable.
    """
    return [i for items in zip(*iterables, strict=strict) for i in items]


def all_combinations(iterable, n=None, m=0):
    """
    Get all combinations of elements from an iterable.

    :param iterable: Iterable containing the elements to combine
    :param n: None or int. Maximum length of the combinations. If None, all combinations will be returned, up to and
    including the iterable itself.
    :param m: int. Minimum length of the combinations, default is 0, which starts combinations at 1 element.
    :return: list of iterables, each element
    """
    if n is None:
        n = len(iterable)
    comb = [itertools.combinations(iterable, i+1) for i in range(m, n)]
    return flatten(comb)


def standard(a, axis=2):
    """
    Center-scales (mean 0, standard deviation 1) along axis or axes.
    :param a: array or tensor
    :param axis: int or tuple
    """
    with np.errstate(invalid='ignore', divide='ignore'):
        res = (a - a.mean(axis=axis, keepdims=True)) / a.std(axis=axis, keepdims=True)
        res[~np.isfinite(res)] = 0.
    return res


def product_dict(**kwargs):
    """
    Gets all combinations from the given named arguments. Each combination is returned as dict with keys given by the
    names of the arguments, and values as one value from each of the lists in the arguments

    :param kwargs: arbitrary named arguments. Lists are split to determine combinations (e.g. [A, B] will be used to
    construct two dict, one with A and one with B), all other objects are internally converted to lists of length 1.
    :return: list of dicts, each dict is a combination of the values in kwargs
    """
    values = [value if isinstance(value, list) else [value] for value in kwargs.values()]
    return [dict(zip(kwargs.keys(), instance)) for instance in itertools.product(*values)]


def listofdict_to_dictoflist(ld: list):
    """
    Hacky solution that converts a list of dicts to a dict of lists.
    All keys present in any of the input list of dicts are preserved.
    """
    return {k: [dic[k] for dic in ld if k in dic] for k in reduce(set.union, [set(_.keys()) for _ in ld])}


def dictoflist_to_listofdict(dl: dict):
    """
    Returns a list of dicts from a dict of lists.
    Each dict in the output list has the same keys as dl and one value from the original lists.

    Note: throws an error if the lists in dl.values() are of different lengths
    """
    return [dict(zip(dl, t)) for t in zip(*dl.values(), strict=True)]


def are_dicts_equal(dicts: list):
    if not dicts:
        return False
    elif len(dicts) == 1:
        return True
    reference_dict = dicts[0]
    for d in dicts[1:]:
        if d != reference_dict:
            return False
    return True


def same_structure(a, b):
    """
    Recursively checks if a and b are of the same structure (e.g. if a and b are nested lists, checks each level in
    turn).
    """
    is_a_list = isinstance(a, list)
    is_b_list = isinstance(b, list)
    xor = is_a_list ^ is_b_list

    if xor and len(a) == len(b):
        return all(map(same_structure, a, b))

    return not xor


def find_overlaps(x,
                  start="Start",
                  end="End",
                  separated_by=0.,
                  name='group',
                  by=None,
                  allow_shared_boundaries=True):
    """
    Finds the overlaps in ranges defined by the columns "start" and "end" of pd.DataFrame object x.
    This includes overlaps that include multiple rows at once.

    Example:
    x = pd.DataFrame({"Start": [0, 10, 20, 30, 40, 50, 60],
                      "End":   [9, 21, 28, 53, 42, 55, 70]
    find_overlaps(x)
            Start  End  group
                0    9      1
               10   21      2
               20   28      2
               30   53      3
               40   42      3
               50   55      3
               60   70      4

    :param x: a pd.DataFrame object
    :param start: str. Gives the column that defines the start of the range.
    :param end: str. Gives the column that defines the end of the range.
    :param separated_by: float. Allows extending (if > 0) or shrinking (if <0) intervals so that overlaps will be
    considered either more broadly or more stringently.
        separated_by > 0: overlaps will be considered even if the actual values are separated by up to its value.
            e.g. separated_by = 1: intervals [1, 2.0001] and [2.9999, 4] are STILL considered overlapping.
        separated_by < 0: overlaps will be considered only if the actual values really overlap by more than its value
            e.g. separated_by = -1: intervals [1, 2.9999] and [2.0001, 4] are NOT considered overlapping.
    :param allow_shared_boundaries: bool. If True (default), intervals that share one boundary are not counted as
        overlapping.
    :param by: str or list of str. Optional columns that define groups within x. Rows that overlap in the
        start and end columns, but have different group as defined by this argument, will not be considered overlapping.
    :param name: str. Name of the "group" column
    :return: x with addition "group" column. The values in this column correspond to overlaps.
    """
    # df = x.copy(deep=True)

    # Flatten start and end into a single column
    startdf = pd.DataFrame({'time': x[start] - separated_by / 2, 'what': 1})
    enddf = pd.DataFrame({'time': x[end] + separated_by / 2, 'what': -1})

    # Sort values: "time" for chronological order, and "what" to avoid consider shared boundaries as overlap
    sort_cols = ['time']

    if allow_shared_boundaries:
        sort_cols += ['what']

    if by is not None:
        startdf = pd.concat([startdf, x[by]], axis=1)
        enddf = pd.concat([enddf, x[by]], axis=1)
        sort_cols = [*by, *sort_cols] if isinstance(by, list) else [by, *sort_cols]

    mergdf = pd.concat([startdf, enddf]).sort_values(sort_cols)

    # Count intervals currently going on over the entire column
    mergdf['running'] = mergdf['what'].cumsum()

    # Finds the starts where there is exactly one interval
    mergdf['newwin'] = mergdf['running'].eq(1) & mergdf['what'].eq(1)

    # Group
    mergdf[name] = mergdf['newwin'].cumsum()

    # Adds the group back to the original data
    x[name] = pd.Series(mergdf[name].loc[mergdf['what'].eq(1)], index=x.index)

    x[name] = pd.factorize(x[name])[0]

    return x


def merge_overlaps(x, start="Start", end="End", by=None, separated_by=0.):
    """
    Merges the overlaps found by find_overlaps. All arguments are passed to find_overlaps and so should be considered
    identically.
    """
    # Merges the overlaps found by find_overlaps
    columns = [start, end]

    x = find_overlaps(x, start=start, end=end, separated_by=separated_by, by=by)

    dict_agg = {start: 'min', end: 'max'}
    if by is not None:
        dict_agg = dict(**{_: 'first' for _ in by}, **dict_agg)
        columns += by

    res = x.groupby(["group"]).agg(dict_agg)

    res = res.reset_index()[columns]
    return res


def arg_names(function):
    """
    Gets a list of all the named arguments accepted by the input function.
    """
    return list(signature(function).parameters.keys())


def restrict_kwargs(d, func, **kwargs):
    """
    Gets only the subset of dict d with keys within the arguments of func.
    """
    arguments = arg_names(func)
    new_d = {k: d[k] for k in d if k in arguments}
    new_kwargs = {k: kwargs[k] for k in kwargs if k in arguments}
    new_d.update(new_kwargs)
    return new_d


def restricted(func):
    def restricted_func(d, **kwargs):
        return func(**restrict_kwargs(d, func, **kwargs))
    return restricted_func


def norm(x, axis=None, keepdims=True):
    amax = x.max(axis=axis, keepdims=keepdims)
    amin = x.min(axis=axis, keepdims=keepdims)
    return (x - amin) / (amax - amin)


def absmax(a, axis=None, keepdims=True):
    # np.abs(a, out=a)
    return np.max(np.abs(a), axis=axis, keepdims=keepdims)


def preemphasis(x, pre, normalise=True, axis=-1):
    slc1 = [slice(None)] * len(x.shape)
    slc2 = [slice(None)] * len(x.shape)

    slc1[axis] = slice(1, None)
    slc2[axis] = slice(None, -1)

    if normalise:
        max_ = absmax(x, axis=axis, keepdims=True)
        x[tuple(slc1)] -= pre * x[tuple(slc2)]

        with np.errstate(divide='ignore', invalid='ignore'):
            x /= max_
            x[~np.isfinite(x)] = 0.
    else:
        x[tuple(slc1)] -= pre * x[tuple(slc2)]

    return x


# Helper functions
# def onsets_offsets(arr):
#     elements, nelements = ndimage.label(arr)
#     if nelements == 0:
#         return np.array([[], []])
#
#     idx = np.array([0, -1])
#     onsets, offsets = np.array(
#         [
#             np.where(elements == element)[0][idx] for element in range(1, nelements)
#         ]
#     ).T
#
#     return np.array([onsets, offsets + 1])


def onsets_offsets(x):
    n = len(x)
    if n == 0:
        return np.array([]), np.array([])

    # find run starts
    loc_run_start = np.empty(n, dtype=bool)
    loc_run_start[0] = True
    np.not_equal(x[:-1], x[1:], out=loc_run_start[1:])
    run_starts = np.nonzero(loc_run_start)[0]

    # find run values
    run_values = np.array(x[loc_run_start], dtype=np.double)

    onsets = run_starts[run_values == 1]
    offsets = run_starts[run_values == 0]
    if run_values[-1] == 1:
        offsets = np.append(offsets, n)
    if run_values[0] == 0:
        offsets = offsets[1:]

    return np.array([onsets, offsets])


def top_k_values(array, k):
    idx = np.argsort(array)[-k:][::-1]
    arr = np.zeros_like(array)
    arr[idx] = 1.
    return arr


def top_k(array, k, axis=-1):
    return np.apply_along_axis(top_k_values, axis, array, k)


def top_1(array, axis=-1):
    x = np.zeros_like(array)
    idx = np.max(array, axis=axis, keepdims=True)
    x[array == idx] = 1.
    return x


def highest_above_second(arr, threshold=2, axis=-1):
    sorted_arr = np.sort(arr, axis=axis)
    slc = [slice(None)] * arr.ndim
    slc[axis] = -1
    highest = sorted_arr[tuple(slc)]
    slc[axis] = -2
    second = sorted_arr[tuple(slc)]

    diff = 1 - second / highest
    return diff > 1./threshold


def count_model_parameters(model, per_layer=False):
    trainable_params = [K.count_params(p) for p in model.trainable_weights]
    non_trainable_params = [K.count_params(p) for p in model.non_trainable_weights]
    if not per_layer:
        trainable_params = np.sum(trainable_params).astype(np.int64)
        non_trainable_params = np.sum(non_trainable_params).astype(np.int64)
        total_params = trainable_params + non_trainable_params
    else:
        total_params = [a + b for a, b in zip(trainable_params, non_trainable_params)]
    return trainable_params, non_trainable_params, total_params


def get_model_memory_usage(batch_size, model, policy=None, per_layer=False):
    if policy:
        compute_size = policy.compute_dtype
        variable_size = policy.variable_dtype
    else:
        compute_size = K.floatx()
        variable_size = K.floatx()

    compute_memory = None
    if compute_size == 'float16':
        compute_memory = 2.
    elif compute_size == 'float32':
        compute_memory = 4.
    elif compute_size == 'float64':
        compute_memory = 8.

    variable_memory = None
    if variable_size == 'float16':
        variable_memory = 2.
    elif variable_size == 'float32':
        variable_memory = 4.
    elif variable_size == 'float64':
        variable_memory = 8.

    shapes_mem_count = []
    internal_model_mem_count = []
    for l in model.layers:
        if l.__class__.__name__ == 'Model':
            internal_model_mem_count += get_model_memory_usage(batch_size, l, policy=policy, per_layer=False)
        out_shape = l.output_shape
        if isinstance(out_shape, list):
            out_shape = out_shape[0]
        shapes_mem_count += [np.prod([s for s in out_shape if s is not None]).astype(np.int64)]

    model_parameters = count_model_parameters(model, per_layer=per_layer)

    if per_layer:
        layer_memory = [_ * variable_memory for _ in model_parameters[-1]]
        layer_memory = [lay + batch_size * mem * compute_memory for lay, mem in zip(layer_memory, shapes_mem_count)]
        gbytes = [np.round(_ / (1024. ** 3), 3) for _ in layer_memory]

        gbytes = dict(zip([layer.name for layer in model.layers], gbytes))

    else:
        total_memory = np.sum(model_parameters[-1]) * variable_memory
        total_memory += batch_size * np.sum(shapes_mem_count) * compute_memory

        # get size in GB
        total_memory /= (1024. ** 3)
        gbytes = np.round(total_memory, 3) + np.sum(internal_model_mem_count)

    return gbytes


def create_labels_from_prediction(pred,
                                  threshold=None,
                                  min_frames=5,
                                  min_to_keep=.5,
                                  ):
    """
    Function to take the output predictions from a model (either directly or after loading from a .pickle file).

    It essentially simply gets the runs above the provided threshold argument, with some fancy arguments to remove:
        - short detections (min_frames is the lower limit in length for detections to be accepted in the labels)
        - low detection (min_to_keep is the value a detection must reach at least once to be accepted)
    :return: tuple of onsets and offsets. Each element is either an array or a list of arrays depending on pred.ndim > 1
    """
    if threshold is not None:
        if not isinstance(threshold, (float, int, list, np.ndarray)) and not callable(threshold):
            raise ValueError('Please provide threshold as one of: scalar/callable or list/array thereof.'
                             'Additionally, if you wish to use multiple thresholds, provide exactly as many as pred.shape[-1]')

        if hasattr(threshold, '__len__') and len(threshold) == 1:
            threshold = threshold[0]
        elif hasattr(threshold, '__len__') and pred.ndim > 1 and len(threshold) != pred.shape[-1]:
            raise ValueError(f'Provided {len(threshold)} but only {pred.shape[-1]} classes exist in pred. '
                             f'Please provide as many thresholds as there are classes in pred')

    if pred.ndim > 1:
        mask = pred.T
        if hasattr(threshold, '__len__'):
            mask = [p > thr if not callable(thr) else callable(thr) for p, thr in zip(mask, threshold)]
        elif threshold is not None:
            mask = [p > threshold if not callable(threshold) else threshold(p) for p in mask]
        onsets, offsets = zip(*[onsets_offsets(_) for _ in mask])

        # remove short detections
        long_detection = [np.array([f - o > min_frames for o, f in zip(on, off)]).astype('bool') for on, off in zip(onsets, offsets)]
        onsets = [on[long] for on, long in zip(onsets, long_detection)]
        offsets = [off[long] for off, long in zip(offsets, long_detection)]

        # remove low detections
        high_detection = [np.array([np.max(pred[o:f]) > min_to_keep for o, f in zip(on, off)]).astype('bool') for on, off in zip(onsets, offsets)]
        onsets = [on[hi] for on, hi in zip(onsets, high_detection)]
        offsets = [off[hi] for off, hi in zip(offsets, high_detection)]

    else:
        mask = pred
        if threshold is not None:
            mask = pred > threshold if not callable(threshold) else threshold(pred)
        onsets, offsets = onsets_offsets(mask)

        # remove short detections
        long_detection = np.array([off - on > min_frames for on, off in zip(onsets, offsets)]).astype('bool')
        onsets = onsets[long_detection]
        offsets = offsets[long_detection]

        # remove low detections
        high_detection = np.array([np.max(pred[o:f]) > min_to_keep for o, f in zip(onsets, offsets)]).astype('bool')
        onsets = onsets[high_detection]
        offsets = offsets[high_detection]

    return onsets, offsets


# Main function
def create_labels_from_network(pred,
                               threshold=0.5,
                               min_to_keep=0.,
                               frame_dur=1./80.,
                               names=None,
                               min_frames=5,
                               keep_vocs=True,
                               use_intervals=True,
                               refine=None
                               ):

    if not isinstance(threshold, dict):
        threshold = {k: threshold for k in pred.keys()}

    if not isinstance(min_to_keep, dict):
        min_to_keep = {k: min_to_keep for k in pred.keys()}

    pred = {k: np.squeeze(np.reshape(v, newshape=(-1, v.shape[-1]))) for k, v in pred.items()}

    det_on, det_off = create_labels_from_prediction(
        pred=pred['detection'],
        threshold=threshold['detection'],
        min_to_keep=min_to_keep['detection'],
        min_frames=min_frames
    )
    if refine is not None:
        pass

    if names is None:
        if use_intervals:
            df = pd.DataFrame({
                'V1': [det_on],
                'V2': [det_off],
                'V3': [''],
            })
        else:
            df = pd.DataFrame({
                    'V1': [det_on, det_off],
                    'V3': ['voc', 'voc-end']
                })

    else:
        from time import time
        # suppress predictions outside of detected vocalisations
        pred_id = np.zeros_like(pred['identification'])

        for on, off in zip(det_on, det_off):
            pred_id[on:off] = pred['identification'][on:off]

        # Restrict to top predictions AND predictions sufficiently higher than the next lower
        pred_id *= top_1(pred['identification'], axis=-1)
        if callable(threshold['identification']):
            pred_id *= np.expand_dims(threshold['identification'](pred['identification']), axis=-1)
        else:
            pred_id *= np.max(pred['identification'] > threshold['identification'], axis=-1, keepdims=True)

        id_ons, id_offs = create_labels_from_prediction(
            pred=pred_id,
            threshold=.05,
            min_to_keep=min_to_keep['identification'],
            min_frames=min_frames,
        )

        if use_intervals:
            df = pd.DataFrame(
                {
                    'V1': id_ons,
                    'V2': id_offs,
                    'V3': names
                }
            )
            if keep_vocs:
                df = pd.concat(
                    [
                        df,
                        pd.DataFrame({
                            'V1': [det_on],
                            'V2': [det_off],
                            'V3': [''],
                        }),
                    ]
                )
        else:
            df = pd.DataFrame(
                {
                    'V1': id_ons + id_offs,
                    'V3': names + [f'{n}-end' for n in names],
                }
            )
            if keep_vocs:
                df = pd.concat([
                    df,
                    pd.DataFrame({
                        'V1': [det_on, det_off],
                        'V3': ['voc', 'voc-end']
                    })
                ])
            df.insert(loc=1, value=df['V1'], column='V2')

    df = df.set_index(['V3']).apply(pd.Series.explode).sort_values(by='V1')
    df = df.reset_index()
    df = df.sort_values(by='V1')
    df = df.drop_duplicates(subset=['V1', 'V2'], keep=False)
    df[['V1', 'V2']] *= frame_dur
    df = df.reindex(sorted(df.columns), axis=1)

    return df