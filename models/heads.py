import math

import tensorflow as tf
K = tf.keras.backend
tfkl = tf.keras.layers

from utilities.functions import restrict_kwargs
from modules.custom_modules import apply_norm, self_attention_layer, conv_batchnorm_layer, resnext_layer, tcn_module
from modules.custom_layers import Expand, Squeeze


def Head(model,
         n,
         filters,
         r=2.,
         dropout_rate=0.,
         recurrent_dropout_rate=0.,
         final_activation="sigmoid",
         embedding_layer='recurrent',
         name=None,
         combination=False,
         provided_bias=None,
         conv_type=tfkl.Conv1D,
         activation='relu',
         activation_args=None,
         norm='batch',
         renorm=False,
         conv_kwargs=None,
         squeeze_excite_r=0.,
         squeeze_excite_op='avg',
         kernel_initializer='glorot_uniform',
         unit_layer=conv_batchnorm_layer,
         recurrent_kernel_initializer='glorot_uniform',
         recurrent_stack_depth=1
         ):
    """
    Function to create time resolution-preserving heads.
    """
    temp_name = name
    embed_name = f"embedding_{name}" if name else "embedding"
    name = None
    channel_axis = -1 if K.image_data_format() == 'channels_last' else 1

    activation_args = activation_args or dict()
    conv_kwargs = conv_kwargs or dict()
    if 'kernel_initializer' not in conv_kwargs:
        conv_kwargs['kernel_initializer'] = kernel_initializer

    # Particular case, embedding_layer can be a tuple of length 2
    match embedding_layer:
        case embedding, depth:
            embedding_layer = embedding
            recurrent_stack_depth = depth

    if isinstance(unit_layer, type(tfkl.Layer)):
        x = unit_layer(kernel_size=1, **restrict_kwargs(locals(), unit_layer, dropout_rate=0.))(model)
    else:
        x = unit_layer(model, kernel_size=1, **restrict_kwargs(locals(), unit_layer, dropout_rate=0.))

    in_channels = x.shape[channel_axis]
    width = int(round(in_channels / r))
    # keeps roughly the same number of parameters when stacking several LSTM layers
    recurrent_stack_width = int(round(math.sqrt((width * (2 * width + 1) / recurrent_stack_depth - 1) / 2)))
    if embedding_layer in ['lstm', 'gru']:
        recurrent_layer = getattr(tfkl, embedding_layer.upper())
        for i in range(recurrent_stack_depth):
            x = tfkl.Bidirectional(
                recurrent_layer(
                    units=recurrent_stack_width if recurrent_stack_depth > 1 else width,
                    return_sequences=True,
                ),
                name=embed_name if i == recurrent_stack_depth - 1 else None
            )(x)
    elif embedding_layer == 'attention':
        x = self_attention_layer(x, norm=norm, r=r, conv_kwargs=conv_kwargs, name=embed_name, conv_type=conv_type)
    elif embedding_layer == 'tcn':
        # hardcoded numbers approximate the same number of parameters as the recurrent case
        x = tcn_module(x, filters=width, kernel_size=3, base_dilation_rate=2, stacks=1, depth=7, conv_kwargs=conv_kwargs, conv_type=conv_type, name=embed_name)

    if dropout_rate > 0.:
        x = tfkl.SpatialDropout1D(rate=dropout_rate)(x)

    x_final = tfkl.Conv1D(filters=n,
                          kernel_size=1,
                          name=temp_name if provided_bias is None and not combination else None,
                          dtype=tf.float32,
                          activation=final_activation if provided_bias is None else None,
                          use_bias=provided_bias is None,
                          **restrict_kwargs(conv_kwargs, tfkl.Conv1D))(x)

    if provided_bias is not None:
        if not isinstance(provided_bias, list):
            provided_bias = [provided_bias]
        x_final = tfkl.Add(dtype=tf.float32)([x_final, provided_bias])
        x_final = tfkl.Activation(activation=final_activation, dtype=tf.float32, name=name if not combination else None)(x_final)

    return x, x_final


def PresenceHead(x,
                 n,
                 filters,
                 depth=3,
                 kernel_size=9,
                 strides=5,
                 dropout_rate: float = 0.,
                 activation='relu',
                 final_activation="sigmoid",
                 name="presence",
                 combination=False,
                 provided_bias=None,
                 unit_layer=conv_batchnorm_layer,
                 activation_args=None,
                 norm='batch',
                 conv_kwargs=None,
                 squeeze_excite_r=0.,
                 squeeze_excite_op='avg',
                 kernel_initializer='glorot_uniform',
                 attention_ratio=None,
                 conv_type=tfkl.Conv1D
                 ):
    """
    Function to create heads that summarise over the time axis
    """
    activation_args = activation_args or dict()
    conv_kwargs = conv_kwargs or dict()
    if 'kernel_initializer' not in conv_kwargs:
        conv_kwargs['kernel_initializer'] = kernel_initializer

    dict_args = dict(
        conv_type=conv_type,
        kernel_size=kernel_size,
        strides=strides,
        filters=filters,
        norm=norm,
        activation=activation,
        activation_args=activation_args,
        conv_kwargs=conv_kwargs,
        squeeze_excite_r=squeeze_excite_r,
        squeeze_excite_op=squeeze_excite_op,
    )

    for i in range(depth):
        if isinstance(unit_layer, type(tfkl.Layer)):
            x = unit_layer(**restrict_kwargs(dict_args, unit_layer))(x)
        else:
            x = unit_layer(**restrict_kwargs(dict_args, unit_layer, x=x))
        if attention_ratio:
            x = self_attention_layer(x, r=attention_ratio, norm=None, conv_kwargs=conv_kwargs)

    embedding_name = f'embedding_{name}'
    x = tfkl.Conv1D(filters=filters // 2,
                    kernel_size=x.shape[1],
                    activation=activation if not isinstance(activation, type(tfkl.Layer)) else None,
                    name=embedding_name if not isinstance(activation, type(tfkl.Layer)) else None,
                    **restrict_kwargs(conv_kwargs, tfkl.Conv1D))(x)

    if isinstance(activation, type(tfkl.Layer)):
        x = activation(**activation_args, name=embedding_name)(x)

    if dropout_rate > 0.:
        x = tfkl.SpatialDropout1D(rate=dropout_rate)(x)

    x_final = tfkl.Conv1D(filters=n,
                          kernel_size=1,
                          name=name if provided_bias is None and not combination else None,
                          dtype=tf.float32,
                          activation=final_activation if provided_bias is None else None,
                          use_bias=provided_bias is None,
                          **restrict_kwargs(conv_kwargs, tfkl.Conv1D))(x)

    if provided_bias is not None:
        x_final = tfkl.Add(dtype=tf.float32)([x_final, provided_bias])
        x_final = tfkl.Activation(action=final_activation, dtype=tf.float32, name=name if not combination else None)(x_final)

    return x, x_final


def RecurrentPresenceHead(x,
                          n,
                          filters,
                          final_activation="sigmoid",
                          dropout_rate: float = 0.,
                          name="presence",
                          combination=False,
                          provided_bias=None,
                          conv_kwargs=None,
                          kernel_initializer='glorot_uniform',
                          unit_layer=conv_batchnorm_layer,
                          ):
    """
    Function to create heads that summarise over the time axis
    """
    conv_kwargs = conv_kwargs or dict()
    if 'kernel_initializer' not in conv_kwargs:
        conv_kwargs['kernel_initializer'] = kernel_initializer

    # Particular case, embedding_layer can be a tuple of length 2
    if isinstance(unit_layer, type(tfkl.Layer)):
        x = unit_layer(kernel_size=1, **restrict_kwargs(locals(), unit_layer, dropout_rate=0., conv_type=tfkl.Conv1D))(x)
    else:
        x = unit_layer(**restrict_kwargs(locals(), unit_layer, dropout_rate=0., kernel_size=1, x=x, conv_type=tfkl.Conv1D))

    embedding_name = f'embedding_{name}'
    x = tfkl.Bidirectional(tfkl.LSTM(units=filters // 2, return_sequences=False, name=embedding_name))(x)
    x = Expand(axis=-2)(x)

    if dropout_rate > 0.:
        x = tfkl.SpatialDropout1D(rate=dropout_rate)(x)

    conv_kwargs = conv_kwargs or dict()
    if 'kernel_initializer' not in conv_kwargs:
        conv_kwargs['kernel_initializer'] = kernel_initializer

    x_final = tfkl.Conv1D(filters=n,
                          kernel_size=1,
                          name=name if provided_bias is None and not combination else None,
                          dtype=tf.float32,
                          activation=final_activation if provided_bias is None else None,
                          use_bias=provided_bias is None,
                          **conv_kwargs)(x)

    if provided_bias is not None:
        x_final = tfkl.Add(dtype=tf.float32)([x_final, provided_bias])
        x_final = tfkl.Activation(action=final_activation, dtype=tf.float32, name=name if not combination else None)(x_final)

    return x, x_final


def Reconstruct_Head_Spec(x,
                          n,
                          filters=128,
                          activation='relu',
                          final_activation='relu',
                          norm='renorm',
                          name='reconstructed',
                          **kwargs):

    paddings = ['valid', 'valid', 'same', 'same', 'same', 'same']
    kernels = [(1, 3), (1, 3), (4, 4), (4, 4), (4, 4), (4, 4)]
    strides = [(1, 1), (1, 1), (1, 2), (1, 2), (1, 2), (1, 2)]
    if isinstance(filters, int):
        filters = [filters] * len(paddings)

    for i, (pad, ker, stri, filt) in enumerate(zip(paddings, kernels, strides, filters, strict=True)):
        x = tfkl.Conv2DTranspose(filters=filt,
                                 kernel_size=ker,
                                 padding=pad,
                                 strides=stri,
                                 activation=activation,
                                 **kwargs)(x)
        if i < len(paddings) - 1:
            # No norm operation before the last layer
            x = apply_norm(x, norm=norm)

    x = tfkl.Conv2D(filters=n, kernel_size=1, activation=final_activation, name=name, dtype=tf.float32, **kwargs)(x)

    return x


def Reconstruct_Head_Wave(x,
                          n,
                          filters=128,
                          activation='relu',
                          final_activation='relu',
                          norm='renorm',
                          name='reconstructed',
                          **kwargs):

    kernels = [8, 8, 8, 4, 4, 4]
    strides = [5, 5, 3, 2, 2, 2]
    paddings = ['same'] * len(kernels)
    if isinstance(filters, int):
        filters = [filters] * len(paddings)

    for i, (pad, ker, stri, filt) in enumerate(zip(paddings, kernels, strides, filters, strict=True)):
        x = tfkl.Conv1DTranspose(filters=filt,
                                 kernel_size=ker,
                                 padding=pad,
                                 strides=stri,
                                 activation=activation,
                                 **kwargs)(x)
        if i < len(paddings) - 1:
            # No norm operation before the last layer
            x = apply_norm(x, norm=norm)

    x = tfkl.Conv1D(filters=n, kernel_size=1, activation=final_activation, name=name, dtype=tf.float32, **kwargs)(x)

    return x



if __name__ == '__main__':
    from utilities.functions import count_model_parameters, get_model_memory_usage
    x = tfkl.Input(shape=(400, 1, 32))
    head = Reconstruct_Head_Spec(x, n=6,
                                 filters=32,
                                 #filters=[512, 256, 128, 64, 32, 16]
                                 )
    mod = tf.keras.Model(inputs=x, outputs=head)
    print(mod.summary())
    print(count_model_parameters(mod), get_model_memory_usage(32, mod))

