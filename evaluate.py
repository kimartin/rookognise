"""
Script for evaluation of models.
"""
import os
import tensorflow as tf
import importlib.util
import pickle
from tqdm import tqdm
import pandas as pd

from generators.audio_generators import AudioGenerator, SpectrogramGenerator
from utilities.functions import flatten
from utilities.metrics import MyAUC
from utilities.output_manipulation import format_outputs


os.chdir('C:/Users/killian/Desktop/database_rooks')

# model(s) path (must be a list, but can be the path to a directory containing several models)
models = ["runs/temp"]
models = flatten([[f'{m}/{f}' for f in os.listdir(m)] for m in models])

# data path
data_path = 'data'
data_file = 'splitting.tsv'
label_file = pd.read_csv(data_file, sep='\t')
test_audio = label_file.loc[label_file['Dataset'].eq('test'), 'Audio'].to_list()
test_labels = label_file.loc[label_file['Dataset'].eq('test'), 'Label'].to_list()
test_audio = [f'{data_path}/{_}' for _ in test_audio]
test_labels = [f'{data_path}/{_}' for _ in test_labels]

# output path (where to write out the performance.csv file that will contain the model metrics)
out_path = "performance.tsv"

model_comparison_path = "model_tests"
# model_comparison_path = None
evaluate_models = True


# df = pd.read_csv(out_path, sep='\t')
# model_list = df['Model'].loc[df['Source_ap'] > 0.65].tolist()

# Predictions
for m in tqdm(models, total=len(models)):
    # if m not in model_list:
    #     continue

    try:
        import re
        spec = importlib.util.spec_from_file_location("config", f'{m}/config.py')
        config = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(config)
    except (FileNotFoundError, ModuleNotFoundError):
        import config

    # Get pattern
    pattern = re.search(pattern='[0-9]+_[0-9]+', string=m)

    # Retrieve model
    model = tf.keras.models.load_model(f'{m}/weights.tf', compile=False)

    # Create generator
    # select the correct generator depending on the model's input_shape
    gen = SpectrogramGenerator if len(model.input_shape) == 4 else AudioGenerator
    gen_args = config.spectrogram_generator_args if gen == SpectrogramGenerator else config.waveform_generator_args
    gen_args = dict(data=test_audio,
                    labels=test_labels,
                    **gen_args,
                    **config.label_generator_args,
                    **config.validation_args,
                    # add_reconstruct=config.head_args['add_reconstruct'],
                    training=False)
    local_gen = gen(**gen_args)

    chunks, labels, _ = local_gen[0]
    heads = {k: v.shape[-1] for k, v in labels.items()}

    # Get model parameters from the plotted model
    model_parameters = [f for f in os.listdir(m) if f.endswith('png')][0]

    # Recreate metrics
    metric_args = dict(num_thresholds=200)
    metrics_dict = {k: [MyAUC(num_labels=c, curve="ROC", name="auroc", **metric_args),
                        MyAUC(num_labels=c, curve="PR", name="ap", **metric_args)]
                    for k, c in heads.items()}

    # Compute metrics and add to the table
    model.compile(loss=None, metrics=metrics_dict)
    if evaluate_models:
        result = model.evaluate(x=local_gen, return_dict=True)
        result = {k: [v] if not isinstance(v, list) else v for k, v in result.items()}
        result = pd.DataFrame({'Model': [m], 'Parameters': [model_parameters], **result})
        result.to_csv(out_path, index=False, sep='\t', mode='a', header=not os.path.exists(out_path))
        # try:
        #
        # except:
        #     continue

    # Save predictions
    if model_comparison_path:
        chunk_overlap = 0.
        gen_args['chunk_overlap'] = chunk_overlap
        local_gen = gen(**gen_args)

        predictions = format_outputs(model, data=local_gen, return_labels=True, chunk_overlap=chunk_overlap, return_inputs=False)
        with open(f'{model_comparison_path}/{pattern.group()}_ovlp{int(100*chunk_overlap)}.pickle', 'wb') as connection:
            pickle.dump(obj=predictions, file=connection, protocol=pickle.HIGHEST_PROTOCOL)

