"""
Config file to parametrize model training and inference

Note: all parameters can be passed as iterable objects (lists or tuples).
For all such objects, a different models will be trained using every possible combinations of values.

(To avoid combinatorial explosition, it may not be wise to use this for more than a couple parameters at a time)
"""

import os

import tensorflow as tf
tfk = tf.keras

from modules.custom_functions import mish
from utilities.metrics import FocalCrossentropy
from modules.custom_modules import resnext_layer, conv_batchnorm_layer
from models.frontends import ConvPoolFrontEnd, ConvPoolFrontEndV2, ConvNeXtFrontEnd, MSDFrontEnd, MSDNeXtFrontend, MSDNeXtFrontendV2
from models.heads import PresenceHead, RecurrentPresenceHead

#### Data paths ####
os.chdir("C:/Users/killian/Desktop/database_rooks")
path = "runs"
data = 'data'
data_file = 'splitting.tsv'

# Random seed for reproducibility
seed = 42

# General parameters
# This dict can be given any parameter in the rest of this script, along with a list of values
# All possible combinations of items from this dict will then be trained in sequence
loop_parameters = dict(
    # unit_layer=[conv_batchnorm_layer],
    # frontend=[MSDNeXtFrontendV2],
    # norm=['renorm'],
    # embedding_layer=['lstm'],
    # presence_head_func=[PresenceHead],
    # conv_type=[tfk.layers.Conv2D],
    # module_filters=[256, 32, 64, 128, 512, 1024],
    # head_filters=[32, 64, 128, 256, 512, 1024],
    # loss_weights=[{'Detection': 1, 'Sex': 1, 'Source': 10, 'presence_Source': 1}]
)

# List of dicts, each dict gives one combination of parameters above that should NOT be trained
excluded_combinations = [] # [{'unit_layer': conv_batchnorm_layer, 'norm': 'renorm'}]

# Training policy (mixed precision reduces memory footprint and may or may not accelerate training)
policy = tfk.mixed_precision.Policy('mixed_float16')
tfk.mixed_precision.set_global_policy(policy)

# PCEN parameters (seen pcen.py for explanations)
pcen_args = dict(
    alpha=.5,
    delta=1.,
    r=.3,
    eps=1E-9,
    s=.025,
    sd=0.5,
    learn_axis=[2],
    absolute_sd=True,
    return_last=False,
    dtype=tf.float32,
    data_format='btfc',
)
# Learned DRC parameters (mutually exclusive with PCEN)
learn_drc_args = dict(
    alpha=1./3.,
    dtype=tf.float32
)

# Generator parameters
# Audio sampling rate, necessary to generate the labels
# (if the data has multiple sampling rates, this should be the highest)
sr = 48000
# use these parameters to define the spectrogram in more human-readable units
# (window_length in second, overlap as proportion or percent, fft_expansion is the ratio of zero-padding desired)
window_length = 0.05
overlap = .75   # 0 < overlap < 1
fft_expansion = 1.

overlap = overlap if overlap <= 1. else overlap / 100
fps = 1. / (window_length * (1 - overlap))


# General
general_generator_args = dict(
    # number of samples per batch (constrained by memory footprint of the model)
    batch_size=32,
    # sample duration in seconds (ignored if duration_in_samples is not None)
    duration=5,
    duration_in_samples=None,
    # extensions expected for the audio files (NB: use a regex if multiple extensions are expected, as it is now)
    audio_format='flac|wav|WAV',
    # extensions expected for the label files (NB: use a regex if multiple extensions are expected)
    labels_format='tsv',
    # time resolution for the labels
    fps=fps,
    # number of channels
    n_channels=6,
    # determines sampling mode: if True, each sample will be centered on one vocalisation
    # if False, samples will be extracted by splitting the entire sound files
    per_voc_sampling=True
)


# Audio
filter_args = dict(
    # Pre-emphasis coefficient if float, Number of pre-emphasis coefficients if int and trainable=True
    # note that the default setting here actually corresponds to the filter: x[n] = x[n] - pre * x[n-1] (the sign is inverted internally)
    pre=1.,
    # If True, the filter will be learned during training
    trainable=False
)

# Spectrogram
spectrogram_args = dict(
    frame_length=int(round(window_length * sr)),
    frame_step=int(round(window_length * sr * (1 - overlap))),
    fft_length=int(round(window_length * sr * fft_expansion)),
    window_fn=tf.signal.hamming_window,
    pad_end=True,
    power=2.,
    # phase=False,
)

# Input frequency axis size
freq_axis_size = 80

# Mel
melspectrogram_args = dict(
    # a and b can be used to specify alternate Mel scales
    # If they are not specified, use the HTK Mel equation: Mel(F) = 2295 * log(1 + F/700)
    # Otherwise, use the equation: Mel(F) = a * F / (b + F)
    # a=0.00024,
    # b=0.741,
    n_mels=freq_axis_size,
    htk=True,
    fmin=0,
    fmax=sr // 2,
    # trainable=True enables learning the values of a and b
    trainable=False,
    librosa=True,
    lateral_inhibition=False,
)


# Combine for generators, but we need them separate in case we use them for layers
waveform_generator_args = {**general_generator_args, **filter_args}
spectrogram_generator_args = {**general_generator_args, **filter_args, **spectrogram_args, **melspectrogram_args}

# Labels
# Here, specify the labels to use
# the classes dict should work with the label files
# for instance here: the label files should include a "Source" column and a "Sex" column, containing the values
# the labels are then used to specify the order of classes, so that for instance, "Aristotle" is the first class of the "Source" output
classes = {
    "Source": ["Aristotle", "Balbo", "Bashir", "Braad", "Brain", "Bussell", "Cassandra", "Connelly", "Elie", "Feisty",
               "Fry", "Gigi", "Huxley", "Jolene", "Jonas", "Kafka", "Leo", "Merlin", "Osiris", "Pomme", "Siobhan", "Tom"],
    "Sex": ['Female', 'Male'],
}
# classes = {
#     "Source": ["Alicante", "Baerchen", "Bluepoint", "Bluestripes", "Cabezon", "Franz",
#                "Gabi", "Gertrude", "GreenO", "GreenZ", "Hugo", "Ibiza", "Klaus", "Martinez", "Munin", "Nino",
#                "Olaf", "Peter", "Pobla", "Redcross", "Redpoint", "Resa", "Ruediger",
#                "Toeffel", "Valencia", "Walter", "White", "Whitecross", "Whitepoint",
#                "Whitestripes", "Willi", "Xufa", "Yellow",
#                # "Blackstripes", "Momo", "Redstripes",
#                ]
# }

# Used when combining outputs that cannot be directly broadcast together
# The idea is to replicate the outputs of e.g. "Sex" to the corresponding outputs of e.g. "Source"
masks_combinations = dict(
    Source_Sex=[
        [0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0],
        [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1],
    ]
)

label_generator_args = dict(
    # use the classes dict defined above
    classes={k: sorted(c) for k, c in classes.items()},
    # keys of the classes dict can be specified here to add corresponding outputs summarising over time
    presence=['Source'],
    # gives the name of the Detection output (i.e. whether or not there is a vocalisation in the recording)
    object_detection_name='Detection',
    # "exclude": gives classes that will be used only for Detection, but NOT other outputs
    exclude=['Inc', 'Pls', 'Unknown', 'Overlap', "Blackstripes", "Momo", "Redstripes",],
    # "ignore": gives classes that will never be used, even to draw samples
    # (in particular, this will effectively ignore all "Ignore" intervals in the labels)
    ignore=['Ignore', 'Noise', ''],
    # "multilabel": gives classes that should be considered as multiple classes (e.g. 'Pls' for Source would be
    # considered as an "All the individuals are vocalising at the same time")
    multilabel=[],
)


# Training-specific
# k controls over-sampling (if > 1) or under-sampling (if < 1) of all samples, effectively multiplying the total number
# of samples by k
k = 1.
training_args = dict(
    # drawing an additional negative_prop * 100% of samples from background-only parts of the recording
    negative_prop=0,
    # used only if per_voc_sampling=False, allow successive chunks to overlap by chunk_overlap*100%
    chunk_overlap=.2,
)

# Validation- and test-specific
validation_args = dict(
    # successive chunks overlap by chunk_overlap*100%
    chunk_overlap=0.,
)

# Loss parameters
loss = FocalCrossentropy
loss_args = dict(
    gamma=2.,
    label_smoothing=.05
)


# General model arguments
stem_args = dict(first_layer='PCEN',
                 add_frequency_mapping=False,
                 add_wavegram=False)
wavegram_args = dict(
    conv_type=tfk.layers.DepthwiseConv1D,
    freq_axis_size=freq_axis_size,
    block_ratio=2,
    pool_size=(10, 10, 6),
    filters=(40 * 6, 80 * 6, 80 * 6)
)

general_model_args = dict(
    conv_kwargs=dict(
        kernel_regularizer=tf.keras.regularizers.l2(l2=1e-5),
        kernel_initializer="he_normal",
        depth_multiplier=4,
    ),
    activation=mish,
    pool_type="conv",
    conv_type=tfk.layers.Conv2D,
    combination_op=tfk.layers.Multiply,
    norm='renorm',
    embedding_layer='lstm',
    # use a dict with the head names as keys to control variant per task
    variant=True,
    unit_layer=conv_batchnorm_layer,
    msd_module_width=3,
    msd_module_depth=2,
    msd_module_c=0.,
    presence_head_func=PresenceHead,
)

# Front-end specific arguments
frontend = MSDNeXtFrontendV2
frontend_args = dict(
    padding=[0],
    depth_multiplier=2.
)

# Head params
head_args = dict(
    # head_filters={'Detection': 64, 'Source': 512, 'presence_Source': 512, 'Sex': 128},
    head_filters=512,
    module_filters=512,
    dropout_rate=0.2,
    final_activation="sigmoid",
    # defines the actual combinations in the form of a dict, where keys are the outputs conditioned on the respective values
    combinations={"Source": ['presence_Source', 'Detection']}, #, 'Sex': ['Detection']},
    add_reconstruct=False,
)

# Run parameters
epochs = 30

# Optimiser
lr_cycle = 6
optimiser_args = dict(
    min_lr=1e-6,
    max_lr=10**(-2.5),
    weight_decay=1e-4,
    cycle_length=lr_cycle,
    inner_optimizer='AdamW'
)

# Callback arguments
callback_args = dict(
    mode="max",
    patience=int(1.5 * lr_cycle),
    min_delta=0,
    save_best_only=True,
    save_weights_only=False,
    tensorboard=False,
    cycle=lr_cycle
)

#### Augmentation ####
#     Augmenter(
#     p=.75,
#     transforms=dict(
#         shuffle_channels=partial(shuffle_slices, size=2, axis=channel_axis),
#         time_stretch=partial(array_stretch, n=10, axis=time_axis, p=.2),
#         frequency_stretch=partial(array_stretch, n=5, axis=freq_axis, p=.2),
#         random_volume_change=partial(random_volume_change, p=6),
#         replace_background=partial(replace_background, axis=channel_axis),
#     ),
#     k=3,
#     return_transforms=False
# )
