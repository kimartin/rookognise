from math import ceil, log2
import cupy as xp
import cupyx.scipy.signal as sgn
import numpy as np


import os.path
import re
import pandas as pd
from itertools import product
from tqdm import tqdm
import joblib

import soundfile as sf
from utilities.functions import flatten, find_overlaps


def hms_to_s(x, sep=None):
    """
    Converts hms string x to an integer representing the number of seconds since midnight.
    The sep argument is to remove separator strings such as ':'.
    e.g. hms_to_s('08:12:00', sep=':') -> 29520
    """
    if sep is not None:
        x = x.replace(sep, "")
    x = int(x)
    return 3600 * (x // 10000) + 60 * (x % 10000 // 100) + x % 100


def isPowerOfTwo(x: int):
    return x and ((x & (x - 1)) == 0)


def wav_crosscorrelate(x):
    """
    Cross-correlates array x along its first axis, in practice this returns cross-correlations for x[0] vs x[i], i >= 1.
    """
    n, c = x.shape

    # we make sure to at least double the length of x and stored
    size = 2 ** ceil(log2(n) + 1)
    stored = xp.concatenate((x[::-1, :1], xp.zeros(shape=(size - n, 1), dtype=x.dtype)), axis=0)
    x = xp.concatenate((x, xp.zeros(shape=(size - n, c), dtype=x.dtype)), axis=0)

    x = xp.fft.rfft(x, axis=0)
    x *= xp.fft.rfft(stored, axis=0)
    x = xp.fft.irfft(x, axis=0)
    return x


def optimal_lag(crosscor):
    xp.abs(crosscor, out=crosscor)
    lags = crosscor.shape[0] - 1 - crosscor.argmax(axis=0)
    return lags - lags.min()


def synchronise(x, lags=None, pad_type='zeros', trim=True):
    n, c = x.shape
    if lags is None:
        lags = optimal_lag(wav_crosscorrelate(xp.asarray(x))).get()

    out_shape = (n + np.max(lags), c)

    if pad_type == "means":
        mean_values = x.mean(axis=0, keepdims=True)
        out = np.full(shape=out_shape, dtype=x[0].dtype, fill_value=mean_values)
    else:
        out = np.zeros(shape=out_shape, dtype=x[0].dtype)

    for i in range(c):
        out[slice(lags[i], lags[i] + n), i] = x[:, i]

    if trim:
        if pad_type == 'means':
            out_values = (out == mean_values).sum(axis=-1)
        else:
            out_values = (out == 0).sum(axis=-1)
        idx = np.where(out_values < out.shape[-1])[0]
        out = out[idx]

    return out


def prepare_df(filelist, indicator_pattern, time_pattern, day_pattern, path=None):
    if path is not None:
        filelist = [f'{path}/{_}' for _ in filelist]

    df = []
    attributes = ['name', 'channels', 'frames', 'duration', 'samplerate']
    for file in tqdm(filelist, total=len(filelist)):
        df_ = sf.info(file, verbose=True)
        df_ = pd.DataFrame({_: getattr(df_, _) for _ in attributes}, index=[0])
        df.append(df_)
    df = pd.concat(df, ignore_index=True, axis=0)

    df['file'] = df['name'].str.replace(f'{path}/', '')

    df['indicator'] = df['name'].str.extract(f'({indicator_pattern})', expand=True)
    df['loc'] = ['CAMB' if 'CAM' in _ else 'STRA' for _ in df['file']]
    df['day'] = df['name'].str.extract(f'({day_pattern})', expand=True)
    df['time'] = df['name'].str.extract(f'({time_pattern})', expand=True)
    df['start_s'] = df['time'].apply(hms_to_s)
    df['end_s'] = df['start_s'] + df['duration']
    df['filename'] = [f'{loc}_{day}_{time}.flac' for loc, day, time in df[['loc', 'day', 'time']].itertuples(index=False)]

    df = df.sort_values(by=['day', 'indicator', 'start_s']).reset_index(drop=True)

    return df


def synchronise_wrapper(group=None, file=None, return_audio=True):
    # doing wav_crosscorrelate manually because otherwise it runs into an OoM error
    if file is None:
        frames = group['frames'].max()
        audio = [sf.read(file=f, dtype='int16', frames=frames, always_2d=True, fill_value=0)[0]
                 for f in group['name'].unique()]
        audio = np.concatenate(audio, axis=-1)
    else:
        frames = sf.info(file).frames
        audio = sf.read(file=file, dtype='int16', always_2d=True)[0]

    sync_frames = int(2 ** 27.8)
    #size = min(2 * sync_frames, 2 ** ceil(log2(frames) + 1))

    mempool = xp.get_default_memory_pool()
    cache = xp.fft.config.get_plan_cache()
    cache.set_size(0)
    cache.set_memsize(0)

    lags = []
    stored = xp.flipud(xp.asarray(audio[:sync_frames, 0]))
    #stored = xp.concatenate((stored, xp.zeros(shape=size - sync_frames, dtype=audio.dtype)))
    # stored = xp.fft.rfft(stored, axis=0)

    cache.clear()
    mempool.free_all_blocks()

    for c in range(audio.shape[-1]):
        # tmp = xp.asarray(audio[:sync_frames, c])
        # tmp = xp.concatenate((tmp, xp.zeros(shape=size - sync_frames, dtype=audio.dtype)))
        # tmp = xp.fft.rfft(tmp, axis=0)
        tmp = sgn.fftconvolve(xp.asarray(audio[:sync_frames, c]), stored, axes=0)

        cache.clear()
        mempool.free_all_blocks()

        # tmp *= stored
        # tmp = xp.fft.irfft(tmp, axis=0)

        xp.abs(tmp, out=tmp)
        tmp = tmp.shape[0] - 1 - tmp.argmax()
        lags.append(tmp.get())

        cache.clear()
        mempool.free_all_blocks()

    lags = np.array(lags)
    lags -= np.min(lags)

    if return_audio:
        audio = synchronise(audio.get(), lags=lags, trim=True)
        return audio, lags

    return lags


if __name__ == '__main__':
    mempool = xp.get_default_memory_pool()

    in_path = "F:/Database_Backup/data/2020_Killian/Rooks/raw/audio/cambridge_aviary"
    out_path = 'D:/Rooks/raw/audio/Cambridge_aviary'
    # lag_save_path = out_path
    lag_save_path = "C:/Users/killian/Desktop"
    # in_path = 'G:/Database_RookSong/data/Rooks/raw/audio/Cambridge_aviary'
    # out_path = 'G:/Database_RookSong/data/Rooks/raw/audio/Cambridge_aviary'

    df = prepare_df(filelist=[f for f in os.listdir(in_path) if f.endswith('flac') or f.endswith('wav')],
                    indicator_pattern=r"SM4[A-Z]+",
                    time_pattern=r"(?<=[^\d])\d{6}(?=[^\d])",
                    day_pattern=r"(?<=[^\d])\d{8}(?=[^\d])", path=in_path)

    df = find_overlaps(df, start='start_s', end='end_s', by='day')

    channel_indices = {"SM4A": [0, 1], "SM4B": [2, 3], "SM4C": [4, 5], "SM4CAMA": [0, 1], "SM4CAMB": [2, 3], "SM4CAMC": [4, 5]}
    df['channel_indices'] = df['indicator'].map(channel_indices)

    # remove hard cases for now
    df = df.groupby(['day', 'group']).filter(lambda x: x['indicator'].nunique() == x.shape[0])

    try:
        already_lagged = pd.read_table(f'{lag_save_path}/lags_info.tsv', sep='\t')
        df = df.loc[~df['filename'].isin(already_lagged['filename'])].reset_index(drop=True)
    except FileNotFoundError:
        pass
    for _, group in tqdm(df.groupby(['day', 'group']), total=df.groupby(['day', 'group']).ngroups):
        group = group.reset_index(drop=True)
        filename = group['filename'].iloc[0]

        # do not rerun on files if they already exist
        if os.path.exists(f'{out_path}/{filename}'):
            continue

        if group['indicator'].nunique() == group.shape[0]:
            # covers both trival (one file total) and easy (one file from each microphone at most) cases
            lags = synchronise_wrapper(group, return_audio=False)

        else:
            continue
            # hard case, where there may be multiple files per microphone

            # find the minimal number of groups needed to get at least one lag for each file in group
            new_group = pd.concat({i: group.loc[list(idx)]
                                   for i, idx in
                                   enumerate(product(*group.index.groupby(group['indicator']).values()), start=0)})
            new_group = new_group.reset_index(drop=False)
            new_group = find_overlaps(new_group, start='start_s', end='end_s', name='new', by='level_0')
            new_group = new_group.groupby('level_0').filter(lambda x: x['new'].nunique() == 1)
            final_group = [new_group.loc[new_group['level_0'].eq(1)]]
            pd.options.display.width = 0
            i = 1
            while len(final_group) == 0 or not sorted(group['name']) == sorted(
                    pd.concat(final_group)['name'].unique()):
                tmp = pd.concat(final_group)['name'].unique()
                shared = [(_, len(set(tmp) & set(g['name']))) for _, g in new_group.groupby('level_0')]
                n_common = max(1, min([le for _, le in shared]))
                groupID = [id for id, le in shared if le == n_common][0]
                final_group.append(new_group.loc[new_group['level_0'].eq(groupID)])
            final_group = pd.concat(final_group).sort_values(['level_0', 'level_1'])

            lags = np.nan

        group = group.explode(column='channel_indices')
        group['channel_indices'] -= group['channel_indices'].min()
        group['lag'] = lags
        group.to_csv(f'{lag_save_path}/lags_info.tsv', sep="\t", mode="a",
                     header=not os.path.exists(f'{lag_save_path}/lags_info.tsv'), index=False)

        # sf.write(file=f'{out_path}/{filename}', data=audio, samplerate=group['samplerate'].iloc[0])




