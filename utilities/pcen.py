import matplotlib.pyplot as plt
import librosa
import librosa.display as disp
import soundfile as sf
from math import exp
import os
import numpy as np
import tensorflow as tf
K = tf.keras.backend
from modules.custom_layers import BoundedTruncatedNormal
from time import time


def pcen(x, eps=1E-6, s=0.025, alpha=0.98, delta=2.0, r=0.5, last_state=None,
         return_last=False, time_axis=None, mask=None):
    """
    Computes the PCEN of the given spectrogram x.
    The spectrogram may be of arbitrary scaling and dimensions.
    Current defaults match the settings suggested on https://www.kaggle.com/c/freesound-audio-tagging-2019/discussion/91859,
    for much lower scales of signal.
    To match the librosa implementation (in theory) ensure x in ]-2**31, 2**31-1[ and set:
    eps=1E-6, s=0.025, alpha=0.98, delta=2, r=0.5

    :param x: numpy array or array-like. The input spectrogram.
    :param eps: positive float. Minuscule value to avoid dividing by 0.
    :param s: positive float. The smoothing coefficient. Set to small-ish value.
    :param alpha: positive float. The gain parameter. Should be close to 1 (but still lower).
    :param delta: positive float. The bias parameter.
    :param r: positive float. The power parameter. Recommendations are between .25 and .5
    :param last_state: can be used to initialise the smoother instead of taking the first frame of the spectrogram.
    If provided, should be an array of dimension (x.shape[0], )
    :param implementation: "scipy", "librosa" or None. If "scipy" or "librosa", use scipy.signal to create m. Else,
    iteratively create m starting from last_state (if provided) or the first frame of x. Notably, the librosa.core.pcen
    function functionally assumes a stationary state equal to the first frame of the spectrogram, hence the small
    differences obtained between the two implementations.
    :param return_last: bool. Whether (True) or not (False) to output the last state of the smoother. True essentially
    allows the transformation to be applied to long files.

    :return: tensor of the same dimensions as x
    """
    shape = list(x.shape)

    m_frames = []
    slc = [slice(None)] * len(shape)
    for f in range(shape[time_axis]):
        slc[time_axis] = slice(f, f+1)
        if last_state is None:
            last_state = x[tuple(slc)]
            m_frames.append(last_state)
            continue
        m_frame = (1 - s) * last_state + s * x[tuple(slc)]
        last_state = m_frame
        m_frames.append(m_frame)

    m = tf.concat(m_frames, axis=time_axis)

    smooth = tf.pow(eps + m, alpha)
    g = tf.math.divide_no_nan(x, smooth)
    pcen_ = tf.pow(g + delta, r) - tf.pow(delta, r)

    # apply mask
    if mask is not None:
        mask = tf.expand_dims(tf.cast(mask, dtype=pcen_.dtype), axis=len(mask.shape))
        pcen_ = pcen_ * mask

    if not return_last:
        last_state = None

    return pcen_, last_state


class PCEN(tf.keras.layers.Layer):
    def __init__(self,
                 alpha=0.98,
                 delta=2.0,
                 r=0.5,
                 eps=1E-6,
                 s=0.025,
                 absolute_sd=True,
                 learn_axis=None,
                 sd=1.0,
                 implementation=None,
                 return_last=False,
                 last=None,
                 name="PCEN",
                 data_format='bftc',
                 fast=True,
                 **kwargs):
        """
        Implementation of PCEN as a trainable layer for Keras/Tensorflow. Trainability is possible at different levels,
        from single value parameters, to one value per frequency band or per time frame, or even per spectrogram
        coefficient.

        :param alpha: The gain parameter.
        :param delta: The bias parameter.
        :param r: The power parameter.
        :param eps: float. The soft threshold of the DRC part of the procedure.
        :param s: float. The smoothing coefficient (basically how fast the IIR filter will filter out static noise).
        Strictly between 0 and 1. Defaults to 0.025
        :param training: bool. Whether to allow the parameters to be learned (if True) or not. Defaults to True.
        :param learn_axis: Should be one of None, "frequency", or "both". If None, all the parameters are
        learnable but are single values. For other values, the parameters are allowed to vary along the corresponding
        axis (i.e. for "frequency", one value per frequency band is learned). Defaults to None.
        :param sd: float, list or dict. The standard deviation(s) to apply to the initialisers. Ignored if
        training = False or learn_axis = None. If provided as a dict, parameters not in the keys of the dict will be
        generated as if given sd[paramater] = 0. If provided as a list, throws error if length is not 1 or 5. If a list
        of length 5 is given, the respective sd values are applied in the order [alpha, delta, r, eps, s].
        :param absolute_sd: bool. Whether to use sd "as is" (resulting in all parameters being generated with the
        provided sd exactly), or multiply by the value of the parameter (resulting in larger parameters having larger
        sd). For best results, if True, sd should not be set higher than 0.5
        (which will then signify that the parameters will be initialised via a truncated normal within
        ]0; 2*value[ where value is the value of the respective parameter (eps, s, alpha, delta, r). For s and alpha however,
        2*value is hard-bounded by 1.
        :param return_last: bool. Passed to the pcen function. If true, returns the last frame of the smoother, allowing
        streaming transformations
        :param fast: bool. Controls which implementation of the M matrix computation to use.
        If True, uses a matrix multiplication implementation that's faster but more memory-intensive.
        Otherwise, uses a for loop.
        :param kwargs:

        :return: PCEN-adjusted spectrogram.

        References:
        [1] Y. Wang, P. Getreuer, T. Hughes, R. F. Lyon, and R. A. Saurous,  “Trainable frontend for robust and
        far-field keyword spotting,” in Proc. IEEE ICASSP, 2017.
        """
        super().__init__(name=name, **kwargs)

        self.supports_masking = True
        if not 0 < s < 1:
            raise ValueError(f"s={s} but should be stricly between 0 and 1")

        pcen_args = {"alpha": alpha, "delta": delta, "r": r, "eps": eps, "s": s}
        # This collects the name of the arguments provided with values below 0 and throws an error
        neg_pcen_args = [k for k in pcen_args if pcen_args[k] <= 0]
        if any(neg_pcen_args):
            length = len(neg_pcen_args)
            s = "" if length == 1 else "s"
            a = "a " if length == 1 else ""
            raise ValueError(f"Parameter{s} {', '.join(neg_pcen_args)} should be {a}positive float{s}.")

        self.channel_axis = 1 if K.image_data_format() == "channels_first" else -1
        assert data_format in ['bftc', 'btfc', 'bctf', 'bcft'], ValueError('data_format should be one of: bftc, btfc, bctf, bcft.')
        self.data_format = data_format

        self.absolute_sd = absolute_sd
        if isinstance(sd, dict):
            if len(sd) == 5:
                self.sd = sd
            else:
                self.sd = dict(zip(pcen_args.keys(), [0]*len(pcen_args)))
                self.sd.update(sd)
                print(f"Some arguments were not provided: {', '.join([k for k, v in self.sd.items() if v == 0])}. "
                      f"Using sd=0 for these arguments...")
        elif isinstance(sd, list):
            if len(sd) == 1:
                self.sd = dict(zip(pcen_args.keys(), sd*len(pcen_args)))
            elif len(sd) == 5:
                self.sd = dict(zip(pcen_args.keys(), sd))
            else:
                raise ValueError(f"sd should be of length 1 or 5, but {len(sd)=}")
        else:
            print(f"Using {sd=} as common value of sd for all parameters")
            self.sd = dict(zip(pcen_args.keys(), [sd]*5))

        # choose between two different implementations of the filter: fast but memory-costly, or slow but memory-friendly
        self.fast = fast
        self.m_function = self.fast_m if self.fast else self.memory_m

        # if following librosa's definition, coerce learn_axis to None so that the parameters are scalars
        self.learn_axis = learn_axis if self.trainable else None
        if not hasattr(self.learn_axis, '__len__') and self.learn_axis is not None:
            self.learn_axis = [self.learn_axis]

        # work in log space for the parameter updates, then convert back into linear space for computation
        # ensures non-negativity of the parameters, as well as stability improvement
        self.eps = tf.math.log(eps)
        self.alpha = tf.math.log(alpha)
        self.delta = tf.math.log(delta)
        self.s = tf.math.log(s)
        self.r = tf.math.log(r)

        self.last = last
        self.return_last = return_last

    def _initialiser(self, mean, sd=None, upper=None, lower=None, absolute=True):
        """
        Truncated normal initialiser for generation in log space instead of linear space.
        Essentially it's just a truncnorm.rvs generator from scipy.stats parametrised to replicate Keras' own
        TruncatedNormal initiliaser (i.e. generate normal values around the mean, values more than 2 standard deviations
        away from the mean are redrawn until they fall within 2 standard deviations from the mean), that is then taken
        to log space (this ensures that the parameters will be strictly positive when mapped back to linear space).

        Returned object depends on self.trainable, being either a fixed tensor for the current backend (False), or an
        initialiser function (True).

        :param mean: float. The mean of the Truncated Normal distribution.
        :param sd: float. The standard deviation of the Truncated Normal distribution
        :param upper: float. Optional boundary on the lower end of the distribution. If generated numbers exceed the
        boundary value, they are redrawn.
        :param lower: float. Optional boundary on the upper end of the distribution. If generated numbers exceed the
        boundary value, they are redrawn.
        :param absolute: bool. If true, use sd as standard deviation, else use sd*mean as standard deviation.
        :return: A tensor (if self.trainable is False) or an initialiser function (if self.trainable is True)
        """
        if self.trainable and sd > 0.:
            mean = exp(mean)
            if self.absolute_sd:
                sd *= mean
            lower = max(lower, mean - 2. * sd) if lower else mean - 2. * sd
            upper = min(upper, mean + 2. * sd) if upper else mean + 2. * sd
            init = BoundedTruncatedNormal(lower=lower, upper=upper)
        else:
            init = tf.keras.initializers.Constant(value=mean)

        return init

    def build(self, input_shape):
        if self.learn_axis:
            training_shape = (*[input_shape[i] for i in self.learn_axis], 1)
            # training_shape = [dim if ax in list(self.learn_axis) else 1 for ax, dim in enumerate(input_shape)]
        else:
            training_shape = ()

        # axes = [i for i, dim in enumerate(training_shape) if dim == 1 and i > 0]
        if 'ft' in self.data_format:
            self.frequency_axis = 1 if self.channel_axis == -1 else 2
            self.time_axis = 2 if self.channel_axis == -1 else 3
        else:
            self.frequency_axis = 2 if self.channel_axis == -1 else 3
            self.time_axis = 1 if self.channel_axis == -1 else 2
        # self.time_axis = axes[0 if self.channel_axis == -1 else -1]

        # Create all the parameters of the PCEN as trainable variables
        # all parameters need to be non-negative, so we work in log space for training and take the exponential to
        # calculate the actual PCEN
        # alpha and s also both need to be under 1
        alpha_init = self._initialiser(mean=self.alpha, sd=self.sd["alpha"], lower=0, upper=1)
        self.gain = self.add_weight(shape=training_shape if self.sd["alpha"] > 0 else (),
                                    initializer=alpha_init,
                                    dtype=self.dtype,
                                    trainable=self.trainable,
                                    name="gain")

        delta_init = self._initialiser(mean=self.delta, sd=self.sd["delta"], lower=0)
        self.bias = self.add_weight(shape=training_shape if self.sd["delta"] > 0 else (),
                                    initializer=delta_init,
                                    dtype=self.dtype,
                                    trainable=self.trainable,
                                    name="bias")

        r_init = self._initialiser(mean=self.r, sd=self.sd["r"], lower=0)
        self.power = self.add_weight(shape=training_shape if self.sd["r"] > 0 else (),
                                     initializer=r_init,
                                     dtype=self.dtype,
                                     trainable=self.trainable,
                                     name="power")

        eps_init = self._initialiser(mean=self.eps, sd=self.sd["eps"], lower=0)
        self.epsilon = self.add_weight(shape=training_shape if self.sd["eps"] > 0 else (),
                                       initializer=eps_init,
                                       dtype=self.dtype,
                                       trainable=self.trainable,
                                       name="threshold")

        s_init = self._initialiser(mean=self.s, sd=self.sd["s"], lower=0, upper=1)
        # if self.sd['s'] > 0:
        #     m_training_shape = (*training_shape, 1) if self.fast else training_shape
        #     # self.m specifically needs an additional axis of size 1 for the self.fast implementation
        # else:
        #     m_training_shape = ()
        self.m = self.add_weight(shape=training_shape if self.sd["s"] > 0 else (),
                                 initializer=s_init,
                                 trainable=self.trainable,
                                 name="smoother",
                                 dtype=self.dtype)

        self.perm = list(range(1, len(input_shape)))
        self.perm.insert(self.time_axis, 0)

        self.power_init, self.powers_past = self.build_m_power(t=input_shape[self.time_axis], ndim=len(input_shape))

        first_slice = [-1] * len(input_shape)
        first_slice[self.time_axis] = 1
        self.first_slice = tf.constant(first_slice, dtype=tf.int32)

        last_slice_begin = [0] * len(input_shape)
        last_slice_begin[self.time_axis] = -1
        last_slice_size = list(input_shape)
        last_slice_size[0] = -1
        last_slice_size[self.time_axis] = 1
        self.last_slice_begin = tf.constant(last_slice_begin, dtype=tf.int32)
        self.last_slice_size = tf.constant(last_slice_size, dtype=tf.int32)

        super().build(input_shape)

    @tf.function
    def build_m_power(self, t, ndim):
        powers = tf.range(1, t+1, dtype=self.dtype)

        # powers_past is a lower triangular matrix where each successive diagonal is an incremental integer
        # e.g. if t = 5, powers_past = [[0. 0. 0. 0. 0.]
        #                               [1. 0. 0. 0. 0.]
        #                               [2. 1. 0. 0. 0.]
        #                               [3. 2. 1. 0. 0.]
        #                               [4. 3. 2. 1. 0.]]
        powers_past = tf.linalg.band_part(tf.subtract(tf.expand_dims(powers, axis=-1), powers), -1, 0)

        new_shape = tf.constant([-1 if i == self.time_axis else 1 for i in range(ndim)])
        powers_init = tf.reshape(powers, shape=new_shape)
        return powers_init, powers_past

    @tf.function
    def fast_m(self, x, s):
        if self.last:
            last = self.last
        else:
            last = tf.slice(x, begin=tf.zeros_like(self.first_slice), size=self.first_slice)
        m0 = tf.multiply(tf.pow(1 - s, self.power_init), last)
        s = tf.expand_dims(s, axis=-1)
        m = tf.linalg.band_part(tf.pow(1 - s, self.powers_past), -1, 0)
        m = tf.add(m0, tf.transpose(tf.multiply(s, tf.matmul(m, tf.transpose(x, perm=(0, 2, 1, 3)))), perm=(0, 2, 1, 3)))
        return m

    @tf.function
    def memory_m(self, x, s):
        m = tf.unstack(x, num=x.shape[self.time_axis], axis=self.time_axis)
        if self.last:
            # initialise m like it's processing an infinite sequence prior to the actual x
            m[0] = tf.add(tf.multiply(1 - s, self.last), tf.multiply(s, m[0]))
        for i in tf.range(1, len(m)):
            m[i] = tf.add(tf.multiply(1 - s, m[i - 1]), tf.multiply(s, m[i]))
        m = tf.stack(m, axis=self.time_axis)
        return m

    @tf.function
    def call(self, x, **kwargs):
        m = self.m_function(x, tf.math.exp(self.m))
        smooth = tf.pow(tf.add(tf.math.exp(self.epsilon), m), tf.math.exp(self.gain))
        g = tf.math.divide_no_nan(x, smooth)

        delta = tf.math.exp(self.bias)
        r = tf.math.exp(self.power)

        if self.return_last:
            self.last = tf.slice(m, begin=self.last_slice_begin, size=self.last_slice_size)

        return tf.subtract(tf.pow(tf.add(g, delta), r), tf.pow(delta, r))

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = super().get_config()
        config.update({
            "sd": self.sd,
            "absolute_sd": self.absolute_sd,
            "learn_axis": self.learn_axis,
            "return_last": self.return_last,
            "last": self.last,
            "data_format": self.data_format
        })
        return config


if __name__ == "__main__":
    # Import a toy file from the accompanying test_audio directory
    dirname = os.path.dirname(__file__)
    path = "../test_audio"
    files = sorted([_ for _ in os.listdir(os.path.join(dirname, path)) if "wav" in _ and not _.startswith(".")])
    filename = os.path.join(dirname, path, files[0])
    print(filename)
    y, sr = sf.read(file=filename, always_2d=True)
    y = tf.transpose(y)
    S = tf.signal.stft(y, frame_length=2400, fft_length=2400, frame_step=600, window_fn=tf.signal.hamming_window)
    filt = librosa.filters.mel(sr=sr, n_fft=2400, n_mels=80)
    S = tf.abs(tf.cast(S, dtype=tf.float32))**2
    S = tf.matmul(S, tf.transpose(filt))
    S = tf.transpose(S)

    # Some parameters for the PCEN (both as the layer and for comparison with librosa below
    gain = .97
    bias = 1.
    power = .33
    eps = 1E-9
    smooth = .05
    sd = 0.

    # Create a toy model with just a PCEN layer to test output
    tf.random.set_seed(seed=42)
    x = PCEN(alpha=gain, delta=bias, r=power, s=smooth, eps=eps,
             sd=sd,
             absolute_sd=True,
             trainable=True,
             learn_axis=[2],
             implementation=None, dynamic=True, dtype="float32",
             data_format='btfc')

    P_custom = tf.transpose(tf.squeeze(x(tf.expand_dims(tf.transpose(S, perm=(1, 0, 2)), axis=0)), axis=0), perm=(1, 0, 2))

    # Compare to power_to_db and librosa.pcen
    P_db = librosa.power_to_db(S, ref=np.max)
    P_librosa = librosa.pcen(S.numpy(), sr=sr, hop_length=600, eps=eps, gain=gain, bias=bias, b=smooth, power=power,
                             axis=1)  # axis defaults to -1, which does NOT work because we have a multi-channel output
    print("Types of the results: librosa: {}, layer: {}".format(P_librosa.dtype, P_custom.dtype))
    print("Minimum values: librosa: {}, layer: {}".format(np.min(P_librosa), np.min(P_custom)))
    print("Maximum absolute difference: {}".format(np.max(np.abs(P_librosa - P_custom))))
    print("Total absolute difference: {}".format(np.sum(np.abs(P_librosa - P_custom))))

    plots = [tf.unstack(S, axis=-1),
             tf.unstack(P_custom, axis=-1),
             tf.unstack(P_db, axis=-1),
             tf.unstack(P_librosa, axis=-1),
             tf.unstack(P_custom - P_librosa, axis=-1)]
    titles = ["Original", "Layer", "dB", "Librosa", "Layer - librosa"]

    plt.figure()
    for i, plot in enumerate(plots):
        # setting common title to two plots??
        plt.subplot(len(plots), 2, 2*i+1)
        disp.specshow(plot[0].numpy(), sr=sr, hop_length=600, fmax=sr / 2, y_axis="mel")
        plt.colorbar()
        plt.subplot(len(plots), 2, 2*(i+1))
        disp.specshow(plot[1].numpy(), sr=sr, hop_length=600, fmax=sr / 2, y_axis="mel")
        plt.colorbar()
    plt.tight_layout()
    plt.show()
