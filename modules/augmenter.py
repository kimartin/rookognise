"""
Implements a class for easier data augmentation.
Instances of the class can be passed to the generators in audio_generators.py at the different stages of data generation.
Instantiating the class includes providing a dictionary of augmentation functions.
Provided augmentation function should take the full batches of samples, labels, and optionally mask arrays.
Other arguments should be passed using functools.partial when instantiating.

Other parameters of the Augmenter class are described in the docstring of the class itself
"""

import tensorflow as tf
import numpy as np
import random


class Augmenter(object):
    def __init__(self, transforms, p=.5, k=3, return_transforms=False):
        """
        Instantiates an object to apply various transformations to batchs of samples for training a neural network.

        :param transforms: Dict of callables, each callable describes a transformation to apply to a single (x, y)
        instance. Callables should be able to take the four arguments (x, y, x_batch, y_batch) in this order. Additional
        arguments should be named and provided using functools.partial(callable, arg1=val1, arg2=val2....).
        The four arguments described above will be passed as the first non-named arguments.
        :param p: float. Probability to apply a transform. This is drawn up to k times independently.
        :param k: int. Maximum number of transforms to apply to a given instance. Should be between 0 and len(transforms)
        :param return_transforms: bool. If True, returns a list of the transforms applied to each sample alongside the trasnformed samples.
        """
        self.transforms = transforms
        self.k = k
        self.p = p
        self.return_transforms = return_transforms

    def __call__(self, x, y, masks=None):
        """
            Takes x (and y?) as lists or entire batches.

            :param x: List of inputs array, or a tensor of shape (batch_size, *image_size).
            :param y: Optional list of inputs labels. If x is changed along an axis, y should match it
            :param k: Expected value of transforms to apply
            :param transforms: dictionary of transform functions to apply to each element of x
            :param return_transforms: bool. If True returns the transforms applied to each input.
            :return:
            """
        if masks is None:
            masks = [None]*len(x)

        transform_list = []
        augmented_x = []
        augmented_y = []
        augmented_masks = []
        # NB: shape of the inputs at this point is (channel, time, frequency) due to Tensorflow assumptions for STFT.
        for new_x, new_y, new_mask in zip(x, y, masks):
            trans_list = []
            possible = list(self.transforms.keys())
            while random.random() < self.p and len(trans_list) <= self.k and len(possible) > 0:
                trans = random.choice(possible)
                possible.remove(trans)
                trans_list.append(trans)
                new_x, new_y, new_mask = self.transforms[trans](new_x, new_y, new_mask, x, y, masks)
            augmented_x.append(new_x)
            augmented_y.append(new_y)
            augmented_masks.append(new_mask if new_mask is not None else None)
            transform_list.append(trans_list)

        if self.return_transforms:
            return augmented_x, augmented_y, augmented_masks, transform_list
        else:
            return augmented_x, augmented_y, augmented_masks


def fixed_sum_int(n, p, sum_to):
    arr = np.array([sum_to // n] * n)
    if p > 0:
        arr = np.random.randint(low=arr * (1 - p),
                                high=arr * (1 + p),
                                dtype="int")

    diff = sum_to - np.sum(arr)
    add = np.random.choice(range(n), size=np.abs(diff), replace=True)
    val, count = np.unique(add, return_counts=True)
    arr[val] += np.sign(diff) * count
    return arr


def apply_piecewise_stretch(arr, splits, new_shapes, value, slc):
    if len(arr.shape) < 3:
        for _ in range(3 - len(arr.shape)):
            arr = tf.expand_dims(arr, axis=-1)
    shape = list(arr.shape)
    axis = shape.index(value)
    reshape = []
    for n in new_shapes:
        shape[axis] = n
        reshape.append(tuple(shape[slc]))

    split_arr = tf.split(arr, num_or_size_splits=splits, axis=axis)
    split_arr = [tf.image.resize(s, size=r) for s, r in zip(split_arr, reshape)]
    return tf.squeeze(tf.concat(split_arr, axis=axis))


def array_stretch(x, y=None, mask=None, x_l=None, y_l=None, masks_list=None, n=None, p=None, axis=None):
    """
    Performs piece-wise stretching/compression on an image using tf.image.resize. First the image is divided into n
    pieces along the provided axis, with the width of the pieces being randomly drawn to be in the interval:
    ]a.shape[axis]/n * (1-p); a.shape[axis]/n * (1+p)[.

    :param x: An array or array-like object.
    :param n: The number of pieces to divide the array in.
    :param axis: int. The axis along which the stretching will be done.
    :param p: float, 0 < p < 1. The maximum proportion of compression/stretching.
    :param y: Label corresponding to x. If provided, stretching is also applied along the corresponding axis, which
    is taken to be the first axis of y.shape of the same value as x.shape[axis]
    :return: A Tensor object, stretched piecewise along the provided axis.
    """
    shape = list(x.shape)
    value = shape[axis]
    slc = slice(None)
    if len(shape) == 3:
        slc = slice(0, -1)
    elif len(shape) > 3:
        slc = slice(-3, -1)

    splits = fixed_sum_int(sum_to=value, n=n, p=0)
    new_shapes = fixed_sum_int(sum_to=value, n=n, p=p)
    x = apply_piecewise_stretch(x, splits, new_shapes, value, slc)

    if y is not None and value in y.shape:
        y = apply_piecewise_stretch(y, splits, new_shapes, value, slc)

    if mask is not None and value in mask.shape:
        mask = apply_piecewise_stretch(mask, splits, new_shapes, value, slc)

    return x, y, mask


def shuffle_slices(x, y=None, mask=None, x_l=None, y_l=None, masks_list=None, axis=-1, groups=None, size=None, out_size = None, **kwargs):
    """
    Shuffles the slices of an array, taken along an axis.

    :param y: a corresponding label to x
    :param groups: int: Number of groups. Effectively shuffles "groups" slices along axis.
    :param size: int. Size of groups. Usable when groups is unknown (e.g. if y.shape[axis] can vary or is unknown)
    :param x: The array to be shuffled
    :param axis: The axis determining the slices to be taken
    :return: An array of the same shape as a
    """
    groups = x.shape[axis] if groups is None else groups
    size = 1 if size is None else size
    groups //= size

    # range from 0 to x.shape[axis] with step size
    ind = tf.range(start=0, limit=x.shape[axis], delta=size, dtype=tf.int32)

    if out_size is not None:
        # add group(s) to fill out
        add = tf.random.uniform(shape=(out_size // size - groups, ),
                                minval=0,
                                maxval=x.shape[axis] // size,
                                dtype=tf.int32) * size
        ind = tf.concat([ind, add], axis=0)

    if size > 1:
        ind = tf.repeat(ind, repeats=tf.constant([size]*ind.shape[0]), axis=0)
        ind = ind + tf.concat([tf.range(0, size, dtype=tf.int32)] * (ind.shape[0] // size), axis=0)

    x = tf.gather(x, ind, axis=axis)
    # if y is not None and y.shape[axis] == x.shape[axis]:
    #     assert tf.rank(x) == tf.rank(y)
    #     y = tf.gather(y, ind, axis=axis)

    return x, y, mask


def match_hist(x, y=None, x_l=None, y_l=None, axis=None, *args, **kwargs):
    x_l = random.choice(x_l)
    if axis is not None:
        if x.shape[axis] != x_l.shape[axis]:
            raise ValueError(f"x and y should have the same dimension on axis {axis}")
        x = tf.unstack(x, axis=axis)
        x_l = tf.unstack(x_l, axis=axis)
        x_temp = map(lambda a: match_hist(*a), zip(x, x_l))
        x_temp = list(x_temp)
        return tf.stack(x_temp, axis=axis)

    else:
        # does the same histogram matching as skimage.match_histograms, but accepts Tensors
        x_flat = tf.reshape(x, shape=[-1])
        val_x, ind_x, counts_x = tf.unique_with_counts(x_flat)

        xl_flat = tf.reshape(x_l, shape=[-1])
        val_xl, ind_xl, counts_xl = tf.unique_with_counts(xl_flat)

        x_quant = tf.cumsum(counts_x) / tf.size(x)
        xl_quant = tf.cumsum(counts_xl) / tf.size(x_l)

        interp_x_values = np.interp(x_quant.numpy(), xl_quant.numpy(), val_xl.numpy())
        interp_x_values = interp_x_values[ind_x]

    return tf.cast(tf.reshape(interp_x_values, shape=x.shape), dtype=x.dtype), y


def add_gaussian_noise(x, y=None, mask=None, x_l=None, y_l=None, masks_list=None):
    noise = tf.random.normal(shape=x.shape, mean=0, stddev=tf.math.reduce_std(x) / 100, dtype=x.dtype)
    noisy_x = x + noise
    noisy_x = tf.clip_by_value(noisy_x, tf.reduce_min(x), tf.reduce_max(x))
    return noisy_x, y, mask


def replace_background(x, y, mask, x_list, y_list, masks_list, axis=0):
    try:
        # for better use, y should be of rank 1 before the following operations
        if tf.rank(y) > 1:
            axes = [i for i, j in enumerate(y.shape) if j not in x.shape]
        else:
            axes = []
        y_ = tf.reduce_max(y, axis=axes)

        # Get instances where all
        x_list = [x1 for x1, y1 in zip(x_list, y_list)
                  if tf.reduce_sum(tf.where(y_ == 0, tf.reduce_max(y1, axis=axes), 0)) == 0]

        # Choose a single instance
        ind = random.randrange(0, len(x_list))
        x_l = x_list[ind]
        if x.shape[axis] > x_l.shape[axis]:
            x_l = shuffle_slices(x_l, axis=axis, size=2, out_size=x.shape[axis])
        else:
            slc = [slice(None)]*len(x_l.shape)
            slc[axis] = slice(0, x.shape[axis])
            x_l = x_l[slc]

        axes = [i for i, j in enumerate(x.shape) if j not in y_.shape]
        for a in axes:
            y_ = tf.expand_dims(y_, axis=a)

        x = tf.where(y_ == 0, x_l, x)
        y = tf.maximum(y, y_list[ind])
        mask = tf.maximum(mask, masks_list[ind])
    except ValueError:
        # if no other input matches x and y, do nothing
        pass
    return x, y, mask


def random_volume_change(x, y=None, mask=None, x_l=None, y_l=None, masks_l=None, p=6):
    """
    Modifies volume of audio given in y by a random amount between -p and +p dB.
    The formula used for the conversion to dB is dB = 10*log10(x/ref), so e.g. dB + 3 corresponds to x * 10 ** (3/10).
    :param x:
    :param p:
    :return:
    """
    p = random.uniform(-p, p)
    x_changed = x * 10 ** (p / 10)
    M = tf.reduce_max(tf.abs(x))
    if M <= 1:
        m, M = -1, 1
    else:
        # if y contains integers, estimate the quantisation step and stay within the possible values of y
        pow = tf.math.ceil(tf.math.log(M) / tf.constant(tf.math.log(2.), dtype=M.dtype))
        m, M = - 2 ** pow, 2 ** pow - 1
    x_changed = tf.clip_by_value(x_changed, m, M)

    return x_changed, y, mask
