import seaborn as sns
import numpy as np
import pandas as pd

from matplotlib.lines import Line2D
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt, colors as col


def scatter_projections(
        projection,
        labels=None,
        ax=None,
        figsize=(10, 10),
        alpha=0.1,
        s=1,
        color="k",
        color_palette="hls",
        categorical_labels=True,
        show_legend=True,
        tick_pos="bottom",
        tick_size=16,
        cbar_orientation="vertical",
        log_x=False,
        log_y=False,
        grey_unlabelled=True,
        fig=None,
        colornorm=False,
        rasterized=True,
        equalize_axes=False,
        facecolor='black',
        sort_labels=True,
        print_lab_dict=False,  # prints color scheme
        legend_kwargs={'bbox_to_anchor': (1.3, .5), 'loc': "right"},
):
    """
    creates a scatterplot of syllables using some projection
    """
    # color labels
    if labels is not None:
        labels = pd.Series(labels)
        if categorical_labels:
            pal = sns.color_palette(color_palette, n_colors=labels.nunique())
            lab = labels.unique()
            if sort_labels:
                lab = sorted(lab)
            lab_dict = {lab: pal[i] for i, lab in enumerate(lab)}
            if grey_unlabelled:
                if -1 in lab_dict.keys():
                    lab_dict[-1] = [0.95, 0.95, 0.95, 1.0]
                if print_lab_dict:
                    print(lab_dict)
            colors = np.array([lab_dict[i] for i in labels], dtype=object)
    elif facecolor is not None:
        c = list(col.to_rgba(facecolor))
        c[:-1] = [1. - _ for _ in c[:-1]]
        colors = tuple(c)
    else:
        colors = color

    if ax is None:
        if projection.shape[1] == 2:
            fig, ax = plt.subplots(figsize=figsize)
        elif projection.shape[1] == 3:
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(projection='3d')

    # plot
    if colornorm:
        norm = col.LogNorm()
    else:
        norm = None

    if categorical_labels or labels is None:
        if labels is None:
            dc = dict(color=colors, norm=norm)
        else:
            dc = dict(c=colors, norm=norm)
    else:
        dc = dict(
            vmin=np.quantile(labels, 0.01),
            vmax=np.quantile(labels, 0.99),
            cmap=plt.get_cmap(color_palette),
        )

    sct = ax.scatter(
        x=projection[:, 0],
        y=projection[:, 1],
        rasterized=rasterized,
        alpha=alpha,
        s=s,
        marker='.',
        **dc
    )

    # Set background color (more useful if given labels)
    ax.set_facecolor(facecolor)

    if log_x:
        ax.set_xscale("log")
    if log_y:
        ax.set_yscale("log")

    if labels is not None and show_legend:
        if not categorical_labels:
            if cbar_orientation == "horizontal":
                axins1 = inset_axes(
                    ax,
                    width="50%",  # width = 50% of parent_bbox width
                    height="5%",  # height : 5%
                    loc="upper left",
                )
            else:
                axins1 = inset_axes(
                    ax,
                    width="5%",  # width = 5% of parent_bbox width
                    height="50%",  # height : 50%
                    loc="lower right",
                )
            cbar = plt.colorbar(sct, cax=axins1, orientation=cbar_orientation)
            cbar.ax.tick_params(labelsize=tick_size)
            axins1.xaxis.set_ticks_position(tick_pos)
        else:
            legend_elements = [
                Line2D([0], [0], marker=".", color=value, label=key)
                for key, value in lab_dict.items()
            ]
            ax.legend(handles=legend_elements, **legend_kwargs)
    if equalize_axes:
        ax.axis("equal")

    # Turns off axes and ticks but keeps facecolor
    for spine in ax.spines.values():
        spine.set_visible(False)
    ax.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)

    return ax