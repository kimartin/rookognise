from typing import Callable
import tensorflow as tf
K = tf.keras.backend
tfkl = tf.keras.layers

from utilities.functions import restrict_kwargs, broadcastable
from utilities.pcen import PCEN
from modules.custom_layers import Squeeze, MatMul, Clip, Print, Expand, LearnedDRC
from modules.custom_functions import mish
from modules.custom_modules import multi_scale_dense_module, conv_batchnorm_layer, apply_norm, self_attention_layer
from models.frontends import ConvPoolFrontEnd
from models.heads import Head, PresenceHead, Reconstruct_Head_Spec, Reconstruct_Head_Wave


def match_cases(arg, heads, default):
    match arg:
        case str() | bool() | int() | float() | tuple():
            arg = {k: arg for k in heads}
        case dict():
            arg = {**arg, **{k: default for k in heads if k not in arg}}
        case _:
            raise ValueError(f'Cannot interpret {arg=}')
    return arg


def build_model(input_shape,
                frontend,
                heads,
                stem=None,
                pool_type="max",
                head_filters=512,
                module_filters=512,
                conv_type=tfkl.Conv2D,
                activation: str | type(tfkl.Layer) | Callable = tfkl.LeakyReLU,
                activation_args=None,
                final_activation="sigmoid",
                dropout_rate: float = 0.,
                norm='renorm',
                embedding_layer='lstm',
                combinations=None,
                combination_op=None,
                masks: None | dict = None,
                variant: bool | dict = False,
                squeeze_excite_r: str = 0.,
                squeeze_excite_op: str = 'avg',
                padding: None | list = None,
                add_reconstruct: bool = False,
                name: str = "Rookognise",
                conv_kwargs: None | dict = None,
                presence_name: str = 'presence',
                unit_layer: type(tfkl.Layer) | Callable = conv_batchnorm_layer,
                msd_module_depth=2,
                msd_module_width=3,
                msd_module_c=0.,
                batch_size=None,
                head_func=Head,
                presence_head_func=PresenceHead
                ):
    """
    Main function used to define the model's architecture.

    :param input_shape: Shape of inputs, excluding the batch dimension.
    :param frontend: should be either instances of the Layer or Model classes, or layers stacked together.
        These layers go after the stem and before the heads.
        The main constraint is that the output of frontend should be squeezable to 2D, e.g. (N, 1, C) or (1, N, C)
    :param stem: should be either instances of the Layer or Model classes, or layers stacked together
    :param heads: dict of head names and number of outputs
    :param pool_type:
    :param head_filters: int. Number of filters to use for the heads.
    :param conv_type: reference to a ConvND class from Tensorflow. The type of convolution operation used in the model.
    :param activation: str, function, or (NOT INSTANTIATED) Layer object. The activation function to use in the model.
    :param activation_args: dict or None. Additional arguments provided to the activation function. Ignored if
        activation is not a Layer object.
    :param final_activation: str, function or Layer object, same as activation. The activation function used in the
    final classification layer of each head.
    :param dropout_rate: float. Dropout rate used before the last layer in each head.
    :param norm: str. Type of normalisation used in the model. See apply_norm.
    :param embedding_layer: str or tuple. Type of embedding layer used in the heads.
        e.g. 'lstm' for a single Bidirectional LSTM layer. See Head.
        The only parameter that can really be tuned is head_filters.
    :param combinations: dict. Provides the combination of head outputs to use in the final model.
        The keys are used to preserve output names.
    :param combination_op: Layer object. The operation to use in combining head outputs.
    :param masks: dict.
        This argument is used in conjunction with combinations, to provide masks to broadcast non-compatible outputs
        together.
        Example:
            heads = {"Sex": 2, "Detection": 1, "Identification": 15}
            combinations = {"Identification": ["Sex", "Detection"]}
            "Detection" can be combined as-is with "Identification", because it has 1 output class.
            But "Sex" is not broadcastable.
            So we use a mask ('mask_tensor') that essentially repeats each class of "Sex" to correspond to a class in
            "Identification".
            mask_tensor is a binary Tensor of the shape (15, 2) that is matrix-multiplied with "Sex".
            mask_tensor should make it so each class in e.g. "Identification" corresponds to one, and only one, class in
            "Sex".
            mask_tensor is then provided with: masks = {"Identification_Sex": mask_tensor}
    :param variant: bool, or dict of bool with the same names as heads.
        If False, connect the frontend to each head directly.
        If True, connect the frontend to a multiscale, densely connected module, THEN to each head.
        If dict, each head can be connected separately to the module if the respective dict key-item pair is True (with
            missing names taken as False).
        Example:
            heads = {"Sex": 2, "Detection": 1, "Identification": 15}
            variant = {"Identification": True} connects only the "Identification" head to the module, equivalent to
            variation = {"Identification": True, "Sex": False, "Detection": False}
    :param squeeze_excite_r: Compression ratio to use for Squeeze-Excite layers. Default value: 0, no Squeeze-Excite
        layer included.
    :param squeeze_excite_op: str. Operation to use for Squeeze-Excite layers. See SqueezeExcite
    :param padding: None or list. If None, equivalent to "valid" padding. If list, corresponding axes are zero-padded
        on both sides to be equivalent to a "same" padding.
    :param name: str. Name of the model.
    :param conv_kwargs: optional dict. Additional arguments used for convolution layers.
    :param presence_name: str. Name modifier for presence-type heads.
    :param unit_layer:
    :param msd_module_depth: Depth of the multiscale densely-connected (MSD) module (i.e. how many blocks deep)
    :param msd_module_width: Width of the MSD module (i.e. how many layers in parallel per block).
    :param msd_module_c: Compression ratio of the MSD module, with respect to the number of input channels
        (after each block, a pointwise convolution reduces the number of output channels to
        msd_module_c * input_channels). Defaults to 0, i.e. no compression.
    :param batch_size: None or int. Optional batch size specification.
    :param head_func:
    :param presence_head_func:
    :return:
    """
    combinations = combinations or dict()
    conv_kwargs = conv_kwargs or dict()

    variant = match_cases(variant, heads, default=False)
    embedding_layer = match_cases(embedding_layer, heads, default=None)
    head_filters = match_cases(head_filters, heads, default=None)

    if "multiply" in heads and "clip" in heads:
        raise ValueError(f"Both 'multiply' and 'clip' were found in the heads argument. Please provide at most one.")

    # NB: frequency_axis assumes spectrograms of shape (batch_size, time_steps, freq_steps, channels),
    # like those returned normally by tf.signal.stft (except for the channel axis)
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1
    frequency_axis = -2 if channel_axis == -1 else -1

    # Arguments that are common to all layer calls
    general_args = dict(
        conv_type=conv_type,
        activation=activation,
        activation_args=activation_args,
        pool_type=pool_type,
        norm=norm,
        padding=padding,
        conv_kwargs=conv_kwargs,
        squeeze_excite_r=squeeze_excite_r,
        squeeze_excite_op=squeeze_excite_op,
    )

    if batch_size is not None:
        inputs = pre = tfkl.Input(shape=input_shape)
    else:
        inputs = pre = tfkl.Input(batch_input_shape=(batch_size, *input_shape))

    if stem is not None:
        pre = stem(inputs)

    # pre = tfkl.BatchNormalization(axis=[channel_axis, frequency_axis])(pre)
    pre = apply_norm(pre, norm='batch', axis=[channel_axis, frequency_axis])

    x = frontend(pre)

    # squeeze out the frequency axis which has been summarised over
    x = Squeeze(axis=frequency_axis)(x)
    # replace the Conv2D (or a variant) with the corresponding Conv1D
    general_args['conv_type'] = getattr(tfkl, conv_type.__name__.replace('2', '1'))
    general_args['padding'] = 'same'

    x = unit_layer(x, filters=module_filters, kernel_size=9, **restrict_kwargs(general_args, unit_layer))
    x = unit_layer(x, filters=module_filters, kernel_size=1, **restrict_kwargs(general_args, unit_layer))

    module = multi_scale_dense_module(x,
                                      growth=module_filters // (msd_module_width + 1),
                                      kernel_size=3,
                                      unit_layer=unit_layer,
                                      width=msd_module_width,
                                      depth=msd_module_depth,
                                      c=msd_module_c,
                                      **restrict_kwargs(general_args, multi_scale_dense_module))

    outs = {}
    for head, n in heads.items():
        head_func_ = presence_head_func if presence_name in head else head_func
        head_args = dict(
            **general_args,
            embedding_layer=embedding_layer[head],
            dropout_rate=dropout_rate,
        )
        _, head_out = head_func_(
            module if variant[head] else x,
            n=n,
            filters=head_filters[head],
            final_activation=final_activation,
            name=head,
            unit_layer=unit_layer,
            combination=(head in combinations.keys() and combination_op is not None),
            **restrict_kwargs(head_args, head_func_)
        )
        outs.update({head: head_out})

    # Combines the outputs of several heads
    # combinations is a dict of the form {'head': ['head', 'head']}
    # for each  key, all values will be combined into the key head using combination_op
    # NB: a MatMul-ed mask is used to broadcast non-compatible outputs
    # e.g. if key_head as 2 classes, value_head1 has 1 class or 2 classes then OK
    # if now value_head2 as 3 classes, a mask must be provided
    if len(outs) > 1 and combination_op is not None and combinations is not None:
        for head_, comb_heads in combinations.items():
            combined_list = [outs[head_]] + [v
                                             if broadcastable(outs[head_], v)
                                             else MatMul(mask=tf.constant(masks[f'{head_}_{k_}'], dtype=tf.float32))(v)
                                             for k_, v in outs.items() if k_ in comb_heads]
            combined = combination_op(name=head_, dtype=tf.float32)(combined_list)
            outs[head_] = combined

    # if add_reconstruct:
    #     outs.update({
    #         'reconstructed_waveform': Reconstruct_Head_Wave(x=module,
    #                                                                  n=inputs.shape[channel_axis],
    #                                                                  filters=[128, 128, 64, 64, 32, 32],
    #                                                                  activation=activation,
    #                                                                  final_activation='tanh',
    #                                                                  name='reconstruct_wave'),
    #         'reconstructed_melspec': Reconstruct_Head_Spec(x=Expand(axis=frequency_axis)(module),
    #                                                        n=inputs.shape[channel_axis],
    #                                                        filters=[128, 128, 64, 64, 32, 32],
    #                                                        activation=activation,
    #                                                        final_activation='relu',
    #                                                        name='reconstruct_spec'),
    #     })

    model = tf.keras.Model(inputs=inputs,
                           outputs=outs,
                           name=f"{frontend.__name__}_{name}")

    # uncommenting this (and still getting the script to work)
    # requires modifying the stem function to return waveform_ and melspec_
    # if add_reconstruct and stem:
    #     #@tf.function
    #     def correlation_coefficient_loss(x, y):
    #         x -= tf.reduce_mean(x)
    #         y -= tf.reduce_mean(y)
    #         r_num = K.sum(tf.multiply(x, y))
    #         r_den = K.sqrt(tf.multiply(K.sum(K.square(x)), K.sum(K.square(y))))
    #         r = r_num / r_den
    #         # r = K.maximum(K.minimum(r, 1.0), -1.0)
    #         return r # 1 - K.square(r)
    #
    #     model.add_loss(correlation_coefficient_loss(outs['reconstructed_waveform'], tf.cast(waveform_, tf.float32)))
    #     model.add_loss(correlation_coefficient_loss(outs['reconstructed_melspec'], tf.cast(melspec_, tf.float32)))
    #
    #     model.add_metric(correlation_coefficient_loss(outs['reconstructed_waveform'], tf.cast(waveform_, tf.float32)), name="wav_reconstruct")
    #     model.add_metric(correlation_coefficient_loss(outs['reconstructed_melspec'], tf.cast(melspec_, tf.float32)), name='mel_reconstruct')

    return model


if __name__ == "__main__":
    from utilities.functions import get_model_memory_usage
    heads = {"presence": 15, "detection": 1, 'identification': 15, "sexing": 2}
    frontend_args = dict(growth=64, depth=2, width=3)
    args = dict(
        activation=mish,
        pool_type="conv",
        conv_type=tfkl.Conv2D,
        combination_op=Clip,
        embedding_layer='lstm',
        heads=heads,
        variant=True,
        combinations={"identification": ['presence', 'detection'], 'sexing': ['detection']},
        add_reconstruct=True,
    )

    mod = build_model(
        input_shape=(400, 80, 6),
        frontend=ConvPoolFrontEnd(padding=[0]),
        **args
    )
    print(get_model_memory_usage(batch_size=32, model=mod, policy=tf.keras.mixed_precision.Policy('mixed_float16')))

    # print(mod.summary())
    tf.keras.utils.plot_model(mod, to_file='C:/Users/killian/Desktop/model.png', show_shapes=True)
