import tensorflow as tf
import numpy as np
import pickle
import config
from utilities.functions import top_1
import pandas as pd

# paths
# data_path is the path to a pickle file, as output by predict (if Lines 83-84 are uncommented)
# i.e. the raw predictions on the test data of a model
data_path = 'C:/Users/killian/Desktop/database_crows/model_tests/20230712_184001_ovlp0.pickle'
# data_path = 'C:/Users/killian/Desktop/database_rooks/model_tests/20230222_214116_ovlp0.pickle'

# out_path is simply where to put the output file afterwards
out_path = 'C:/Users/killian/Desktop'

_, true, pred = pickle.load(open(data_path, 'rb'))

pred_id = pred['Source'][np.sum(true['Source'], axis=-1) == 1]
true_id = true['Source'][np.sum(true['Source'], axis=-1) == 1]

pred_id = top_1(pred_id, axis=-1)

print(np.sum(true_id, axis=0))
confusion = true_id.T @ pred_id

confusion_df = pd.DataFrame(confusion, index=sorted(config.classes['Source']), columns=sorted(config.classes['Source']))

confusion_df['CORRECT'] = np.diagonal(confusion)
confusion_df['TRUE'] = np.sum(confusion, axis=1)
confusion_df['PREDICTED'] = np.sum(confusion, axis=0)

confusion_df['RECALL'] = np.round(100 * confusion_df['CORRECT'] / confusion_df['TRUE'], 2)
confusion_df['PRECISION'] = np.round(100 * confusion_df['CORRECT'] / confusion_df['PREDICTED'], 2)

confusion_df.to_csv(f'{out_path}/confusion_crows.tsv', sep='\t')

