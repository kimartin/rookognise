"""
Replicates Fig. 3, the precision / recall curves for each output
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import roc_auc_score, average_precision_score, roc_curve, precision_recall_curve


def confusion_matrix(true, pred, n_thresholds, quantiles=False, axis=None):
    thresholds = np.linspace(0, 1, n_thresholds + 1)
    if quantiles:
        thresholds = np.quantile(pred, q=thresholds, axis=0)

    if axis is None:
        true = np.reshape(true, -1)
        pred = np.reshape(pred, -1)
        tp = np.zeros(shape=(n_thresholds+1, ))
        tn = np.zeros(shape=(n_thresholds+1,))
        fp = np.zeros(shape=(n_thresholds+1,))
        fn = np.zeros(shape=(n_thresholds+1,))
    else:
        if axis != -1 or axis != true.ndim:
            true = np.moveaxis(true, source=axis, destination=-1)
            pred = np.moveaxis(pred, source=axis, destination=-1)

        true = np.reshape(true, (-1, true.shape[-1]))
        pred = np.reshape(pred, (-1, pred.shape[-1]))
        tp = np.zeros(shape=(n_thresholds+1, true.shape[axis]))
        tn = np.zeros(shape=(n_thresholds+1, true.shape[axis]))
        fp = np.zeros(shape=(n_thresholds+1, true.shape[axis]))
        fn = np.zeros(shape=(n_thresholds+1, true.shape[axis]))

    for i, thr in enumerate(thresholds):
        p = np.where(pred > thr, np.ones_like(pred), np.zeros_like(pred))
        tp[i] = np.sum(true * p, axis=0 if axis is not None else None)
        tn[i] = np.sum((1-true) * (1-p), axis=0 if axis is not None else None)
        fp[i] = np.sum((1-true) * p, axis=0 if axis is not None else None)
        fn[i] = np.sum(true * (1-p), axis=0 if axis is not None else None)
    return thresholds, tp, fp, tn, fn


def metric_wrapper(true, pred, n_thr=200, axis=None, quantiles=False):
    thr, tp, fp, tn, fn = confusion_matrix(true, pred, n_thresholds=n_thr, quantiles=quantiles, axis=axis)
    with np.errstate(invalid='ignore', divide='ignore'):
        prec = tp / (fp + tp)
        rec = tp / (fn + tp)
        fpr = fp / (tn + fp)
        f = 2 * tp / (2 * tp + fp + fn)

    if axis is not None:
        _, tp_, fp_, tn_, fn_ = confusion_matrix(np.max(true, axis=-1),
                                                 np.max(pred, axis=-1),
                                                 axis=None,
                                                 n_thresholds=n_thr)
        with np.errstate(invalid='ignore', divide='ignore'):
            prop = (tp_ + fp_) / (tp_ + fp_ + tn_ + fn_)
    else:
        with np.errstate(invalid='ignore', divide='ignore'):
            prop = (tp + fp) / (tp + fp + tn + fn)

    prec[np.isnan(prec)] = 1.
    fpr[np.isnan(prec)] = 1.
    rec[np.isnan(rec)] = 1.
    f[np.isnan(f)] = 1.
    prop[np.isnan(prop)] = 1.

    return thr, prec, rec, fpr, f, prop


def figure_network_curves(true, pred, num_thresholds, filename=None):
    fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(9, 9))
    [ax.axis([0, 1, 0, 100]) for ax in axs.flat]
    [ax.set_aspect("auto") for ax in axs.flat]
    [ax.set_xlabel("Threshold", labelpad=0) for ax in axs.flat]
    [ax.set_ylabel("Value (%)", labelpad=0) for ax in axs.flat]
    [ax.tick_params(axis='both', labelsize=plt.rcParams['font.size'] - 4) for ax in axs.flat]

    for k in true:
        if k == "Detection":
            title = "Detection"
            dim = (0, 0)
            # thr, prec, rec, f, prop = metric_wrapper(t, p, n_thr=200, axis=-1, quantiles=False)
            # cons_threshold_det = thr[np.mean(rec, axis=-1) >= .95][-1]
            # optimistic_threshold_det = thr[np.argmin(np.abs(np.mean(rec, axis=-1) - np.mean(prec, axis=-1)))]
        elif k == "Sex":
            title = "Sex"
            dim = (0, 1)
        elif k == "presence_Source":
            title = "Presence"
            dim = (1, 0)
        else:
            title = "Source"
            dim = (1, 1)
            # p = p[np.sum(t, axis=-1) == 1, :]
            # t = t[np.sum(t, axis=-1) == 1, :]

        t = true[k]
        p = pred[k]

        thr, prec, rec, fpr, f, prop = metric_wrapper(np.squeeze(t), np.squeeze(p), n_thr=num_thresholds,
                                                      axis=None if k == "Detection" else -1, quantiles=False)
        cons_threshold = 0.05

        if len(rec.shape) > 1:
            axs[dim].fill_between(thr, np.mean(prec, axis=-1) * 100 - np.std(prec, axis=-1) * 100,
                                  np.mean(prec, axis=-1) * 100 + np.std(prec, axis=-1) * 100,
                                  facecolor="C0",
                                  alpha=.1)
            axs[dim].fill_between(thr, np.mean(rec, axis=-1) * 100 - np.std(rec, axis=-1) * 100,
                                  np.mean(rec, axis=-1) * 100 + np.std(rec, axis=-1) * 100,
                                  facecolor="tab:orange", alpha=.1)

            rec = np.mean(rec, axis=-1)
            prec = np.mean(prec, axis=-1)
            f = np.mean(f, axis=-1)
            axs[dim].hlines(y=np.mean(np.sum(t, axis=-1) > 0) * 100, xmin=0, xmax=1, colors="black", label='_nolegend_')
        else:
            axs[dim].hlines(y=np.mean(t) * 100, xmin=0, xmax=1, colors="black", label='_nolegend_')

        optimistic_threshold = thr[np.argmax(f)]

        axs[dim].plot(thr, prec * 100, color="C0")
        axs[dim].plot(thr, rec * 100, color="tab:orange")
        # axs[dim].plot(thr, f * 100, color = "yellow")

        print(k)
        print("Conservative", {'threshold': cons_threshold,
                               'prec': prec[thr == cons_threshold][0],
                               'rec': rec[thr == cons_threshold][0],
                               'f1': f[thr == cons_threshold][0],
                               'prop': prop[thr == cons_threshold][0]})
        print("Optimistic",
              {'threshold': optimistic_threshold,
               'prec': prec[thr == optimistic_threshold][0],
               'rec': rec[thr == optimistic_threshold][0],
               'f1': f[thr == optimistic_threshold][0],
               'prop': prop[thr == optimistic_threshold][0]})
        axs[dim].plot(thr, prop * 100, color="red")

        # axs[dim].plot(thr, f, color="black")

        # conservative threshold and corresponding values
        linestyle_cons = "--"
        axs[dim].vlines(cons_threshold, ymax=100, ymin=0, color="black", linestyles=linestyle_cons)
        axs[dim].hlines(y=prec[thr == cons_threshold] * 100, xmin=0, xmax=cons_threshold,
                        color="C0", linestyles=linestyle_cons, linewidth=1)
        axs[dim].hlines(y=rec[thr == cons_threshold] * 100, xmin=0, xmax=cons_threshold,
                        color="tab:orange", linestyles=linestyle_cons, linewidth=1)
        axs[dim].hlines(y=prop[thr == cons_threshold] * 100, xmin=0, xmax=cons_threshold,
                        color="red", linestyles=linestyle_cons, linewidth=1)
        # axs[dim].text(x = cons_threshold+.01, y = 0, s = round(cons_threshold, 2), color="black", va='bottom')
        # axs[dim].text(x = cons_threshold, y = prec[thr == cons_threshold]*100, s = f'{round(prec[thr == cons_threshold][0]*100, 1)}%', color="C0")
        # axs[dim].text(x = cons_threshold, y = rec[thr == cons_threshold]*100, s = f'{round(rec[thr == cons_threshold][0]*100, 1)}%', color="orange")
        # axs[dim].text(x = cons_threshold, y = prop[thr == cons_threshold]*100, s = f'{round(prop[thr == cons_threshold][0]*100, 1)}%', color="red")

        # optimistic threshold and corresponding values
        linestyle_optimistic = ":"
        axs[dim].vlines(optimistic_threshold, ymax=100, ymin=0, color="black", linestyles=linestyle_optimistic)
        axs[dim].hlines(y=prec[thr == optimistic_threshold] * 100, xmin=0, xmax=optimistic_threshold,
                        color="C0", linestyles=linestyle_optimistic, linewidth=1)
        axs[dim].hlines(y=rec[thr == optimistic_threshold] * 100, xmin=0, xmax=optimistic_threshold,
                        color="tab:orange", linestyles=linestyle_optimistic, linewidth=1)
        axs[dim].hlines(y=prop[thr == optimistic_threshold] * 100, xmin=0, xmax=optimistic_threshold,
                        color="red", linestyles=linestyle_optimistic, linewidth=1)
        # axs[dim].text(x = optimistic_threshold+.01, y = 0, s = round(optimistic_threshold, 2), color = "black", va='bottom')
        # axs[dim].text(x = optimistic_threshold, y = prec[thr == optimistic_threshold]*100, s = f'{round(prec[thr == optimistic_threshold][0]*100, 1)}%', color="C0")
        # axs[dim].text(x = optimistic_threshold, y = rec[thr == optimistic_threshold]*100, s = f'{round(rec[thr == optimistic_threshold][0]*100, 1)}%', color="orange")
        # axs[dim].text(x = optimistic_threshold, y = prop[thr == optimistic_threshold]*100, s = f'{round(prop[thr == optimistic_threshold][0]*100, 1)}%', color="red")

        axs[dim].set_title(title)

    fig.legend(["Precision", "Recall", "Retained"], ncol=3, loc="lower center")
    plt.subplots_adjust(wspace=.3, hspace=.3)
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename, dpi=1000, bbox_inches='tight')
        plt.close()


if __name__ == "__main__":
    import pickle

    # paths
    # data_path is the path to a pickle file, as output by predict (if Lines 83-84 are uncommented)
    # i.e. the raw predictions on the test data of a model
    data_path = 'C:/Users/killian/Desktop/database_crows/model_tests/20230712_184001_ovlp0.pickle'
    #data_path = 'C:/Users/killian/Desktop/database_rooks/model_tests/20230222_214116_ovlp0.pickle'

    # out_path is simply where to put the PNG file afterwards
    out_path = 'C:/Users/killian/Desktop'

    # Sets font size for all plots
    plt.rcParams.update({'font.size': 14})

    with open(data_path, 'rb') as file:
        spec, true, pred = pickle.load(file)

    # Figure 3 without the annotated values (these are printed by the script but mut be added manually afterwards)
    figure_network_curves(true,
                          pred,
                          num_thresholds=200,
                          filename=f'{out_path}/Fig3.png'
                          )