import re
import tensorflow as tf
import numpy as np
from utilities.functions import listofdict_to_dictoflist
from decimal import Decimal
from math import ceil


def overlap_add(x, chunk_overlap):
    if chunk_overlap == 0.:
        return np.reshape(x, (-1, x.shape[-1]))

    # assumes a 3D shape
    samples, frames, classes = x.shape

    if chunk_overlap > 1.:
        chunk_overlap = float(chunk_overlap / frames)
    elif chunk_overlap > frames:
        raise ValueError(f'{chunk_overlap=}, which is too high. '
                         f'Please provide chunk overlap as a positive scalar below {frames}.'
                         f'Values above 1 are used as is for step size, '
                         f'float values in [0, 1] interpreted as percentages')
    chunk_overlap = Decimal(str(chunk_overlap))

    step = ceil(frames * (1 - chunk_overlap))
    step_prop = ceil(1 / (1 - chunk_overlap))
    size = (samples - 1) * step + frames
    num = np.zeros(shape=(step_prop, size, classes), dtype=x.dtype)
    denom = np.zeros(shape=(step_prop, size, classes), dtype=x.dtype)
    for b in range(samples):
        num[b % step_prop, slice(step * b, step * b + frames)] = x[b]
        denom[b % step_prop, slice(step * b, step * b + frames)] = 1.

    num.sum(axis=0, out=num)

    return num / denom.sum(axis=0)


def format_outputs(models,
                   data,
                   y_true=None,
                   return_labels=False,
                   return_inputs=False,
                   trim_by_inputs=False,
                   chunk_overlap=0.,
                   ):
    """
    Format the outputs of one or more models.
    NB: assumes models are multi-outputs, so their outputs are assumed to be dictionaries with multiple key-value pairs
    :param models: model or list of models to use
    :param data: array-like or a generator, the data to predict on
    :param y_true: optional labels. Ignored if data is a generator
    :param return_labels: bool. Whether ground truth labels should be returned
    :param return_inputs: bool. Whether inputs should be returned.
    :param trim_by_inputs: If True, inputs that were padded to fit into the model(s) will be trimmed to remove the
    padding. Padding is defined as an input where all axis (except the batch axis) sum to 0.
    :param chunk_overlap: float. Non-negative. Overlap between successive samples. If between 0 and 1, considered as a
    proportion. If above 1, considered as step size.
    :return:
    """
    if not isinstance(models, list):
        y_pred = models.predict(x=data)
    elif len(models) == 1:
        y_pred = models[0].predict(x=data)
    else:
        y_pred = [model.predict(x=data) for model in models]
        y_pred = listofdict_to_dictoflist(y_pred)
        y_pred = {k: np.mean(np.stack(v, axis=0), axis=0) for k, v in y_pred.items()}

    if return_labels and return_inputs and y_true is None:
        inputs, y_true = zip(*[d[:2] for d in data])
    elif return_labels and y_true is None:
        y_true = [d[1] for d in data]
        y_true = listofdict_to_dictoflist(y_true)
        y_true = {k: np.concatenate(v, axis=0) for k, v in y_true.items()}
        inputs = None
    elif return_inputs or trim_by_inputs:
        inputs = [d[0] for d in data]
    else:
        inputs = None

    # Get indices of non-zero inputs (corresponding to padded samples) for later removal
    if trim_by_inputs:
        ind = [np.squeeze(np.sum(_, axis=tuple(range(1, len(_.shape)))) > 0) for _ in inputs]

        if return_inputs:
            inputs = np.concatenate([inpu[indi] for inpu, indi in zip(inputs, ind)], axis=0)
            inputs = overlap_add(x=inputs, chunk_overlap=chunk_overlap)
        else:
            inputs = None

        ind = np.concatenate(ind, axis=0)

        y_pred = {k: v[ind] for k, v in y_pred.items()}
        if y_true is not None:
            y_true = {k: v[ind] for k, v in y_true.items()}

    # Organize predictions (and preserve last axis even for 1-class outputs
    y_pred = {k: np.squeeze(v, axis=tuple(i for i, d in enumerate(v.shape[:-1]) if d == 1)) for k, v in y_pred.items()}
    # Flatten, accounting for chunk_overlap
    y_pred = {k: overlap_add(x=v, chunk_overlap=chunk_overlap) if len(v.shape) == 3 else v for k, v in y_pred.items()}

    # Organize and flatten labels likewise
    if return_labels:
        y_true = {k: np.squeeze(v, axis=tuple(i for i, d in enumerate(v.shape[:-1]) if d == 1)) for k, v in y_true.items()}
        y_true = {k: overlap_add(x=v, chunk_overlap=chunk_overlap) if len(v.shape) == 3 else v for k, v in y_true.items()}

    return inputs, y_true, y_pred


def make_embedding_model(model,
                         embedding_layer_names=["embedding"],
                         keep_original_output=True):
    """
    From a model object, create a new model object that returns not only the input model's outputs, but also a collection
    of outputs from the layers of the input models that have names containing one of the elements of embedding_layer_names.
    The outputs of the new model will be a dict of the original outputs with the corresponding layer names, with
    the additional layer outputs and their corresponding names.

    Create a new model that returns not
    :param model: A Keras / Tensorflow model object.
    :param embedding_layer_names: list or str. Gives the pattern(s) of layer names to include as additional outputs
    for the new model.
    :param keep_original_output: bool. If True, the new model's outputs will keep the original model's outputs. If False,
    only the layers found with embedding_layer_names will be included in the new model's outputs.
    :return: A Keras / Tensorflow model object.
    """

    if not isinstance(embedding_layer_names, list):
        embedding_layer_names = [embedding_layer_names]

    embeds = [layer.output for layer in model.layers if any([_ in layer.name for _ in embedding_layer_names])]
    if len(embeds) == 0:
        raise ValueError(f"No embedding layer found, i.e with name containing one of {embedding_layer_names}")

    # Should get the same name as the original outputs, otherwise get the actual output names?
    embeds = dict(zip([re.split(pattern="/", string=e.name)[0] for e in embeds], embeds))

    if keep_original_output:
        old_outputs = dict(zip([re.split(pattern="/", string=e.name)[0] for e in model.outputs],
                               model.outputs))
        # Create the new model that returns the embeddings in addition to its former outputs
        new_model = tf.keras.Model(inputs=model.inputs, outputs={**embeds, **old_outputs})
    else:
        new_model = tf.keras.Model(inputs=model.inputs, outputs=embeds)

    return new_model


def separate_embeddings_and_outputs(model,
                                    data,
                                    embedding_layer_names=['embedding'],
                                    labels=None):
    """
    Simple wrapper to separate the outputs and embeddings in a model create with make_embedding_model
    """
    _, labels, fit_features = format_outputs(model,
                                             data=data,
                                             labels=labels,
                                             )
    preds = {k: v for k, v in fit_features.items() if not any([_ in k for _ in embedding_layer_names])}
    embeddings = {k: v for k, v in fit_features.items() if any([_ in k for _ in embedding_layer_names])}

    # make embeddings.keys() match preds.keys() if at all possible
    for k_p in list(preds.keys()):
        for k_e in list(embeddings.keys()):
            if k_p in k_e:
                embeddings[k_p] = embeddings.pop(k_e)

    return labels, preds, embeddings


if __name__ == '__main__':
    test = np.random.uniform(size=(720, 800, 22))
    print(overlap_add(test, chunk_overlap=.5, axis=-2).shape)