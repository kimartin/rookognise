"""
Predict and save predictions on new data.
"""
import functools
import os
import re

import numpy as np
import tensorflow as tf
from tqdm import tqdm
import multiprocessing as mp
import importlib.util

from generators.audio_generators import AudioGenerator, SpectrogramGenerator
from utilities.output_manipulation import format_outputs
from utilities.functions import listofdict_to_dictoflist, are_dicts_equal
from utilities.extraction_functions import labels_from_network
import config

# Common root
root = "C:/Users/killian/Desktop/database_rooks"

# Path to the model (should end in a .tf file)
# model_path = [f'{root}/runs/Rookognise/20230203_185329/weights.tf']
model_path = [f'{root}/runs/temp2/{f}/weights.tf' for f in os.listdir(f'{root}/runs/temp2')]

# Load models
model_list = [tf.keras.models.load_model(path, compile=False) for path in model_path]

# Configure all generatores
generator_list = []
generator_args_list = []
for mod, mod_path in zip(model_list, model_path):
    try:
        import re
        import importlib

        spec = importlib.util.spec_from_file_location("config", mod_path.replace('weights.tf', 'config.py'))
        config = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(config)
    except (FileNotFoundError, ModuleNotFoundError):
        import config

    if len(mod.input_shape) == 4:
        generator_list.append(SpectrogramGenerator)
        generator_args_list.append(config.spectrogram_generator_args)
    else:
        generator_list.append(AudioGenerator)
        generator_args_list.append(config.waveform_generator_args)


# Path to wav files
data_path = f'{root}/unseen'

# Path to output
# out_path = f"{root}/unseen"
out_path = data_path

# List of files
audio_format = 'wav|flac'
files = os.listdir(data_path)

files = [f for f in files if not f.startswith('._')]
files = [f for f in files
         if bool(re.search(pattern=audio_format, string=f))
         and re.sub(pattern=audio_format, string=f, repl='txt') not in files]

for file in tqdm(files, total=len(files)):
    predictions = []
    if are_dicts_equal(generator_args_list):
        local_gen = generator_list[0](data=f'{data_path}/{file}', training=False, **generator_args_list[0])
        predictions = format_outputs(model_list, data=local_gen, chunk_overlap=0.)[-1]
    else:
        predictions = []
        for model, gen, gen_args in zip(model_list, generator_list, generator_args_list):
            local_gen = gen(data=f'{data_path}/{file}', labels=None, **gen_args, training=False)
            outputs = format_outputs(models=model, data=local_gen, chunk_overlap=0.)[-1]
            predictions.append(outputs)
        if len(model_list) == 1:
            predictions = predictions[0]
        else:
            # average the predictions of several models
            predictions = listofdict_to_dictoflist(predictions)
            predictions = {k: tf.reduce_mean(tf.stack(v, axis=0), axis=0) for k, v in predictions.items()}

    filename = re.sub(pattern=audio_format, repl=f'txt', string=file)
    labels = labels_from_network(predictions,
                                 threshold={'Detection': .5, 'Source': .2},
                                 min_frames=5,
                                 min_to_keep={'Detection': .75, 'Source': .5},
                                 method=np.median,
                                 merge=5,
                                 frame_dur=1. / 80.,
                                 names=config.classes)
    labels.to_csv(f'{out_path}/{filename}', index=False, header=False, sep="\t")






