"""
Implements a variety of generators for training neural networks on audio data with corresponding annotations.
Features include : random sampling from long recordings, RANDOM sub- and over-sampling (which can be done class-wise if
classes are found in the labels), additional negative sampling (i.e. extracting samples with only the negative class
present), augmentation, transformation from audio clip to spectrogram to mel-spectrogram, and generation of label data
from annotations.
Optionally, masks can be generated if some classes are included in the exclude argument
"""
import pickle
import re
import os
import warnings

import wave
import matplotlib.pyplot as plt
import scipy.signal
import tensorflow as tf
import soundfile as sf
import librosa
import numpy as np
import math
import pandas as pd
from time import time
from utilities.functions import merge_overlaps, flatten, preemphasis, listfiles, match_filenames


class BaseAudioGenerator(tf.keras.utils.Sequence):
    """
    Base class generator.

    Features :
        - take a set of audio paths and extract audio samples
        - take a set of labels files to determine samples

    Children classes should allow specifications of:
        - labels
        - data augmentation
    """

    def __init__(self,
                 data,
                 batch_size,
                 duration,
                 labels=None,
                 classes=None,
                 object_detection_name=None,
                 presence=None,
                 presence_name='presence',
                 training=False,
                 dtype="float32",
                 labels_format="tsv",
                 audio_format="wav",
                 n_channels=-1,
                 exclude=None,
                 ignore=None,
                 multilabel=None,
                 start_col='Start',
                 end_col='End',
                 negative_prop=0.,
                 match_check=None, # '[0-9]{8}',
                 k=1.,
                 fps=80,
                 mode='frame',
                 chunk_overlap=0.,
                 per_voc_sampling=True,
                 duration_in_samples=None,
                 **kwargs):
        # Generator parameters
        self.data = self.labels = data
        if labels is not None:
            self.labels = labels
        self.match_check = match_check
        self.duration = duration
        self.duration_in_samples = duration_in_samples
        self.batch_size = batch_size
        self.audio_format = audio_format
        self.labels_format = labels_format
        self.n_channels = n_channels
        self.training = training
        self.fps = fps
        self.frames = int(self.fps * self.duration)
        self.chunk_overlap = chunk_overlap
        if 1 < self.chunk_overlap <= self.frames:
            self.chunk_overlap /= self.frames
        elif self.chunk_overlap > self.frames:
            raise ValueError(f'{self.chunk_overlap=} which is too high. Please provide chunk overlap either as '
                             f'a float between 0 and 1, or as an int between 1 and {self.frames}.')

        self.per_voc_sampling = per_voc_sampling if self.training else False

        self.dtype = dtype

        self.k = k
        self.negative_prop = negative_prop

        self.start_col = start_col
        self.end_col = end_col
        self.classes = classes
        self.presence = presence or []
        self.presence_name = presence_name
        self.object_detection_name = object_detection_name

        # Prepare files
        # audio
        if isinstance(self.data, str) and os.path.isdir(self.data):
            self.audio_names = listfiles(path=self.data,
                                         pattern=[self.audio_format, '^\\._'],
                                         negate=[False, True],
                                         full_names=True)
        elif not isinstance(self.labels, list):
            self.audio_names = [self.data]
        else:
            self.audio_names = self.data
        self.audio_names = sorted(self.audio_names)

        # labels
        if self.labels is not None:
            if isinstance(self.labels, str) and os.path.isdir(self.labels):
                self.labels_names = listfiles(path=self.labels,
                                              pattern=[self.labels_format, '^\\._'],
                                              negate=[False, True],
                                              full_names=True)
            else:
                if not isinstance(self.labels, list):
                    self.labels = [self.labels]
                self.labels_names = match_filenames(filenames=self.labels,
                                                    pattern=[self.labels_format, '^\\._'],
                                                    negate=[False, True])

        if len(self.labels_names) == 0:
            self.labels = None

        if self.labels is not None:
            # File matching
            # Most trivial case : different numbers of labels and audio files
            audio_names = set([re.sub(self.audio_format, '', _) for _ in self.audio_names])
            label_names = set([re.sub(self.labels_format, '', _) for _ in self.labels_names])
            if len(audio_names - label_names) > 0:
                print('In audio but not in labels: ', sorted(audio_names - label_names))
            if len(label_names - audio_names) > 0:
                print('In labels but not in audio: ', sorted(label_names - audio_names))
            if len(self.audio_names) != len(self.labels_names):
                # print(list(zip(self.audio_names, self.labels_names)))
                raise ValueError(
                    f"{len(self.audio_names)} audio files and {len(self.labels_names)} label files have been "
                    f"found, but the same number of each should be present. ")

            # Pattern matching cases
            if self.match_check is not None:
                audio_patterns = [re.findall(pattern=self.match_check, string=_)[0] for _ in self.audio_names]
                labels_patterns = [re.findall(pattern=self.match_check, string=_)[0] for _ in self.labels_names]
            else:
                # special case: match files by their full name, excluding extensions and path
                audio_patterns = [re.findall(rf'[^/]*?(?={self.audio_format})', _)[0] for _ in self.audio_names]
                labels_patterns = [re.findall(rf'[^/]*?(?={self.labels_format})', _)[0] for _ in self.labels_names]

            if len(set(audio_patterns).symmetric_difference(labels_patterns)) > 0:
                raise ValueError(f'Some files do not match between audio and labels: '
                                 f'\naudio files not in labels: {sorted(list(set(audio_patterns) - set(labels_patterns)))}, '
                                 f'\nlabels files not in audio: {sorted(list(set(labels_patterns) - set(audio_patterns)))}')

        # Get relevant information from the audio files
        self.audio_info = [sf.info(file=_) for _ in self.audio_names]
        self.audio_info = dict(zip(self.audio_names, self.audio_info))
        self.lengths = {k: v.duration for k, v in self.audio_info.items()}
        self.channels = {k: v.channels for k, v in self.audio_info.items()}
        self.sr = [v.samplerate for _, v in self.audio_info.items()]

        # Open connections
        self.audio_objects = {name: sf.SoundFile(file=name) for name in self.audio_names}
        # self.audio_objects = {name: wave.open(f=name, mode='rb') for name in self.audio_names}

        self.total = sum(self.lengths.values())
        # if len(set(self.sr)) > 1:
        #     raise ValueError("Files of multiple sample rates are not currently supported")
        # self.sr = set(self.sr)[0]
        self.max_channels = max(n_channels, max(self.channels.values()))

        # Create label DataFrame and keep a copy saved
        self.reserve_labels = None
        if self.labels is not None:
            # read
            self.reserve_labels = [pd.read_table(_, engine="python") for _ in self.labels_names]

            for lab, file in zip(self.reserve_labels, self.audio_names):
                dur = sf.info(file).duration
                test = lab.loc[(lab['End'] - lab['Start']).idxmax()]

            # add column with name of corresponding file
            self.reserve_labels = [df.assign(name=self.audio_names[i], samplerate=self.sr[i]) for i, df in enumerate(self.reserve_labels)]
            # concatenate into one data frame
            self.reserve_labels = pd.concat(self.reserve_labels, ignore_index=True, axis=0)
            # self.reserve_labels.columns = map(str.lower, self.reserve_labels.columns)
            # define lower and upper bounds for start and end of chunks
            self.reserve_labels['max_start'] = np.maximum(0, self.reserve_labels[self.end_col] - self.duration)

        # Classes that are excluded from the labels (i.e. not considered for classification), ignored (i.e. removed
        # from training), and multilabel (i.e. correspond to multiple classes at once)
        self.exclude = exclude or []
        self.ignore = ignore or []
        self.multilabel = multilabel or []

        # Get classes
        if self.labels is not None and self.classes is not None:
            classes_in_labels = [_ in self.reserve_labels.columns for _ in self.classes]
            if not all(classes_in_labels):
                raise ValueError('Some specified columns are missing from the labels data frame: '
                                 f'{[c for i, c in enumerate(self.classes) if not classes_in_labels[i]]}')

            if isinstance(self.classes, list):
                # get classes from the labels
                warnings.warn(
                    'Classes were not provided but were obtained from the labels, and sorted alphabetically.'
                    'Please ensure that is the desired behaviour (e.g. if some classes are missing from the'
                    'labels).')
                self.classes = {
                    col:
                        sorted([_ for _ in self.reserve_labels[col].dropna().unique().tolist()
                                if _ not in self.exclude + self.ignore + self.multilabel])
                    for col in self.classes
                }

            def process(dc, name):
                if self.classes is None and len(dc) > 0 and not isinstance(dc, dict):
                    if self.classes is None and len(dc) > 0 and not isinstance(dc, dict):
                        raise ValueError(
                            f'{name} should be a dict providing the values to ignore in the desired columns')

                if self.classes is not None and not isinstance(dc, dict):
                    dc = {_: dc for _ in self.classes}

                return dc

            self.exclude = process(self.exclude, 'exclude')
            self.ignore = process(self.ignore, 'ignore')
            self.multilabel = process(self.multilabel, 'multilabel')

            # add class_id columns for faster label generation
            # instead of looking of indexes for every label, this lets us directly access them later
            for col in classes:
                class_id = dict(zip(self.classes[col], range(len(self.classes[col]))))
                class_id.update(dict(zip(self.exclude[col] + self.ignore[col] + self.multilabel[col],
                                         [-1] * len(self.exclude[col] + self.ignore[col] + self.multilabel[col]))))
                self.reserve_labels[f"{col}_id"] = self.reserve_labels[col].map(class_id).fillna(-1).astype("int")

        # Negative samples
        if self.labels is not None and self.negative_prop > 0.:
            interval_merge = merge_overlaps(self.reserve_labels,
                                            start=self.start_col,
                                            end=self.end_col,
                                            by=['name'])
            self.backgrounds = [
                pd.DataFrame({
                    'name': n,
                    start_col: [0] + _[self.end_col].tolist(),
                    end_col: _[self.start_col].tolist() + [self.lengths.get(n)]
                })
                for n, _ in interval_merge.groupby('name')
            ]
            self.backgrounds = pd.concat(self.backgrounds, ignore_index=True)
            self.backgrounds = self.backgrounds.loc[self.backgrounds[self.end_col] - self.backgrounds[self.start_col] > self.duration]
            self.backgrounds = self.backgrounds.reset_index(drop=True)

        # remove rows correspond to self.ignore
        # NB: we do this AFTER getting negative samples so that the negative samples cannot be taken from within
        # self.ignore intervals
        if self.labels is not None and self.classes:
            for col in self.classes:
                self.reserve_labels = self.reserve_labels.loc[~self.reserve_labels[col].isin(self.ignore[col])].reset_index(drop=True)

        # Number of samples in an epoch
        self.negative_prop = negative_prop if self.training and self.per_voc_sampling else 0.
        self.positive_samples = self.reserve_labels.shape[0] if self.labels else 0
        self.negative_samples = int(round(self.reserve_labels.shape[0] * self.negative_prop)) if self.labels else 0
        self.total_samples = self.negative_samples + self.positive_samples

        # Prepare shapes for the batch, labels, and masks
        self.sample_length = {sr: duration_in_samples or int(self.duration * sr) for sr in set(self.sr)}
        # self.sample_length = duration_in_samples or int(self.duration * max(self.sr))
        self.batch_shape = (self.batch_size, max(self.sample_length.values()), self.max_channels)
        self.labels_shape = dict()
        self.mask_shape = dict()
        if self.classes:
            self.labels_shape = {k: (self.batch_size, self.frames, len(v)) for k, v in self.classes.items()}
            self.mask_shape = {k: (self.batch_size, self.frames, 1) for k, v in self.classes.items()}

        if self.object_detection_name:
            self.labels_shape[self.object_detection_name] = (self.batch_size, self.frames, 1)
            self.mask_shape[self.object_detection_name] = (self.batch_size, self.frames, 1)

        if len(self.presence) > 0:
            self.labels_shape.update({
                f'{self.presence_name}_{k}': (self.batch_size, 1, len(v))
                for k, v in self.classes.items() if k in presence
            })
            self.mask_shape.update({
                f'{self.presence_name}_{k}': (self.batch_size, 1, 1)
                for k, v in self.classes.items() if k in presence
            })

        # determine label_gen_func
        self.mode = mode
        self.label_gen_func = self.frame_label_gen  # if self.mode == 'frame' else self.bounding_box_label_gen

        # This part is used only if self.training == False, otherwise it gets overwritten in self.on_epoch_end()
        self.starts = [np.arange(0, self.lengths[n], self.duration * (1 - self.chunk_overlap)) for n in self.audio_names]
        self.names = [[n] * len(s) for n, s in zip(self.audio_names, self.starts)]
        self.samplerates = [[sr] * len(s) for sr, s in zip(self.sr, self.starts)]
        self.starts = np.concatenate(self.starts)
        self.samplerates = np.concatenate(self.samplerates)
        self.names = flatten(self.names)

        self.on_epoch_end()

    def __len__(self):
        """
        Returns the number of batches in an epoch of training.

        :return: int number of batches in an epoch
        """
        if self.per_voc_sampling and self.total_samples > 0:
            n = math.ceil(self.total_samples * self.k / self.batch_size)
        else:
            n = math.ceil(len(self.starts) / self.batch_size)
        return n

    def on_epoch_end(self):
        if self.training and self.reserve_labels is not None:
            labels = self.reserve_labels
            if self.per_voc_sampling:
                labels['epoch_start'] = np.random.uniform(low=labels['max_start'],
                                                          high=labels[self.start_col],
                                                          size=labels.shape[0])

                # Draw audio samples from regions with no labels
                if self.negative_prop > 0.:
                    negative_labels = self.backgrounds.sample(n=self.negative_samples, replace=True)
                    negative_labels['epoch_start'] = np.random.uniform(low=negative_labels[self.start_col],
                                                                       high=negative_labels[self.end_col] - self.duration,
                                                                       size=negative_labels.shape[0])
                    labels = pd.concat([labels, negative_labels], ignore_index=True)

            else:
                labels = [pd.DataFrame({'name': n,
                                        'epoch_start': np.arange(0., self.lengths[n] - self.duration, self.duration),
                                        'duration': self.lengths[n] - self.duration})
                          for n in self.audio_names]
                labels = pd.concat(labels, ignore_index=True)
                labels['epoch_start'] = np.random.uniform(
                    low=np.maximum(0.,
                                   labels['epoch_start'] - self.duration * self.chunk_overlap),
                    high=np.minimum(labels['duration'] - self.duration,
                                    labels['epoch_start'] + self.duration * self.chunk_overlap),
                    size=labels.shape[0]
                )

            labels = labels.sample(frac=self.k, replace=self.k > 1.).reset_index(drop=True)
            self.names = labels['name']
            self.starts = labels['epoch_start']
            self.samplerates = labels['samplerate']

    def frame_label_gen(self, names, starts):
        if not self.labels:
            return None, None

        labels = {k: np.zeros(shape=v, dtype=self.dtype) for k, v in self.labels_shape.items()}
        masks = {k: np.ones(shape=v, dtype=self.dtype) for k, v in self.mask_shape.items()}

        for i, (name, start) in enumerate(zip(names, starts)):
            label_df = self.reserve_labels.loc[
                       (self.reserve_labels['name'].eq(name)) &
                       (self.reserve_labels[self.end_col] >= start) &
                       (self.reserve_labels[self.start_col] <= start + self.duration)
            ].reset_index(drop=True)

            # Clip so 0 is the start of the clip
            label_df[[self.start_col, self.end_col]] -= start
            label_df.loc[:, [self.start_col, self.end_col]] = label_df.loc[:, [self.start_col, self.end_col]].clip(0, self.duration)

            # take even partial frames as full presence (to also take a tiny, tiny bit of leeway)
            label_df[self.start_col] = np.floor(label_df[self.start_col] * self.fps).astype('int')
            label_df[self.end_col] = np.ceil(label_df[self.end_col] * self.fps).astype('int')

            if self.object_detection_name:
                for k, v in self.ignore.items():
                    for st, en, cl in label_df[[self.start_col, self.end_col, k]].itertuples(index=False):
                        if cl in v:
                            masks[self.object_detection_name][i, slice(st, en), :] = 0.
                        else:
                            labels[self.object_detection_name][i, slice(st, en), :] = 1.

            if self.classes:
                for k, v in self.classes.items():
                    for st, en, cl, id in label_df[[self.start_col, self.end_col, k, f'{k}_id']].itertuples(index=False):
                        if cl in self.exclude[k] + self.ignore[k]:
                            masks[k][i, slice(st, en), :] = 0.
                        else:
                            labels[k][i, slice(st, en), id] = 1.

            if len(self.presence) > 0:
                for k in self.presence:
                    labels[f'presence_{k}'] = labels[k].max(axis=-2, keepdims=True)
                    masks[f'presence_{k}'] = np.ones(shape=self.mask_shape[f'presence_{k}'], dtype=self.dtype)

        return labels, masks

    def chunk_batch(self, names, starts, samplerates):
        chunks = np.zeros(shape=self.batch_shape, dtype=self.dtype)
        # for some reason SoundFile does weird things with float32 when reading .flac files, resulting in extreme values
        # so for now, hard-coding reading to int16 then dividing by 32768 to get in correct range [-1., 1[
        for i, (name, start, sr) in enumerate(zip(names, starts, samplerates)):
            audio_file = self.audio_objects[name]
            audio_file.seek(start)
            data = audio_file.read(frames=self.sample_length[sr], always_2d=True, dtype=np.int16, fill_value=0.)
            chunks[i, ..., :self.sample_length[sr], :self.audio_info[name].channels] = data
        # for now hard-coding conversion to float
        chunks /= 32768.

        return chunks

    def __getitem__(self, item):
        ind = slice(item * self.batch_size, (item + 1) * self.batch_size)
        names = self.names[ind]
        starts = self.starts[ind]
        samplerates = self.samplerates[ind]

        chunks = self.chunk_batch(names, (starts * samplerates).astype('int'), samplerates)
        labels, masks = self.label_gen_func(names, starts)

        samplerates = np.pad(samplerates, [0, self.batch_size - len(samplerates)], constant_values=max(self.sr))

        return chunks, labels, masks, samplerates


class AudioGenerator(BaseAudioGenerator):
    def __getitem__(self, item):
        return super().__getitem__(item)[:-1]


class WaveformGenerator(BaseAudioGenerator):
    def __init__(self, pre=0., remove_offset=False, augment=None, **kwargs):
        super().__init__(**kwargs)
        self.pre = pre
        self.remove_offset = remove_offset
        self.augment = augment

        if len(set(self.sr)) > 1:
            raise ValueError('Cannot use WaveformGenerator when the audio dataset has multiple sampling rates.'
                             'Instead, please use SpectrogramGenerator.')

    def __getitem__(self, item):
        chunks, labels, masks, _ = super().__getitem__(item)

        if self.remove_offset:
            chunks -= np.mean(chunks, axis=-2, keepdims=True)

        if self.pre != 0.:
            chunks = preemphasis(chunks, pre=self.pre, normalise=True, axis=-2)

        if self.augment:
            # chunks is of the shape (self.batch_size, self.frames, self.max_channels)
            # and must be of shape (self.batch_size, self.max_channels, self.frames) for augmenting
            chunks = self.augment(chunks)

        return chunks, labels, masks


class SpectrogramGenerator(BaseAudioGenerator):
    def __init__(self,
                 pre=0.,
                 remove_offset=False,
                 augment=None,
                 wl=None,
                 ovlp=None,
                 expansion=1.,
                 frame_length=None,
                 frame_step=None,
                 fft_length=None,
                 window=tf.signal.hamming_window,
                 n_mels=None,
                 fmin=0,
                 fmax=np.inf,
                 power=1.,
                 fps=None,
                 audio_augment_function=None,
                 spectrogram_augment_function=None,
                 melspectrogram_augment_function=None,
                 **kwargs):

        self.pre = pre
        self.remove_offset = remove_offset
        self.augment = augment

        self.wl = wl
        self.ovlp = ovlp
        if self.ovlp:
           self.ovlp = self.ovlp if self.ovlp <= 1. else self.ovlp / 100.
        self.expansion = expansion

        if fps is None and self.wl:
            fps = 1. / (self.wl * (1. - self.ovlp))
        super().__init__(fps=fps, **kwargs)

        self.frame_length = {sr: frame_length or int(round(self.wl * sr)) for sr in set(self.sr)}
        self.frame_step = {sr: frame_step or int(round(fr * (1. - self.ovlp))) for sr, fr in self.frame_length.items()}
        self.fft_length = {sr: fft_length or int(round(fr * self.expansion)) for sr, fr in self.frame_length.items()}

        # self.frame_length = frame_length or int(round(self.wl * self.sr))
        # self.frame_step = frame_step or int(round(self.frame_length * (1. - self.ovlp)))
        # self.fft_length = fft_length or int(round(self.frame_length * self.expansion))

        self.n_mels = n_mels
        self.window = window
        self.fmin = fmin
        self.fmax = min(fmax, max(self.sr) // 2)
        self.power = power

        if self.n_mels:
            # self.filters = librosa.filters.mel(sr=self.sr, n_fft=self.frame_length, n_mels=self.n_mels, fmin=self.fmin, fmax=self.fmax, htk=True)
            self.filters = librosa.filters.mel(sr=max(self.sr),
                                               n_fft=max(self.fft_length.values()),
                                               n_mels=self.n_mels,
                                               fmin=self.fmin,
                                               fmax=self.fmax,
                                               htk=True)
            self.filters = tf.constant(self.filters.T)

        self.augment = audio_augment_function or self.augment
        self.spectrogram_augment_function = spectrogram_augment_function
        self.melspectrogram_augment_function = melspectrogram_augment_function

    def stft_fn(self, x):
        sr = x[1].numpy()
        x = x[0]
        x = tf.signal.stft(x[:self.sample_length[sr]],
                           window_fn=tf.signal.hamming_window,
                           frame_length=self.frame_length[sr],
                           frame_step=self.frame_step[sr],
                           fft_length=self.fft_length[sr],
                           pad_end=True
                           )
        x = tf.abs(x) ** self.power
        return x

    def __getitem__(self, item):
        chunks, labels, masks, samplerates = super().__getitem__(item)

        # exchanges the last two axes so the samples are in the last axis
        chunks = tf.transpose(chunks, perm=(0, 2, 1))

        chunks = tf.map_fn(self.stft_fn, elems=(chunks, samplerates), dtype=self.dtype)

        if self.spectrogram_augment_function is not None:
            chunks = self.spectrogram_augment_function(chunks)

        if self.n_mels:
            chunks = tf.matmul(chunks, self.filters)

        if self.melspectrogram_augment_function is not None:
            chunks = self.melspectrogram_augment_function(chunks)

        # chunks should be of shape (self.batch_size, self.frequency, self.frames, self.max_channels)
        chunks = tf.transpose(chunks, perm=[0, 2, 3, 1])

        return chunks, labels, masks


if __name__ == '__main__':
    gen = SpectrogramGenerator(data='C:/Users/killian/Desktop/database_rooks/data/test',
                             labels='C:/Users/killian/Desktop/database_rooks/data/test',
                             batch_size=32,
                               duration=10,
                               classes=['Sex', 'Source'],
                               object_detection_name='Detection',
                               presence=['Source'],
                               exclude=['Pls', 'Inc'],
                               ignore=['Ignore'],
                               training=False,
                               audio_format='wav|flac',
                               labels_format='tsv',
                               wl=.05,
                               ovlp=75.,
                               n_mels=80,
                               negative_prop=0.,
                               window=tf.signal.hamming_window
                             )
    for i in range(len(gen)):
        # fig, axs = plt.subplots(nrows=4, ncols=8)
        print(i)
        chunks, labels, masks = gen[i]

    #     for j, ax in enumerate(axs.flat):
    #         ax.plot(labels['Detection'][j])
    #         ax.plot(masks['Detection'][j])
    #     plt.show()
    #
    # for i in range(4):
    #     c = np.log10(np.max(chunks[i], axis=-1).T)
    #     axs[0, i].plot(labels['Detection'][i] * 80)
    #     axs[2, i].plot(labels['Sex'][i] * 80)
    #     axs[4, i].plot(labels['Source'][i] * 80)
    #
    #     axs[1, i].plot(masks['Detection'][i] * 80)
    #     axs[3, i].plot(masks['Sex'][i] * 80)
    #     axs[5, i].plot(masks['Source'][i] * 80)
    #
    # [ax.set_aspect('auto') for ax in axs.flat]
    # plt.show()
    # print(chunks.shape, {k: v.shape for k, v in labels.items()}, {k: v.shape for k, v in masks.items()})
    # print({k: np.sum(v) for k, v in labels.items()}, {k: np.sum(v) for k, v in masks.items()})