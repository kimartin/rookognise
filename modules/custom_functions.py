import tensorflow as tf


def step_relu(alpha):
    @tf.function
    def step_relu_fixed_alpha(x):
        return tf.maximum(alpha, tf.floor(x) + tf.nn.sigmoid(100*(tf.math.mod(x, 1) - 0.5)))
    return step_relu_fixed_alpha


@tf.function
def mish(x):
    return x * tf.nn.tanh(tf.nn.softplus(x))


class Mish(tf.keras.layers.Activation):
    def __init__(self, init_weight=1., axis=None, **kwargs):
        super().__init__(**kwargs)

        self.init_weight = init_weight
        self.axis = [axis] if isinstance(axis, int) else list(axis)

    def build(self, input_shape):
        if self.axis is not None:
            shape = [1] * len(input_shape)
            for ax in self.axis:
                shape[ax] = input_shape[ax]
        else:
            shape = ()

        self.weight = self.add_weight(name='weight',
                                      shape=shape,
                                      initializer=tf.keras.initializers.Constant(value=self.init_weight),
                                      trainable=True,
                                      dtype=self.dtype,
                                      constraint=tf.keras.constraints.NonNeg())

        super.build(input_shape)

    def call(self, inputs):
        return inputs * tf.nn.tanh(tf.nn.softplus(inputs * self.weight))


def leaky_relu(alpha):
    @tf.function
    def leaky_relu_fixed_alpha(x):
        return tf.maximum(alpha * x, x)
    return leaky_relu_fixed_alpha
